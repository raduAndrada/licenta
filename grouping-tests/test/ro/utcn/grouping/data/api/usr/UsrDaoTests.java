package ro.utcn.grouping.data.api.usr;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import org.junit.Test;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.google.common.collect.ImmutableList;

import ro.utcn.grouping.data.api.usr.ImtUsrEntityGet;
import ro.utcn.grouping.data.api.usr.UsrDao;
import ro.utcn.grouping.model.usr.ImtUsrCriteria;
import ro.utcn.grouping.model.usr.MdfUsr;
import ro.utcn.grouping.model.usr.Usr;
import ro.utcn.grouping.model.usr.UsrTp;
import test.DataTests;
import test.TestDbUtil;

public class UsrDaoTests extends DataTests {

	private static final MdfUsr USR_1 = MdfUsr.create().setUsrnm("andrada").setUsrTp(UsrTp.STUDENT)
			.setEmail("radu.andrada8@gmail.com").setFnm("radu").setLnm("andrada").setPsswd("andrada");

	static {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Inject
	private UsrDao usrDao;

	@Inject
	private TestDbUtil dbUtil;

	@Test
	@DatabaseSetup(value = "UsrDaoTests-01-i.xml")
	@ExpectedDatabase(value = "UsrDaoTests-01-e.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testAddUsr_onEmptyDatabase() throws Exception {
		usrDao.addUsr(USR_1);
	}

	@Test
	@DatabaseSetup(value = "UsrDaoTests-01-e.xml")
	public void testGetUsr() throws Exception {
		final List<Usr> usrs = usrDao.getUsr(ImtUsrEntityGet.builder()
				.criteria(ImtUsrCriteria.builder().addEmailIncl("radu.andrada8@gmail.com").build()).build());
		assertThat(usrs).isEqualTo(ImmutableList.of(USR_1));
	}

}
