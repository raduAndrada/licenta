package ro.utcn.grouping.data.api.course;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import org.junit.Test;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.google.common.collect.ImmutableList;

import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.ImtCourse;
import ro.utcn.grouping.model.course.ImtCourseCriteria;
import ro.utcn.grouping.model.course.MdfCourseRecord;
import test.DataTests;

public class CourseDaoTests extends DataTests {

	private static final Course COURSE_1 = ImtCourse.builder().nm("Introduction in computer science")
			.dsc("The basics of computer science").studentsCount(30).build();

	private static final Course COURSE_2 = ImtCourse.builder().nm("Introduction in computer science")
			.dsc("A general presentation of computer programming").studentsCount(30).build();

	private static final MdfCourseRecord COURSE_RECORD_1 = MdfCourseRecord.create().setCourse(COURSE_1)
			.setCrtTm(Instant.parse("2018-03-11T11:57:00.00Z"));

	static {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Inject
	private CourseDao courseDao;

	@Test
	@DatabaseSetup(value = "CourseDaoTests-01-i.xml")
	@ExpectedDatabase(value = "CourseDaoTests-01-e.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testAddCourse_onEmptyDatabase() throws Exception {
		courseDao.addCourse(COURSE_RECORD_1);
	}

	@Test
	@DatabaseSetup(value = "CourseDaoTests-01-e.xml")
	public void testGetCourse() throws Exception {
		final List<Course> courses = courseDao.getCourse(ImtCourseEntityGet.builder()
				.criteria(ImtCourseCriteria.builder().addNmIncl("Introduction in computer science").build()).build());
		assertThat(courses).isEqualTo(ImmutableList.of(COURSE_RECORD_1.getCourse()));
	}

	@Test
	@DatabaseSetup(value = "CourseDaoTests-02-i.xml")
	@ExpectedDatabase(value = "CourseDaoTests-02-e.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testUpdate_Course() throws Exception {
		courseDao.updateCourse(ImtCourseEntityUpdate.builder()
				.criteria(ImtCourseCriteria.builder().addNmIncl("Introduction in computer science").build())
				.dsc("A general presentation of computer programming").build());
	}

	@Test
	@DatabaseSetup(value = "CourseDaoTests-03-i.xml")
	@ExpectedDatabase(value = "CourseDaoTests-03-e.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testDelete_Course() throws Exception {
		courseDao.deleteCourse(ImtCourseEntityDelete.builder()
				.criteria(ImtCourseCriteria.builder().addNmIncl("Introduction in computer science").build()).build());
	}

}
