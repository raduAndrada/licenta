package ro.utcn.grouping.data.api.rq;

import java.util.TimeZone;

import javax.inject.Inject;

import org.junit.Test;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

import ro.utcn.grouping.model.rq.ImtRqCriteria;
import ro.utcn.grouping.model.rq.RqCriteria;
import ro.utcn.grouping.model.rq.RqStatus;
import test.DataTests;

public class RqDaoTests extends DataTests {

	static {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Inject
	private RqDao rqDao;

	@Test
	@DatabaseSetup(value = "RqDaoTests-01-i.xml")
	@ExpectedDatabase(value = "RqDaoTests-01-e.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testAddCourse_onEmptyDatabase() throws Exception {
		final RqCriteria criteria = ImtRqCriteria.builder().addIdIncl(1L).build();
		final int ok = rqDao
				.updateRq(ImtRqEntityUpdate.builder().rqStatus(RqStatus.ONGOING).criteria(criteria).build());
	}

}
