drop table if exists student;
drop table if exists proffessor;
drop table if exists course;
drop table if exists usr;



CREATE TABLE professor (
    id          INTEGER NOT NULL ,
    usr_usrnm   VARCHAR(150) NOT NULL,
    usr_email   VARCHAR(150) NOT NULL
);


ALTER TABLE professor ADD CONSTRAINT professor_pk PRIMARY KEY ( id );


CREATE TABLE student (
    id          INTEGER NOT NULL ,
    usr_usrnm   VARCHAR(150) NOT NULL,
    usr_email   VARCHAR(150) NOT NULL,
    group_id    INTEGER NOT NULL
);



ALTER TABLE student ADD CONSTRAINT student_pk PRIMARY KEY ( id );


CREATE TABLE usr (
    usrnm     VARCHAR(150) NOT NULL ,
    psswd     VARCHAR(150) NOT NULL ,
    email     VARCHAR(150) NOT NULL ,
    usr_tp      VARCHAR(100) NOT NULL ,
    f_nm      VARCHAR(100) ,
    l_nm      VARCHAR(100) 
);



ALTER TABLE usr ADD CONSTRAINT user_pk PRIMARY KEY ( usrnm,
email );

CREATE TABLE course (
    nm   VARCHAR(100) NOT NULL ,
    dsc   VARCHAR(300) NOT NULL ,
    crt_tm DATETIME NOT NULL ,
    students_count INT(11) NOT NULL,
);


ALTER TABLE course ADD CONSTRAINT course_pk PRIMARY KEY ( nm );

CREATE TABLE rq (
    id INT(11) NOT NULL ,
    rq_tp  VARCHAR(50) ,
    lesson_id  INT(11) NOT NULL ,
    allowed_tm INT(11) NOT NULL ,
    max_rsp INT(11) NOT NULL,
    time_measurement_tp  VARCHAR(150) ,
    rq_status  VARCHAR(50) ,
    rq_groupp_tp VARCHAR(150) ,
    rq_groupp_sz INT(11) 
);


ALTER TABLE rq ADD CONSTRAINT rq_pk PRIMARY KEY ( id );





