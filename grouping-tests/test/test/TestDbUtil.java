package test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

public class TestDbUtil {
	private final DataSource dataSource;

	public TestDbUtil(final DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setIdentity(final String tableName, final Integer identityNext) {
		try (final Connection connection = dataSource.getConnection()) {
			try (final Statement statement = connection.createStatement()) {
				statement.executeQuery(
						String.format("alter table %s alter column ID RESTART WITH %s", tableName, identityNext));
			}
		} catch (final SQLException e) {
			throw new IllegalStateException("Failed to set identity", e);
		}
	}

}
