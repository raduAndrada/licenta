package test;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;

public class TestsConfigBase {

	@Bean
	public TestDbUtil testDbUtil(final DataSource dataSource) {
		return new TestDbUtil(dataSource);
	}
}
