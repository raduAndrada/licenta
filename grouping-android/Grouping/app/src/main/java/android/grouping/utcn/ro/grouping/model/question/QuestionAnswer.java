package android.grouping.utcn.ro.grouping.model.question;

/**
 * Created by Andrada on 5/5/2018.
 */
public class QuestionAnswer {

    private Long id;

    private Long questionId;

    private String answerText;

    private Boolean correct;

    public QuestionAnswer(String answerText, Boolean correct, Long id, Long questionId) {
        this.answerText = answerText;
        this.correct = correct;
        this.id = id;
        this.questionId = questionId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
}
