package android.grouping.utcn.ro.grouping.adaptors;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.grouping.utcn.ro.grouping.R;
import android.grouping.utcn.ro.grouping.activities.usr.ChooseGrouppForSemProjActivity;
import android.grouping.utcn.ro.grouping.model.course.Course;
import android.grouping.utcn.ro.grouping.model.course.CourseDetail;
import android.grouping.utcn.ro.grouping.model.course.CourseUsr;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.beardedhen.androidbootstrap.api.attributes.BootstrapBrand;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andrada on 3/10/2018.
 */
public class CourseAdapter extends ArrayAdapter {

    private static final String USRNM = "usrnm";
    private static final String PSSWD = "passwd";
    private static final String COURSE_NM = "courseNm";
    private static final String SHARED_PREFERENCES = "sharedPreferences";

    public static final String GROUPPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPPING_REST_BASE_URL + "/v1/students/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);


    private String studentUsrnm;
    public CourseAdapter(Context context, List<CourseDetail> courses) {
        super(context, 0, courses);
        TypefaceProvider.registerDefaultIconSets();
        //this.studentUsrnm = usrnm;
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.studentUsrnm = sharedPref.getString(USRNM, "none");


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final CourseDetail courseRecord = (CourseDetail) getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.course_item, parent, false);
        }
        // Lookup view for data population
        TextView courseNm = (TextView) convertView.findViewById(R.id.courseItemNmId);
        TextView courseDsc = (TextView) convertView.findViewById(R.id.courseItemDscId);
        final BootstrapButton btnEnroll = (BootstrapButton) convertView.findViewById(R.id.coursItemEnroll) ;
        final BootstrapButton btnViewSemProjs = (BootstrapButton) convertView.findViewById(R.id.courseItemViewSemProjBtnId) ;

        btnViewSemProjs.setVisibility(View.INVISIBLE);


        // Populate the data into the template view using the data object
        courseNm.setText(courseRecord.getCourse().getNm());
        courseDsc.setText(courseRecord.getCourse().getDsc());
        final View finalConvertView = convertView;

        btnEnroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usrService.enrollStudent(new CourseUsr( courseRecord.getCourse().getNm(), studentUsrnm)).enqueue(new Callback<Course>() {
                    @Override
                    public void onResponse(Call<Course> call, Response<Course> response) {
                        btnViewSemProjs.setVisibility(View.VISIBLE);
                        btnEnroll.setText("Enrolled");
                        btnEnroll.setEnabled(false);
                        Toast.makeText(finalConvertView.getContext(), "Enrolled successful", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Course> call, Throwable t) {

                    }
                });
            }
        });
        btnViewSemProjs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = finalConvertView.getContext().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(COURSE_NM, courseRecord.getCourse().getNm());
                editor.commit();
                Intent intent = new Intent(getContext(), ChooseGrouppForSemProjActivity.class);
                //intent.putExtra(PASSWORD,passwordEditText.getText().toString());
                getContext().startActivity(intent);
            }
        });

        if (courseRecord.isEnrolled()) {
            btnEnroll.setText("Enrolled");
            btnEnroll.setEnabled(false);
            btnViewSemProjs.setVisibility(View.VISIBLE);
        }
        // Return the completed view to render on screen
        return convertView;
    }

}
