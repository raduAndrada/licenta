package android.grouping.utcn.ro.grouping.activities.usr;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.adaptors.RqAdapter;
import android.grouping.utcn.ro.grouping.model.question.QuestionRecord;
import android.grouping.utcn.ro.grouping.model.rq.Rq;
import android.grouping.utcn.ro.grouping.model.rq.RqRecord;
import android.grouping.utcn.ro.grouping.model.rq.RqTp;
import android.grouping.utcn.ro.grouping.model.rq.TimeMeasurementTp;
import android.grouping.utcn.ro.grouping.model.rsp.Rsp;
import android.grouping.utcn.ro.grouping.model.rsp.RspCreateResultList;
import android.grouping.utcn.ro.grouping.model.rsp.RspCreateList;
import android.grouping.utcn.ro.grouping.model.usr.Usr;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.grouping.utcn.ro.grouping.R;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.beardedhen.androidbootstrap.TypefaceProvider;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RqGrouppActivity extends AppCompatActivity {


    private static final String USRNM = "usrnm";
    private static final String SHARED_PREFERENCES = "sharedPreferences";
    private static final String RQ_TP = "rqTp";
    private static final String ALLOWED_TIME= "allowedTime";
    private static final String RQ_GROUPP_ID = "rqGrouppId";
    public static final String GROUPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPING_REST_BASE_URL + "/v1/students/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);


    private List<QuestionRecord> questionRecords= new ArrayList<>();
    private RqAdapter adapter = null;
    private Toolbar toolbar;

    private Long rqGrouppId ;
    private String usrnm;
    private RqTp rqTp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rq_groupp);
        TypefaceProvider.registerDefaultIconSets();
        toolbar = (Toolbar) findViewById(R.id.main_menu_id);
        toolbar.setTitle("Time");
        setSupportActionBar(toolbar);

        final SharedPreferences sharedPref = RqGrouppActivity.this.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        usrnm = sharedPref.getString(USRNM, "none");
        rqGrouppId= sharedPref.getLong(RQ_GROUPP_ID, 0L);


        usrService.getRqForStudent(usrnm).enqueue(new Callback<RqRecord>() {
            @Override
            public void onResponse(Call<RqRecord> call, Response<RqRecord> response) {
                final RqRecord rqRecord = response.body();
                rqTp = rqRecord.getRq().getRqTp();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(RQ_TP, rqTp.toString());
                editor.commit();
                questionRecords.addAll(rqRecord.getQuestions());
                adapter = new RqAdapter(RqGrouppActivity.this, questionRecords);
                ListView listView = (ListView) findViewById(R.id.question_record_list_view_id);
                listView.setAdapter(adapter);
                // adapter.notifyDataSetChanged();
                Integer allowedTm = computeAllowedTime(rqRecord.getRq(),rqRecord.getQuestions().size());
                editor.putInt(ALLOWED_TIME, allowedTm);
                editor.commit();
                final QuestionCountDownTimer questionCountDownTimer = new QuestionCountDownTimer(allowedTm,1000);
                questionCountDownTimer.start();

            }

            @Override
            public void onFailure(Call<RqRecord> call, Throwable t) {

            }
        });

        //adapter.notifyDataSetChanged();


    }

    private Integer computeAllowedTime(Rq rq, int qNum) {
        final Integer allowedTm = rq.getTimeMeasurementTp() == TimeMeasurementTp.ON_QUESTION
                ? qNum * rq.getAllowedTm()
                : rq.getAllowedTm();
        return allowedTm * 1000;
    }

    private void buildRspListForTextFillRq() {
        View v;
        EditText et;
        RspCreateList rspList = new RspCreateList(null);
        List <Rsp> rsps = new ArrayList<Rsp>();
        for (int i = 0; i < adapter.getCount(); i++) {
            QuestionRecord qRecord = (QuestionRecord) adapter.getItem(i);
            v = adapter.getView(i,null,null);
            et = (EditText) v.findViewById(R.id.question_fill_answer_text_id);
            Rsp rsp = new Rsp(null, qRecord.getQuestion().getId(), rqGrouppId, qRecord.getqAnswer(), usrnm);
            rsps.add(rsp);
        }
        rspList.setList(rsps);
        usrService.sendRsps(rspList).enqueue(new Callback<RspCreateResultList>() {
            @Override
            public void onResponse(Call<RspCreateResultList> call, Response<RspCreateResultList> response) {
                // OK .. we sent it!

            }

            @Override
            public void onFailure(Call<RspCreateResultList> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.main_menu_edit_id) {
            return true;
        }
        if (id == R.id.main_menu_home_id) {
            return true;
        }
        if (id == R.id.main_menu_logout_id) {
            final Usr usr = new Usr();
            usr.setUsrnm(this.usrnm);
            usr.setPsswd("student");
            usrService.logoutUsr(usr).enqueue(new Callback<Usr>() {
                @Override
                public void onResponse(Call<Usr> call, Response<Usr> response) {
                    Intent intent = new Intent(RqGrouppActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<Usr> call, Throwable t) {
                    System.out.print(call);
                }
            });
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private class QuestionCountDownTimer extends CountDownTimer {

        public QuestionCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

            toolbar.setTitle("Time's up!");

            if (rqTp == RqTp.TEXT_FILLING) {
                buildRspListForTextFillRq();
            }
            createAlertDialog();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            toolbar.setTitle("Time: " + millisUntilFinished/1000 + " s");

        }

        private void createAlertDialog(){
            AlertDialog alertDialog = new AlertDialog.Builder(RqGrouppActivity.this).create();
            alertDialog.setTitle("Time's up!");
            alertDialog.setMessage("Time for answering the questions is up! Your answers were submitted as they were!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(RqGrouppActivity.this, RqGrouppVotingActivity.class);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        });



            alertDialog.show();
        }

    }

}
