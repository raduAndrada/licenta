package android.grouping.utcn.ro.grouping.model.usr;

/**
 * Created by Andrada on 3/10/2018.
 */
public class UsrCreateResult {


    private String usrnm;

    private String email;

    public UsrCreateResult(String email, String usrnm) {
        this.email = email;
        this.usrnm = usrnm;
    }

    public UsrCreateResult() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsrnm() {
        return usrnm;
    }

    public void setUsrnm(String usrnm) {
        this.usrnm = usrnm;
    }
}
