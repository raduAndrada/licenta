package android.grouping.utcn.ro.grouping.adaptors;

import android.content.Context;
import android.grouping.utcn.ro.grouping.R;
import android.grouping.utcn.ro.grouping.model.rsp.Rsp;
import android.grouping.utcn.ro.grouping.model.rsp.RspRecord;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Andrada on 5/6/2018.
 */
public class RgGrouppVotingAdapter extends ArrayAdapter {



    private String studentUsrnm;
    private String rqTp ;
    public RgGrouppVotingAdapter(Context context, List<RspRecord> rspRecords) {
        super(context, 0, rspRecords);


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        if (convertView == null) {

            final RspRecord rspRecord = (RspRecord) getItem(position);
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.rsp_voting_item, parent, false);
            final TextView questionTextView = (TextView) convertView.findViewById(R.id.question_text_rsp_id);
            final RadioGroup radioGroup = (RadioGroup) convertView.findViewById(R.id.rsp_radio_group_id);

            questionTextView.setText(rspRecord.getQuestionText());
            for ( Rsp r: rspRecord.getList() ) {
                final RadioButton rBtn = new RadioButton(convertView.getContext());
                rBtn.setText(r.getRspText());
                radioGroup.addView(rBtn);

            }
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    View radioButton = group.findViewById(checkedId);
                    int radioId = group.indexOfChild(radioButton);
                    rspRecord.setCheckedRspId(radioId);
                }
            });
        }



        // Return the completed view to render on screen
        return convertView;
    }

}
