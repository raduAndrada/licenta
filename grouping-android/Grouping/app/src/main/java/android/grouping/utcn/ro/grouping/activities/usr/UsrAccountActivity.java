package android.grouping.utcn.ro.grouping.activities.usr;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.model.course.StudentCourseGetResult;
import android.grouping.utcn.ro.grouping.model.course.CourseDetail;
import android.grouping.utcn.ro.grouping.adaptors.CourseAdapter;
import android.grouping.utcn.ro.grouping.model.rq.RqGroupp;
import android.grouping.utcn.ro.grouping.model.rq.RqStatus;
import android.grouping.utcn.ro.grouping.model.usr.Usr;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.grouping.utcn.ro.grouping.R;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsrAccountActivity extends AppCompatActivity {


    private static final String USRNM = "usrnm";
    private static final String RQ_GROUPP_ID = "rqGrouppId";
    private static final String SHARED_PREFERENCES = "sharedPreferences";
    public static final String GROUPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPING_REST_BASE_URL + "/v1/students/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);

    private List<CourseDetail> courses= new ArrayList<>();
    private CourseAdapter adapter = null;

    private String usrnm = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usr_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_menu_id);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


        SharedPreferences sharedPref = UsrAccountActivity.this.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.usrnm = sharedPref.getString(USRNM, "none");
        adapter = new CourseAdapter(UsrAccountActivity.this, courses);


        ListView listView = (ListView) findViewById(R.id.usrCoursesList);
        listView.setAdapter(adapter);

        usrService.getAvailableCourses(usrnm).enqueue(new Callback<StudentCourseGetResult>() {
            @Override
            public void onResponse(Call<StudentCourseGetResult> call, Response<StudentCourseGetResult> response) {
                courses.addAll(response.body().getList());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<StudentCourseGetResult> call, Throwable t) {

            }
        });

        adapter.notifyDataSetChanged();
        startChecks(sharedPref);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.main_menu_edit_id) {
            return true;
        }
        if (id == R.id.main_menu_home_id) {
            return true;
        }
        if (id == R.id.main_menu_logout_id) {
            final Usr usr = new Usr();
            usr.setUsrnm(this.usrnm);
            usr.setPsswd("student");
            usrService.logoutUsr(usr).enqueue(new Callback<Usr>() {
                @Override
                public void onResponse(Call<Usr> call, Response<Usr> response) {
                    Intent intent = new Intent(UsrAccountActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<Usr> call, Throwable t) {
                    System.out.print(call);
                }
            });
            return true;
        }

    return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        final Usr usr = new Usr();
        usr.setUsrnm(this.usrnm);
        usr.setPsswd("student");
        usrService.logoutUsr(usr).enqueue(new Callback<Usr>() {
            @Override
            public void onResponse(Call<Usr> call, Response<Usr> response) {
                Intent intent = new Intent(UsrAccountActivity.this, LoginActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Usr> call, Throwable t) {
                System.out.print(call);
            }
        });
    }

    private void startChecks(final SharedPreferences sharedPref){
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                usrService.getRqGrouppForStudent(usrnm).enqueue(new Callback<RqGroupp>() {
                    @Override
                    public void onResponse(Call<RqGroupp> call, Response<RqGroupp> response) {
                       RqGroupp rqGroupp = response.body();
                        if (rqGroupp.getRqStatus() == RqStatus.ONGOING) {
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putLong(RQ_GROUPP_ID, rqGroupp.getId());
                            editor.commit();
                            createAlertDialog();
                            timer.cancel();
                        }

                    }

                    @Override
                    public void onFailure(Call<RqGroupp> call, Throwable t) {

                    }
                });
            }

        }, 0, 1000);
    }

    private void createAlertDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(UsrAccountActivity.this).create();
        alertDialog.setTitle("Course activity");
        alertDialog.setMessage("The groups had been formed. Ready?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(UsrAccountActivity.this, RqGrouppActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
        });
        alertDialog.show();
    }

}
