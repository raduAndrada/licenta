package android.grouping.utcn.ro.grouping.activities.usr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.R;
import android.grouping.utcn.ro.grouping.model.usr.Usr;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.TypefaceProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {


    public static final String GROUPPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPPING_REST_BASE_URL + "/v1/usrs/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);


    private static final String USRNM = "usrnm";
    private static final String PSSWD = "passwd";
    private static final String SHARED_PREFERENCES = "sharedPreferences";
    private String usrnm = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TypefaceProvider.registerDefaultIconSets();
        setContentView(R.layout.activity_login);
        final BootstrapButton loginBtn = (BootstrapButton) findViewById(R.id.login_button_id);
        final EditText usrnmEditText =(EditText) findViewById(R.id.usrnm_login_id);
        final EditText psswdEditText =(EditText) findViewById(R.id.login_psswd_id);
        final TextView createAccountText =(TextView) findViewById(R.id.login_create_account_id);
        final TextView loginFailureText = (TextView) findViewById(R.id.invalid_login_id);
        loginFailureText.setVisibility(View.INVISIBLE);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Usr loginUsr = new Usr(usrnmEditText.getText().toString(), psswdEditText.getText().toString());
                usrService.loginUsr(loginUsr).enqueue(new Callback<Usr>() {
                    @Override
                    public void onResponse(Call<Usr> call, Response<Usr> response) {
                        if (response.isSuccessful()) {
                            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(USRNM,usrnmEditText.getText().toString());
                            editor.putString(PSSWD, psswdEditText.getText().toString());
                            editor.commit();
                            loginFailureText.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(LoginActivity.this, UsrAccountActivity.class);
                            //intent.putExtra(PASSWORD,passwordEditText.getText().toString());
                            usrnm = usrnmEditText.getText().toString();
                            startActivity(intent);

                            // facem logout dupa 3 ore de la login
                            LogoutTimer logoutTimer = new LogoutTimer(180 * 60000, 1000);
                            logoutTimer.start();
                        }
                    }

                    @Override
                    public void onFailure(Call<Usr> call, Throwable t) {

                        loginFailureText.setVisibility(View.VISIBLE);
                    }
                });
                /*
                Usr usr = usrBusiness.loginUsr();
                if (usr == null) {
                    //login failed
                    loginFailureText.setVisibility(View.VISIBLE);
                } else {
                    //login was successful
                    loginFailureText.setVisibility(View.INVISIBLE);

                    //TODO create redirect activity
                    //Intent intent = new Intent(LoginActivity.this, GamificationActivity.class);
                    //intent.putExtra(USRNM, loggedIn);
                    //startActivity(intent);
                }
                */
            }
        });


        createAccountText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent intent = new Intent(LoginActivity.this, CreateUsrActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class LogoutTimer extends CountDownTimer{

        public LogoutTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            final Usr usr = new Usr();
            usr.setUsrnm(usrnm);
            usr.setPsswd("student");
            usrService.logoutUsr(usr).enqueue(new Callback<Usr>() {
                @Override
                public void onResponse(Call<Usr> call, Response<Usr> response) {
                    Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<Usr> call, Throwable t) {
                    System.out.print(call);
                }
            });
        }
    }
}
