package android.grouping.utcn.ro.grouping.model.question;

import java.util.List;

/**
 * Created by Andrada on 5/5/2018.
 */
public class QuestionRecord {

    private Question question;

    private List<QuestionAnswer> questionAnswerList;

    private String qAnswer;


    public QuestionRecord(Question question, List<QuestionAnswer> questionAnswerList) {
        this.question = question;
        this.questionAnswerList = questionAnswerList;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<QuestionAnswer> getQuestionAnswerList() {
        return questionAnswerList;
    }

    public void setQuestionAnswerList(List<QuestionAnswer> questionAnswerList) {
        this.questionAnswerList = questionAnswerList;
    }

    public String getqAnswer() {
        return qAnswer;
    }

    public void setqAnswer(String qAnswer) {
        this.qAnswer = qAnswer;
    }
}
