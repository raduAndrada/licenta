package android.grouping.utcn.ro.grouping.model.rq;

/**
 * Created by Andrada on 5/5/2018.
 */
public class RqGroupp {

    private Long id;

    private Long rqId;

    private RqStatus rqStatus;


    public RqGroupp(RqStatus rqStatus, Long id, Long rqId) {
        this.rqStatus = rqStatus;
        this.id = id;
        this.rqId = rqId;
    }


    public RqStatus getRqStatus() {
        return rqStatus;
    }

    public void setRqStatus(RqStatus rqStatus) {
        this.rqStatus = rqStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRqId() {
        return rqId;
    }

    public void setRqId(Long rqId) {
        this.rqId = rqId;
    }
}
