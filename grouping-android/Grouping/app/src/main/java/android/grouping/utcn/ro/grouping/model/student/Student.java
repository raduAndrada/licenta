package android.grouping.utcn.ro.grouping.model.student;

/**
 * Created by Andrada on 3/31/2018.
 */
public class Student {

    private String usrUsrnm;

    private Long grouppId;

    private String usrEmail;

    private Long lessonGrouppId;


    private Boolean checkedIn;

    public Student(Boolean checkedIn, Long grouppId, Long lessonGrouppId, String usrEmail, String usrUsrnm) {
        this.checkedIn = checkedIn;
        this.grouppId = grouppId;
        this.lessonGrouppId = lessonGrouppId;
        this.usrEmail = usrEmail;
        this.usrUsrnm = usrUsrnm;
    }

    public Student() {
    }

    public Long getGrouppId() {
        return grouppId;
    }

    public void setGrouppId(Long grouppId) {
        this.grouppId = grouppId;
    }

    public String getUsrEmail() {
        return usrEmail;
    }

    public void setUsrEmail(String usrEmail) {
        this.usrEmail = usrEmail;
    }

    public String getUsrUsrnm() {
        return usrUsrnm;
    }

    public void setUsrUsrnm(String usrUsrnm) {
        this.usrUsrnm = usrUsrnm;
    }

    public Long getLessonGrouppId() {
        return lessonGrouppId;
    }

    public void setLessonGrouppId(Long lessonGrouppId) {
        this.lessonGrouppId = lessonGrouppId;
    }

    public Boolean getCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(Boolean checkedIn) {
        this.checkedIn = checkedIn;
    }
}
