package android.grouping.utcn.ro.grouping.adaptors;

import android.content.Context;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.R;
import android.grouping.utcn.ro.grouping.model.course.Course;
import android.grouping.utcn.ro.grouping.model.course.CourseUsr;
import android.grouping.utcn.ro.grouping.model.groupp.Groupp;
import android.grouping.utcn.ro.grouping.model.groupp.GrouppStudent;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.TypefaceProvider;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andrada on 3/10/2018.
 */
public class GrouppAdapter extends ArrayAdapter {

    private static final String USRNM = "usrnm";
    private static final String COURSE_NM = "courseNm";
    private static final String PSSWD = "passwd";
    private static final String SHARED_PREFERENCES = "sharedPreferences";

    public static final String GROUPPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPPING_REST_BASE_URL + "/v1/students/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);

    private String couseNm;

    private String studentUsrnm;
    public GrouppAdapter(Context context, List<Groupp> groupps) {
        super(context, 0, groupps);
        TypefaceProvider.registerDefaultIconSets();
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.studentUsrnm = sharedPref.getString(USRNM, "none");
        final String courseNm = sharedPref.getString(COURSE_NM, "none");


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Groupp groupp = (Groupp) getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.groupp_item, parent, false);
        }
        // Lookup view for data population
        TextView projNm = (TextView) convertView.findViewById(R.id.grouppProjNmTextId);
        final TextView members = (TextView) convertView.findViewById(R.id.grouppMembersTextId);
        Button btnChooseGroupp = (Button) convertView.findViewById(R.id.chooseGrouppBtnId) ;

        // Populate the data into the template view using the data object
        projNm.setText(groupp.getProjNm());
        members.setText(groupp.getStudentsUsrUsrnmsInGroupp().toString());
        final View finalConvertView = convertView;
        btnChooseGroupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO add to group
                members.setText(members.getText() + studentUsrnm);
                usrService.joinGroupp(new GrouppStudent(groupp.getId(), studentUsrnm)).enqueue(new Callback<Integer>() {
                    @Override
                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                        Toast.makeText(finalConvertView.getContext(), "Joined  successful", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Integer> call, Throwable t) {

                    }
                });
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }

}
