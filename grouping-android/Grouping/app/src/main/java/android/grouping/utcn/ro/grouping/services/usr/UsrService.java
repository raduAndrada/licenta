package android.grouping.utcn.ro.grouping.services.usr;

import android.grouping.utcn.ro.grouping.model.course.Course;
import android.grouping.utcn.ro.grouping.model.course.StudentCourseGetResult;
import android.grouping.utcn.ro.grouping.model.course.CourseUsr;
import android.grouping.utcn.ro.grouping.model.groupp.GrouppGetResult;
import android.grouping.utcn.ro.grouping.model.groupp.GrouppStudent;
import android.grouping.utcn.ro.grouping.model.rq.RqGroupp;
import android.grouping.utcn.ro.grouping.model.rq.RqRecord;
import android.grouping.utcn.ro.grouping.model.rsp.Rsp;
import android.grouping.utcn.ro.grouping.model.rsp.RspCreateResultList;
import android.grouping.utcn.ro.grouping.model.rsp.RspCreateList;
import android.grouping.utcn.ro.grouping.model.rsp.RspGetResult;
import android.grouping.utcn.ro.grouping.model.usr.Usr;
import android.grouping.utcn.ro.grouping.model.usr.UsrCreateResult;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Andrada on 3/10/2018.
 */
public interface UsrService {

    @POST("login")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<Usr> loginUsr(@Body Usr Usr);

    @POST("logout")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<Usr> logoutUsr(@Body Usr usr);


    @POST("create")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<UsrCreateResult> createUsr(@Body Usr Usr);

    @GET("courses/{usrnm}")
    @Headers({
            "Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<StudentCourseGetResult> getMyCourses(@Path("usrnm") String usrnm);

    @GET("courses/{usrnm}")
    @Headers({
            "Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<StudentCourseGetResult> getAvailableCourses(@Path("usrnm") String usrnm);

    @POST("courses")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<Course> enrollStudent(@Body CourseUsr courseUsr);

    @GET("groups/{courseNm}/semester-projects")
    @Headers({
            "Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<GrouppGetResult> getGroupsForSemProjByChoice(@Path("courseNm") String courseNm);

    @POST("join-groupp")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<Integer> joinGroupp(@Body GrouppStudent grouppStudent);

    @GET("request-group/{usrnm}")
    @Headers({
            "Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<RqGroupp> getRqGrouppForStudent(@Path("usrnm") String usrnm);

    @GET("request/{usrnm}")
    @Headers({
            "Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<RqRecord> getRqForStudent (@Path("usrnm") String usrnm);

    @POST("response")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<RspCreateResultList> sendRsps(@Body RspCreateList rspCreateList);


    @GET("response/{rqGrouppId}")
    @Headers({
            "Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<RspGetResult> getRspsForRqGroupp (@Path("rqGrouppId") Long rqGrouppId);

    @PUT("response/vote")
    @Headers({"Content-Type: application/json",
            "X-Authorization: student"
    })
    Call<Rsp> voteRsp(@Body RspCreateList rspCreateList);

}
