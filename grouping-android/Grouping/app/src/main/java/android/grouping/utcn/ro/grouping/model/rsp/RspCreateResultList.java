package android.grouping.utcn.ro.grouping.model.rsp;

import java.util.List;

/**
 * Created by Andrada on 5/6/2018.
 */
public class RspCreateResultList {

    private List<RspCreateResult> list;


    public RspCreateResultList(List<RspCreateResult> list) {
        this.list = list;
    }

    public List<RspCreateResult> getList() {
        return list;
    }

    public void setList(List<RspCreateResult> list) {
        this.list = list;
    }
}
