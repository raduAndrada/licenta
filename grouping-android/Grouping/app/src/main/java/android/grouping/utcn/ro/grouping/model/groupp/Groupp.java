package android.grouping.utcn.ro.grouping.model.groupp;

import java.util.List;

/**
 * Created by Andrada on 3/31/2018.
 */
public class Groupp {

    private Long id;

    private Integer size;

    private String projNm;

    private String leaderUsrnm;

    private Integer mandate;

    private String courseNm;

    private GrouppTp grouppTp;

    private List<String> studentsUsrUsrnmsInGroupp;

    public Groupp(String courseNm, Long id, GrouppTp grouppTp, String leaderUsrnm, Integer mandate, String projNm, Integer size, List<String> studentsUsrUsrnmsInGroupp) {
        this.courseNm = courseNm;
        this.id = id;
        this.grouppTp = grouppTp;
        this.leaderUsrnm = leaderUsrnm;
        this.mandate = mandate;
        this.projNm = projNm;
        this.size = size;
        this.studentsUsrUsrnmsInGroupp = studentsUsrUsrnmsInGroupp;
    }

    public Groupp() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseNm() {
        return courseNm;
    }

    public void setCourseNm(String courseNm) {
        this.courseNm = courseNm;
    }

    public GrouppTp getGrouppTp() {
        return grouppTp;
    }

    public void setGrouppTp(GrouppTp grouppTp) {
        this.grouppTp = grouppTp;
    }

    public String getLeaderUsrnm() {
        return leaderUsrnm;
    }

    public void setLeaderUsrnm(String leaderUsrnm) {
        this.leaderUsrnm = leaderUsrnm;
    }

    public Integer getMandate() {
        return mandate;
    }

    public void setMandate(Integer mandate) {
        this.mandate = mandate;
    }

    public String getProjNm() {
        return projNm;
    }

    public void setProjNm(String projNm) {
        this.projNm = projNm;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<String> getStudentsUsrUsrnmsInGroupp() {
        return studentsUsrUsrnmsInGroupp;
    }

    public void setStudentsUsrUsrnmsInGroupp(List<String> studentsUsrUsrnmsInGroupp) {
        this.studentsUsrUsrnmsInGroupp = studentsUsrUsrnmsInGroupp;
    }
}

