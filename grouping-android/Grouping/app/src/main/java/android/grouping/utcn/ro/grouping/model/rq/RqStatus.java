package android.grouping.utcn.ro.grouping.model.rq;

/**
 * Created by Andrada on 5/5/2018.
 */
public enum RqStatus {
    OPENED, ONGOING, PENDING, TERMINATED
}
