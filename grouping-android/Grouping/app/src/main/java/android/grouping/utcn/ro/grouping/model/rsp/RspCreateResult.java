package android.grouping.utcn.ro.grouping.model.rsp;

/**
 * Created by Andrada on 5/6/2018.
 */
public class RspCreateResult {

    private String rspText;


    private Long rqGrouppId;

    public RspCreateResult(Long rqGrouppId, String rspText) {
        this.rqGrouppId = rqGrouppId;
        this.rspText = rspText;
    }

    public Long getRqGrouppId() {
        return rqGrouppId;
    }

    public void setRqGrouppId(Long rqGrouppId) {
        this.rqGrouppId = rqGrouppId;
    }

    public String getRspText() {
        return rspText;
    }

    public void setRspText(String rspText) {
        this.rspText = rspText;
    }
}
