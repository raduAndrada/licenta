package android.grouping.utcn.ro.grouping.activities.usr;

import android.grouping.utcn.ro.grouping.model.usr.Usr;
import android.grouping.utcn.ro.grouping.model.usr.UsrCreateResult;
import android.grouping.utcn.ro.grouping.model.usr.UsrTp;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.grouping.utcn.ro.grouping.R;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.TypefaceProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateUsrActivity extends AppCompatActivity {

    public static final String GROUPPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPPING_REST_BASE_URL + "/v1/usrs/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_usr);
        TypefaceProvider.registerDefaultIconSets();

        final EditText usrnmEditText = (EditText) findViewById(R.id.usrCreateUsrnmEditTextId);
        final EditText emailEditText = (EditText) findViewById(R.id.usrCreateEmailEditTextId);
        final EditText fnmEditText = (EditText) findViewById(R.id.usrCreateFnmEditTextId);
        final EditText lnmEditText = (EditText) findViewById(R.id.usrCreateLnmEditTextId);
        final EditText psswdEditText = (EditText) findViewById(R.id.usrCreatePsswdEditTextId);



        BootstrapButton createUsrBtn = (BootstrapButton) findViewById(R.id.usrCreateCreateAccountBtnId);

        createUsrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String usrnm = usrnmEditText.getText().toString();
                final String email = emailEditText.getText().toString();
                final String psswd = fnmEditText.getText().toString();
                final String fNm = lnmEditText.getText().toString();
                final String lNm = psswdEditText.getText().toString();
                final Usr usrToCreate =  new Usr(email, fNm, lNm, psswd, usrnm, UsrTp.STUDENT);

                usrService.createUsr(usrToCreate).enqueue(new Callback<UsrCreateResult>() {
                    @Override
                    public void onResponse(Call<UsrCreateResult> call, Response<UsrCreateResult> response) {
                        Toast.makeText(getApplicationContext(), "Successfully registered", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<UsrCreateResult> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "No user created", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });



    }

}
