package android.grouping.utcn.ro.grouping.model.rq;

import android.grouping.utcn.ro.grouping.model.question.QuestionRecord;

import java.util.List;

/**
 * Created by Andrada on 5/5/2018.
 */
public class RqRecord {

    private Rq rq;

    private List<QuestionRecord> questions;

    public RqRecord(List<QuestionRecord> questions, Rq rq) {
        this.questions = questions;
        this.rq = rq;
    }

    public List<QuestionRecord> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionRecord> questions) {
        this.questions = questions;
    }

    public Rq getRq() {
        return rq;
    }

    public void setRq(Rq rq) {
        this.rq = rq;
    }
}
