package android.grouping.utcn.ro.grouping.model.groupp;

/**
 * Created by Andrada on 3/31/2018.
 */
public class GrouppStudent {

    private Long gouppId;

    private String usrUsrnm;


    public GrouppStudent(Long gouppId, String usrUsrnm) {
        this.gouppId = gouppId;
        this.usrUsrnm = usrUsrnm;
    }

    public GrouppStudent() {
    }

    public String getUsrUsrnm() {
        return usrUsrnm;
    }

    public void setUsrUsrnm(String usrUsrnm) {
        this.usrUsrnm = usrUsrnm;
    }

    public Long getGouppId() {
        return gouppId;
    }

    public void setGouppId(Long gouppId) {
        this.gouppId = gouppId;
    }
}
