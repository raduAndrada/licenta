package android.grouping.utcn.ro.grouping.model.rq;

/**
 * Created by Andrada on 5/5/2018.
 */
public class Rq {

    private Long id;

    private Integer allowedTm;

    private Long lessonId;


    private Integer maxRsp;

    private RqTp rqTp;

    // TIME MEASUREMENT

    private TimeMeasurementTp timeMeasurementTp;

    // when a rq is active it starts the grouping
    private RqStatus rqStatus;

    private RqGrouppTp rRqGrouppTp;

    private Integer rqGrouppSz;


    public Rq(Integer allowedTm, Long id, Long lessonId, Integer maxRsp, Integer rqGrouppSz, RqStatus rqStatus, RqTp rqTp, RqGrouppTp rRqGrouppTp, TimeMeasurementTp timeMeasurementTp) {
        this.allowedTm = allowedTm;
        this.id = id;
        this.lessonId = lessonId;
        this.maxRsp = maxRsp;
        this.rqGrouppSz = rqGrouppSz;
        this.rqStatus = rqStatus;
        this.rqTp = rqTp;
        this.rRqGrouppTp = rRqGrouppTp;
        this.timeMeasurementTp = timeMeasurementTp;
    }

    public Integer getAllowedTm() {
        return allowedTm;
    }

    public void setAllowedTm(Integer allowedTm) {
        this.allowedTm = allowedTm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Integer getMaxRsp() {
        return maxRsp;
    }

    public void setMaxRsp(Integer maxRsp) {
        this.maxRsp = maxRsp;
    }

    public Integer getRqGrouppSz() {
        return rqGrouppSz;
    }

    public void setRqGrouppSz(Integer rqGrouppSz) {
        this.rqGrouppSz = rqGrouppSz;
    }

    public RqStatus getRqStatus() {
        return rqStatus;
    }

    public void setRqStatus(RqStatus rqStatus) {
        this.rqStatus = rqStatus;
    }

    public RqTp getRqTp() {
        return rqTp;
    }

    public void setRqTp(RqTp rqTp) {
        this.rqTp = rqTp;
    }

    public RqGrouppTp getrRqGrouppTp() {
        return rRqGrouppTp;
    }

    public void setrRqGrouppTp(RqGrouppTp rRqGrouppTp) {
        this.rRqGrouppTp = rRqGrouppTp;
    }

    public TimeMeasurementTp getTimeMeasurementTp() {
        return timeMeasurementTp;
    }

    public void setTimeMeasurementTp(TimeMeasurementTp timeMeasurementTp) {
        this.timeMeasurementTp = timeMeasurementTp;
    }
}
