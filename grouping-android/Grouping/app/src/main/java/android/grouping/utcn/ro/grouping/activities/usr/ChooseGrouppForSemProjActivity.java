package android.grouping.utcn.ro.grouping.activities.usr;

import android.content.Context;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.model.groupp.Groupp;
import android.grouping.utcn.ro.grouping.adaptors.GrouppAdapter;
import android.grouping.utcn.ro.grouping.model.groupp.GrouppGetResult;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.grouping.utcn.ro.grouping.R;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChooseGrouppForSemProjActivity extends AppCompatActivity {


    private static final String USRNM = "usrnm";
    private static final String SHARED_PREFERENCES = "sharedPreferences";
    public static final String GROUPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String USRS_URL= GROUPING_REST_BASE_URL + "/v1/students/";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);
    private static final String COURSE_NM = "courseNm";

    private List<Groupp> groupps= new ArrayList<>();
    private GrouppAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_groupp_for_sem_proj);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_menu_id);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        SharedPreferences sharedPref = ChooseGrouppForSemProjActivity.this.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        final String usrnm = sharedPref.getString(USRNM, "none");
        final String courseNm = sharedPref.getString(COURSE_NM, "none");
        adapter = new GrouppAdapter(ChooseGrouppForSemProjActivity.this, groupps);


        ListView listView = (ListView) findViewById(R.id.grouppsForSemProjId);
        listView.setAdapter(adapter);

        usrService.getGroupsForSemProjByChoice(courseNm).enqueue(new Callback<GrouppGetResult>() {
            @Override
            public void onResponse(Call<GrouppGetResult> call, Response<GrouppGetResult> response) {
                groupps.addAll(response.body().getList());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<GrouppGetResult> call, Throwable t) {

            }
        });

        adapter.notifyDataSetChanged();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }




}
