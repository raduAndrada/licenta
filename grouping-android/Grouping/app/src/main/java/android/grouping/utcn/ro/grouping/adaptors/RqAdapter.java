package android.grouping.utcn.ro.grouping.adaptors;

import android.content.Context;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.R;
import android.grouping.utcn.ro.grouping.model.groupp.Groupp;
import android.grouping.utcn.ro.grouping.model.groupp.GrouppStudent;
import android.grouping.utcn.ro.grouping.model.question.QuestionAnswer;
import android.grouping.utcn.ro.grouping.model.question.QuestionRecord;
import android.grouping.utcn.ro.grouping.model.rq.RqTp;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;



/**
 * Created by Andrada on 5/6/2018.
 */
public class RqAdapter extends ArrayAdapter{
    private static final String USRNM = "usrnm";
    private static final String RQ_ID = "rqId";
    private static final String RQ_TP = "rqTp";
    private static final String PSSWD = "passwd";
    private static final String SHARED_PREFERENCES = "sharedPreferences";


    private String studentUsrnm;
    private String rqTp ;
    public RqAdapter(Context context, List<QuestionRecord> questionRecordList) {
        super(context, 0, questionRecordList);
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.studentUsrnm = sharedPref.getString(USRNM, "none");
        final Long rqId = sharedPref.getLong(RQ_ID, 0L);
        this.rqTp = sharedPref.getString(RQ_TP,"none");

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            // we check if the request is type choice or text filling
            // we have different items to attach for eac
            if (rqTp.equals(RqTp.CHOICE.toString())) {
                final QuestionRecord questionRecord = (QuestionRecord) getItem(position);
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.question_choice_item, parent, false);
                TextView questionChoiceTextView = (TextView) convertView.findViewById(R.id.question_choice_text_id);
                questionChoiceTextView.setText(questionRecord.getQuestion().getQuestionText());
                List<QuestionAnswer> qaList = questionRecord.getQuestionAnswerList();

                CheckBox checkBox1 = (CheckBox) convertView.findViewById(R.id.checkBox_choice_1);
                CheckBox checkBox2 = (CheckBox) convertView.findViewById(R.id.checkBox_choice_2);
                CheckBox checkBox3 = (CheckBox) convertView.findViewById(R.id.checkBox_choice_3);
                CheckBox checkBox4 = (CheckBox) convertView.findViewById(R.id.checkBox_choice_4);
                CheckBox checkBox5 = (CheckBox) convertView.findViewById(R.id.checkBox_choice_5);

                checkBox1.setText(qaList.get(0).getAnswerText());
                checkBox2.setText(qaList.get(1).getAnswerText());
                checkBox3.setText(qaList.get(2).getAnswerText());
                checkBox4.setText(qaList.get(3).getAnswerText());
                checkBox5.setText(qaList.get(4).getAnswerText());

            } else {
                final QuestionRecord questionRecord = (QuestionRecord) getItem(position);

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.question_fill_item, parent, false);
                TextView questionFillTextView = (TextView)  convertView.findViewById(R.id.question_fill_text_id);
                questionFillTextView.setText(questionRecord.getQuestion().getQuestionText());
                EditText et = (EditText)  convertView.findViewById(R.id.question_fill_answer_text_id);
                et.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        questionRecord.setqAnswer(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                //et.setText("");
            }

        }
        if (!rqTp.equals(RqTp.CHOICE.toString())) {

            final QuestionRecord questionRecord = (QuestionRecord) getItem(position);
            TextView questionFillTextView = (TextView)  convertView.findViewById(R.id.question_fill_text_id);
            questionFillTextView.setText(questionRecord.getQuestion().getQuestionText());
            EditText et = (EditText)  convertView.findViewById(R.id.question_fill_answer_text_id);
            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    questionRecord.setqAnswer(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        return convertView;
    }


}
