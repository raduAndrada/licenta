package android.grouping.utcn.ro.grouping.model.course;

/**
 * Created by Andrada on 3/25/2018.
 */
public class CourseUsr {

    private String usrUsrnm;
    private String courseNm;

    public CourseUsr(String courseNm, String usrUsrnm) {
        this.courseNm = courseNm;
        this.usrUsrnm = usrUsrnm;
    }

    public String getCourseNm() {
        return courseNm;
    }

    public void setCourseNm(String courseNm) {
        this.courseNm = courseNm;
    }

    public String getUsrUsrnm() {
        return usrUsrnm;
    }

    public void setUsrUsrnm(String usrUsrnm) {
        this.usrUsrnm = usrUsrnm;
    }
}
