package android.grouping.utcn.ro.grouping.model.rsp;

import java.util.List;

/**
 * Created by Andrada on 5/6/2018.
 */
public class RspGetResult {

    private List<RspRecord> list;

    public RspGetResult(List<RspRecord> list) {
        this.list = list;
    }

    public List<RspRecord> getList() {
        return list;
    }

    public void setList(List<RspRecord> list) {
        this.list = list;
    }
}
