package android.grouping.utcn.ro.grouping.model.rsp;

import java.util.List;

/**
 * Created by Andrada on 5/6/2018.
 */
public class RspRecord {

    private String questionText;

    private List<Rsp> list;

    int checkedRspId;


    public RspRecord(List<Rsp> list, String questionText) {
        this.list = list;
        this.questionText = questionText;
    }

    public List<Rsp> getList() {
        return list;
    }

    public void setList(List<Rsp> list) {
        this.list = list;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public int getCheckedRspId() {
        return checkedRspId;
    }

    public void setCheckedRspId(int checkedRspId) {
        this.checkedRspId = checkedRspId;
    }
}
