package android.grouping.utcn.ro.grouping.model.question;

import java.util.List;

/**
 * Created by Andrada on 5/5/2018.
 */
public class QuestionGetResult {

    private List<QuestionRecord> list;

    public QuestionGetResult(List<QuestionRecord> list) {
        this.list = list;
    }

    public List<QuestionRecord> getList() {
        return list;
    }

    public void setList(List<QuestionRecord> list) {
        this.list = list;
    }
}
