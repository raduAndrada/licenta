package android.grouping.utcn.ro.grouping.activities.usr;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.grouping.utcn.ro.grouping.adaptors.RgGrouppVotingAdapter;
import android.grouping.utcn.ro.grouping.model.rq.RqTp;
import android.grouping.utcn.ro.grouping.model.rsp.Rsp;
import android.grouping.utcn.ro.grouping.model.rsp.RspCreateList;
import android.grouping.utcn.ro.grouping.model.rsp.RspGetResult;
import android.grouping.utcn.ro.grouping.model.rsp.RspRecord;
import android.grouping.utcn.ro.grouping.services.usr.UsrService;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.grouping.utcn.ro.grouping.R;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.TypefaceProvider;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RqGrouppVotingActivity extends AppCompatActivity {


    private static final String SHARED_PREFERENCES = "sharedPreferences";
    public static final String GROUPING_REST_BASE_URL ="http://192.168.0.115:8000";
    private static final String RQ_GROUPP_ID = "rqGrouppId";

    private static final String USRS_URL= GROUPING_REST_BASE_URL + "/v1/students/";
    private static final String ALLOWED_TIME= "allowedTime";
    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(USRS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private static final UsrService usrService = retrofit.create(UsrService.class);

    private List<RspRecord> rspList= new ArrayList<>();

    private RgGrouppVotingAdapter adapter = null;
    private Long rqGrouppId;
    private List<Rsp> selectedRsps = new ArrayList<>();
    private Toolbar toolbar;
    private int allowedTm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TypefaceProvider.registerDefaultIconSets();
        setContentView(R.layout.activity_rq_groupp_voting);

        toolbar = (Toolbar) findViewById(R.id.main_menu_id);
        toolbar.setTitle("Time");
        setSupportActionBar(toolbar);

        final BootstrapButton voteBtn= (BootstrapButton ) findViewById(R.id.submit_vote_for_rsp_btn_id);

        final SharedPreferences sharedPref = RqGrouppVotingActivity.this.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        rqGrouppId= sharedPref.getLong(RQ_GROUPP_ID, 0L);
        allowedTm = sharedPref.getInt(ALLOWED_TIME, 10000);

        usrService.getRspsForRqGroupp(rqGrouppId).enqueue(new Callback<RspGetResult>() {
            @Override
            public void onResponse(Call<RspGetResult> call, Response<RspGetResult> response) {
                rspList.addAll(response.body().getList());
                adapter = new RgGrouppVotingAdapter(RqGrouppVotingActivity.this, rspList);
                ListView listView = (ListView) findViewById(R.id.rsp_voting_list_id);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                final RspVotingCountDownTimer rspVotingCountDownTimer = new RspVotingCountDownTimer(allowedTm, 1000);
                rspVotingCountDownTimer.start();
                voteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View view;
                        RadioGroup radioGroup;
                        for (int i = 0; i < adapter.getCount(); i++) {
                            RspRecord rspRecord = (RspRecord) adapter.getItem(i);
                            view = adapter.getView(i, null, null);
                            radioGroup = (RadioGroup) view.findViewById(R.id.rsp_radio_group_id);
                            int selectedId = rspRecord.getCheckedRspId();
                            // find the radiobutton by returned id
                            RadioButton radioButton = (RadioButton) radioGroup.getChildAt(selectedId);
                            for (Rsp r : rspRecord.getList()) {
                                if (r.getRspText() != null && r.getRspText().equals(radioButton.getText().toString())) {
                                    if (!selectedRsps.contains(r)) {
                                        selectedRsps.add(r);
                                    }
                                }
                            }

                        }
                        rspVotingCountDownTimer.onFinish();
                    }
                });
            }

            @Override
            public void onFailure(Call<RspGetResult> call, Throwable t) {

            }
        });



    }

    private class RspVotingCountDownTimer extends CountDownTimer {

        public RspVotingCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

            toolbar.setTitle("Time's up!");
            createAlertDialog();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            toolbar.setTitle("Time: " + millisUntilFinished / 1000 + " s");

        }
        private void createAlertDialog(){
            AlertDialog alertDialog = new AlertDialog.Builder(RqGrouppVotingActivity.this).create();
            alertDialog.setTitle("Time's up!");
            alertDialog.setMessage("Time for voting is up!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, int which) {

                            usrService.voteRsp(new RspCreateList(selectedRsps)).enqueue(new Callback<Rsp>() {
                                @Override
                                public void onResponse(Call<Rsp> call, Response<Rsp> response) {

                                    Toast.makeText(getApplicationContext(), "Vote has been added", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                    Intent intent = new Intent(RqGrouppVotingActivity.this, UsrAccountActivity.class);
                                    startActivity(intent);

                                }

                                @Override
                                public void onFailure(Call<Rsp> call, Throwable t) {

                                }
                            });

                        }
                    });



            alertDialog.show();
        }
    }

}
