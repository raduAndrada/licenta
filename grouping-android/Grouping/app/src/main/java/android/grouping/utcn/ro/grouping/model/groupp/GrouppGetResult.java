package android.grouping.utcn.ro.grouping.model.groupp;

import android.grouping.utcn.ro.grouping.model.course.Course;
import android.grouping.utcn.ro.grouping.model.student.Student;

import java.util.List;

/**
 * Created by Andrada on 3/10/2018.
 */
public class GrouppGetResult {

    private List<Groupp> list;

    private List<Student> studentsInGroupp;

    public GrouppGetResult(List<Groupp> list, List<Student> studentsInGroupp) {
        this.list = list;
        this.studentsInGroupp = studentsInGroupp;
    }

    public GrouppGetResult() {
    }

    public List<Groupp> getList() {
        return list;
    }

    public void setList(List<Groupp> list) {
        this.list = list;
    }

    public List<Student> getStudentsInGroupp() {
        return studentsInGroupp;
    }

    public void setStudentsInGroupp(List<Student> studentsInGroupp) {
        this.studentsInGroupp = studentsInGroupp;
    }
}
