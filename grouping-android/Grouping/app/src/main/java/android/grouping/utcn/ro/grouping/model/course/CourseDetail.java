package android.grouping.utcn.ro.grouping.model.course;

/**
 * Created by Andrada on 5/5/2018.
 */
public class CourseDetail {


    private Course course;
    private boolean enrolled;

    public CourseDetail(Course course, boolean enrolled) {
        this.course = course;
        this.enrolled = enrolled;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public boolean isEnrolled() {
        return enrolled;
    }

    public void setEnrolled(boolean enrolled) {
        this.enrolled = enrolled;
    }
}
