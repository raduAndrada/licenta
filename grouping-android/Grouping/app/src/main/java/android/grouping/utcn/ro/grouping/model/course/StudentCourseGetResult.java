package android.grouping.utcn.ro.grouping.model.course;

import android.os.CountDownTimer;

import java.util.List;

/**
 * Created by Andrada on 3/10/2018.
 */
public class StudentCourseGetResult {

    private List<CourseDetail> list;

    public StudentCourseGetResult(List<CourseDetail> list) {
        this.list = list;
    }

    public StudentCourseGetResult() {
    }

    public List<CourseDetail> getList() {
        return list;
    }

    public void setList(List<CourseDetail> list) {
        this.list = list;
    }
}
