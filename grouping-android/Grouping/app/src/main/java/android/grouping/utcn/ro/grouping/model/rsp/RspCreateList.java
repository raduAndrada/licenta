package android.grouping.utcn.ro.grouping.model.rsp;

import java.util.List;

/**
 * Created by Andrada on 5/6/2018.
 */
public class RspCreateList {

    private List<Rsp> list;

    public RspCreateList(List<Rsp> list) {
        this.list = list;
    }

    public List<Rsp> getList() {
        return list;
    }

    public void setList(List<Rsp> list) {
        this.list = list;
    }
}
