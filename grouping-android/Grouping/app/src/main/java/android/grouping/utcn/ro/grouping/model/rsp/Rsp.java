package android.grouping.utcn.ro.grouping.model.rsp;

/**
 * Created by Andrada on 5/5/2018.
 */
public class Rsp {

    private Long id;

    private Long rqGrouppId;

    private Long questionId;

    private String studentUsrnm;

    private String rspText;

    public Rsp(Long id, Long questionId, Long rqGrouppId, String rspText, String studentUsrnm) {
        this.id = id;
        this.questionId = questionId;
        this.rqGrouppId = rqGrouppId;
        this.rspText = rspText;
        this.studentUsrnm = studentUsrnm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getRqGrouppId() {
        return rqGrouppId;
    }

    public void setRqGrouppId(Long rqGrouppId) {
        this.rqGrouppId = rqGrouppId;
    }

    public String getRspText() {
        return rspText;
    }

    public void setRspText(String rspText) {
        this.rspText = rspText;
    }

    public String getStudentUsrnm() {
        return studentUsrnm;
    }

    public void setStudentUsrnm(String studentUsrnm) {
        this.studentUsrnm = studentUsrnm;
    }
}
