package android.grouping.utcn.ro.grouping.model.question;

/**
 * Created by Andrada on 5/5/2018.
 */
public class Question {

    private Long id;

    private Long rqId;

    private String questionText;

    public Question(Long id, String questionText, Long rqId) {
        this.id = id;
        this.questionText = questionText;
        this.rqId = rqId;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getRqId() {
        return rqId;
    }

    public void setRqId(Long rqId) {
        this.rqId = rqId;
    }
}
