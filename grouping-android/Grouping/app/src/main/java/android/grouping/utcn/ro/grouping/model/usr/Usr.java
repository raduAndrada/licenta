package android.grouping.utcn.ro.grouping.model.usr;

/**
 * Created by Andrada on 3/10/2018.
 */
public class Usr {

    private String usrnm;

    private String psswd;

    private String email;

    private String fNm;

    private String lNm;


    private UsrTp usrTp;


    public Usr() {
    }

    public Usr(String email, String fNm, String lNm, String psswd, String usrnm, UsrTp usrTp) {
        this.email = email;
        this.fNm = fNm;
        this.lNm = lNm;
        this.psswd = psswd;
        this.usrnm = usrnm;
        this.usrTp = usrTp;

    }


    public Usr(String psswd, String usrnm) {
        this.psswd = psswd;
        this.usrnm = usrnm;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getfNm() {
        return fNm;
    }

    public void setfNm(String fNm) {
        this.fNm = fNm;
    }

    public String getlNm() {
        return lNm;
    }

    public void setlNm(String lNm) {
        this.lNm = lNm;
    }

    public String getPsswd() {
        return psswd;
    }

    public void setPsswd(String psswd) {
        this.psswd = psswd;
    }

    public String getUsrnm() {
        return usrnm;
    }

    public void setUsrnm(String usrnm) {
        this.usrnm = usrnm;
    }

    public UsrTp getUsrTp() {
        return usrTp;
    }

    public void setUsrTp(UsrTp usrTp) {
        this.usrTp = usrTp;
    }
}
