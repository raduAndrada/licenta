package android.grouping.utcn.ro.grouping.model.course;

/**
 * Created by Andrada on 3/10/2018.
 */
public class Course {

    private String nm;

    private Integer studentsCount;

    private String dsc;


    public Course(String nm) {
        this.nm = nm;
    }

    public Course() {
    }

    public Course(String dsc, String nm, Integer studentsCount) {
        this.dsc = dsc;
        this.nm = nm;
        this.studentsCount = studentsCount;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }

    public void setStudentsCount(Integer studentsCount) {
        this.studentsCount = studentsCount;
    }
}
