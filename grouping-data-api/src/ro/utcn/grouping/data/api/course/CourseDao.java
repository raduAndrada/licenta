package ro.utcn.grouping.data.api.course;

import java.util.List;

import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseRecord;

public interface CourseDao {

	int addCourse(CourseRecord add);

	int deleteCourse(CourseEntityDelete courseNm);

	int updateCourse(CourseEntityUpdate update);

	List<Course> getCourse(CourseEntityGet get);
}
