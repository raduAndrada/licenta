package ro.utcn.grouping.data.api.course;

import java.util.List;

import ro.utcn.grouping.model.course.CourseUsr;

public interface CourseUsrDao {

	int addCourseUsr(CourseUsr add);

	List<CourseUsr> getCourseUsr(CourseUsrEntityGet get);
}
