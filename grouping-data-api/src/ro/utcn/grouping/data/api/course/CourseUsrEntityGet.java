package ro.utcn.grouping.data.api.course;

import org.immutables.value.Value;

import ro.utcn.grouping.model.course.CourseUsrCriteria;

@Value.Immutable
@Value.Modifiable
public interface CourseUsrEntityGet {

	CourseUsrCriteria getCriteria();
}
