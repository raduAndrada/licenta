package ro.utcn.grouping.data.api.course;

import org.immutables.value.Value;

import ro.utcn.grouping.model.course.CourseCriteria;

@Value.Immutable
@Value.Modifiable
public interface CourseEntityGet {

	CourseCriteria getCriteria();
}
