package ro.utcn.grouping.data.api.course;

import org.immutables.value.Value;

import ro.utcn.grouping.model.course.CourseCriteria;

@Value.Immutable
public interface CourseEntityUpdate {

	Integer getStudentsCount();

	String getDsc();

	CourseCriteria getCriteria();
}
