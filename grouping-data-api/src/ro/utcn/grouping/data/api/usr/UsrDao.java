package ro.utcn.grouping.data.api.usr;

import java.util.List;

import ro.utcn.grouping.model.usr.Usr;

public interface UsrDao {

	int addUsr(Usr add);

	List<Usr> getUsr(UsrEntityGet get);
}
