package ro.utcn.grouping.data.api.usr;

import org.immutables.value.Value;

import ro.utcn.grouping.model.usr.UsrCriteria;

@Value.Immutable
@Value.Modifiable
public interface UsrEntityGet {

	UsrCriteria getCriteria();
}
