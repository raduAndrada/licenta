package ro.utcn.grouping.data.api.rsp;

import org.immutables.value.Value;

import ro.utcn.grouping.model.rsp.RspCriteria;

@Value.Immutable
public interface RspEntityUpdate {

	Integer getVoteCount();

	RspCriteria getCriteria();

}
