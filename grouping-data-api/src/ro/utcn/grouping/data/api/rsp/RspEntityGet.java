package ro.utcn.grouping.data.api.rsp;

import org.immutables.value.Value;

import ro.utcn.grouping.model.rsp.RspCriteria;

@Value.Immutable
@Value.Modifiable
public interface RspEntityGet {

	RspCriteria getCriteria();
}
