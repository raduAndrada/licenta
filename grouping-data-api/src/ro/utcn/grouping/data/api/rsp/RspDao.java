package ro.utcn.grouping.data.api.rsp;

import java.util.List;

import ro.utcn.grouping.model.rsp.Rsp;

public interface RspDao {

	int addRsp(Rsp add);

	List<Rsp> getRsp(RspEntityGet get);

	int updateRsp(RspEntityUpdate rspEntityUpdate);
}
