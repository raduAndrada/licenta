package ro.utcn.grouping.data.api.question;

import java.util.List;

import ro.utcn.grouping.model.question.Question;

public interface QuestionDao {

	int addQuestion(Question question);

	int updateQuestion(QuestionEntityUpdate questionEntityUpdate);

	int deleteQuestion(QuestionEntityDelete questionEntityDelete);

	List<Question> getQuestion(QuestionEntityGet questionEntityGet);
}
