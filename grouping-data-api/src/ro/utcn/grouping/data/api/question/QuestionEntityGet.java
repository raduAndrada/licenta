package ro.utcn.grouping.data.api.question;

import org.immutables.value.Value;

import ro.utcn.grouping.model.question.QuestionCriteria;

@Value.Immutable
@Value.Modifiable
public interface QuestionEntityGet {

	QuestionCriteria getCriteria();
}
