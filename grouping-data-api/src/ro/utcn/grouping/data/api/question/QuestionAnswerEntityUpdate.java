package ro.utcn.grouping.data.api.question;

import org.immutables.value.Value;

import ro.utcn.grouping.model.question.QuestionAnswerCriteria;

@Value.Immutable
public interface QuestionAnswerEntityUpdate {

	String getAnswerText();

	Boolean getCorrect();

	QuestionAnswerCriteria getCriteria();

}
