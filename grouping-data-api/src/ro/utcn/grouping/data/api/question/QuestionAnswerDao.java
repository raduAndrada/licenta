package ro.utcn.grouping.data.api.question;

import java.util.List;

import ro.utcn.grouping.model.question.QuestionAnswer;

public interface QuestionAnswerDao {

	int addQuestionAnswer(QuestionAnswer questionAnswer);

	int updateQuestionAnswer(QuestionAnswerEntityUpdate questionAnswerEntityUpdate);

	int deleteQuestionAnswer(QuestionAnswerEntityDelete questionAnswerEntityDelete);

	List<QuestionAnswer> getQuestionAnswer(QuestionAnswerEntityGet questionAnswerEntityGet);
}
