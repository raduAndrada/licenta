package ro.utcn.grouping.data.api.proffessor;

import org.immutables.value.Value;

import ro.utcn.grouping.model.proffesor.ProffessorCriteria;

@Value.Immutable
@Value.Modifiable
public interface ProffessorEntityGet {

	ProffessorCriteria getCriteria();
}
