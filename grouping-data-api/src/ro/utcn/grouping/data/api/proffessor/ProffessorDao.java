package ro.utcn.grouping.data.api.proffessor;

import java.util.List;

import ro.utcn.grouping.model.proffesor.Proffessor;

public interface ProffessorDao {

	int addProffessor(Proffessor proffessor);

	List<Proffessor> getProffessor(ProffessorEntityGet proffessorEntityGet);
}
