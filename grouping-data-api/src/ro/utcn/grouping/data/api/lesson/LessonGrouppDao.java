package ro.utcn.grouping.data.api.lesson;

import java.util.List;

import ro.utcn.grouping.model.lesson.LessonGroupp;

public interface LessonGrouppDao {

	int addLessonGroupp(LessonGroupp add);

	List<LessonGroupp> getLesson(LessonGrouppEntityGet get);
}
