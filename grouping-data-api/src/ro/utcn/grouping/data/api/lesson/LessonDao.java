package ro.utcn.grouping.data.api.lesson;

import java.util.List;

import ro.utcn.grouping.model.lesson.Lesson;

public interface LessonDao {

	int addLesson(Lesson add);

	List<Lesson> getLesson(LessonEntityGet get);
}
