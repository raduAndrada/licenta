package ro.utcn.grouping.data.api.lesson;

import org.immutables.value.Value;

import ro.utcn.grouping.model.lesson.LessonCriteria;

@Value.Immutable
public interface LessonEntityUpdate {

	String getTheme();

	LessonCriteria getCriteria();
}
