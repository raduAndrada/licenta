package ro.utcn.grouping.data.api.lesson;

import org.immutables.value.Value;

import ro.utcn.grouping.model.lesson.LessonGrouppCriteria;

@Value.Immutable
@Value.Modifiable
public interface LessonGrouppEntityGet {

	LessonGrouppCriteria getCriteria();
}
