package ro.utcn.grouping.data.api.lesson;

import org.immutables.value.Value;

import ro.utcn.grouping.model.lesson.LessonCriteria;

@Value.Immutable
@Value.Modifiable
public interface LessonEntityGet {

	LessonCriteria getCriteria();
}
