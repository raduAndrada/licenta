package ro.utcn.grouping.data.api.groupp;

import org.immutables.value.Value;

import ro.utcn.grouping.model.groupp.GrouppCriteria;

@Value.Immutable
@Value.Modifiable
public interface GrouppEntityGet {

	GrouppCriteria getCriteria();
}
