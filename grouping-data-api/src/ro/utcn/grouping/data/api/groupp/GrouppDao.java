package ro.utcn.grouping.data.api.groupp;

import java.util.List;

import ro.utcn.grouping.model.groupp.Groupp;

public interface GrouppDao {

	int addGroupp(Groupp group);

	List<Groupp> getGroupp(GrouppEntityGet GroupEntityGet);
}
