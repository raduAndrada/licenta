package ro.utcn.grouping.data.api.student;

import org.immutables.value.Value;

import ro.utcn.grouping.model.student.StudentCriteria;

@Value.Immutable
@Value.Modifiable
public interface StudentEntityGet {

	StudentCriteria getCriteria();
}
