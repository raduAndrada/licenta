package ro.utcn.grouping.data.api.student;

import java.util.List;

import ro.utcn.grouping.model.student.Student;

public interface StudentDao {

	int addStudent(Student student);

	List<Student> getStudent(StudentEntityGet studentEntityGet);

	int updateStudent(StudentEntityUpdate student);
}
