package ro.utcn.grouping.data.api.student;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import ro.utcn.grouping.model.student.StudentCriteria;

@Value.Immutable
public interface StudentEntityUpdate {

	@Nullable
	Long getGrouppId();

	@Nullable
	Long getRqGrouppId();

	@Nullable
	Boolean getCheckedIn();

	StudentCriteria getCriteria();
}
