package ro.utcn.grouping.data.api.rq;

import java.util.List;

import ro.utcn.grouping.model.rq.RqGroupp;

public interface RqGrouppDao {

	int addRqGroupp(RqGroupp rq);

	int updateRqGroupp(RqGrouppEntityUpdate rqGrouppEntityUpdate);

	int deleteRqGroupp(RqGrouppEntityDelete delete);

	List<RqGroupp> getRqGroupp(RqGrouppEntityGet rqGrouppEntityGet);
}
