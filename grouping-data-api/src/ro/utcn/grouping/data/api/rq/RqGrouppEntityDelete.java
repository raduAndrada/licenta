package ro.utcn.grouping.data.api.rq;

import org.immutables.value.Value;

import ro.utcn.grouping.model.rq.RqGrouppCriteria;

@Value.Immutable
@Value.Modifiable
public interface RqGrouppEntityDelete {

	RqGrouppCriteria getCriteria();
}
