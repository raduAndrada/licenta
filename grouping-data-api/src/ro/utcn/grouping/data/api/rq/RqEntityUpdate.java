package ro.utcn.grouping.data.api.rq;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import ro.utcn.grouping.model.rq.RqCriteria;
import ro.utcn.grouping.model.rq.RqGrouppTp;
import ro.utcn.grouping.model.rq.RqStatus;
import ro.utcn.grouping.model.rq.RqTp;
import ro.utcn.grouping.model.rq.TimeMeasurementTp;

@Value.Immutable
public interface RqEntityUpdate {

	// time until the request becomes invalid
	// it will be expressed in seconds
	@Nullable
	Integer getAllowedTm();

	// max number of allowed responses
	@Nullable
	Integer getMaxRsp();

	// type of the requestTime
	@Nullable
	RqTp getRqTp();

	// TIME MEASUREMENT
	@Nullable
	TimeMeasurementTp getTimeMeasurementTp();

	// when a rq is active it starts the grouping
	@Nullable
	RqStatus getRqStatus();

	// type of groups
	@Nullable
	RqGrouppTp getRqGrouppTp();

	@Nullable
	Integer getRqGrouppSz();

	RqCriteria getCriteria();
}
