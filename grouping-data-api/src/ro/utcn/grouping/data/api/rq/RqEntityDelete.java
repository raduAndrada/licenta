package ro.utcn.grouping.data.api.rq;

import org.immutables.value.Value;

import ro.utcn.grouping.model.rq.RqCriteria;

@Value.Immutable
@Value.Modifiable
public interface RqEntityDelete {

	RqCriteria getCriteria();
}
