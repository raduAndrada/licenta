package ro.utcn.grouping.data.api.rq;

import org.immutables.value.Value;

import ro.utcn.grouping.model.rq.RqGrouppCriteria;
import ro.utcn.grouping.model.rq.RqStatus;

@Value.Immutable
public interface RqGrouppEntityUpdate {

	RqStatus getRqStatus();

	RqGrouppCriteria getCriteria();
}
