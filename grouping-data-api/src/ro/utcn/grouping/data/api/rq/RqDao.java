package ro.utcn.grouping.data.api.rq;

import java.util.List;

import ro.utcn.grouping.model.rq.Rq;

public interface RqDao {

	int addRq(Rq rq);

	int updateRq(RqEntityUpdate rqEntityUpdate);

	int deleteRq(RqEntityDelete rqEntityDelete);

	List<Rq> getRq(RqEntityGet rqEntityGet);
}
