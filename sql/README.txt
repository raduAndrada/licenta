V1
pe modelul de baza de date: se adauga tabela COURSE cu cheia primara: COURSE_NM
facem legatura cu USR.COURSE_NM
putem pune coloana pentru id ????
modificat nume in tabela USR pentru LECTURE -> COURSE_NM 
modificat nume coloana in USR pentru TYPE -> USR_TP



V2 (25.03.2018)
adaugat coloana pentru username profesor in course, coloana ST_DT si END_DT si o coloana pentru enrollment key
schimbat tabela GROUP in GROUPP LEADER_PERIOD -> MANDATE, LEADER -> LEADER_URSNM si THEME-> PROJ_NM
modificat tabela RQ -> sters GROUP_ID, RQ_NM -> RQ_TP.


V3 (22.04.2018)
adaugat tabele: LESSON, RQ, RQ_GROUPP, RSP, QUESTION, QUESTION_ANSWER
adaugat coloana CHECKED_IN in tabela STUDENT (scopul este de a stii cand un student s-a logat de pe aplicatie). 


V4 (8.05.2018)
modificat tabela RQ_GROUPP, coloana active => RQ_STATUS, similar si pentru RQ
adaugat coloana VOTE_COUNT pentru tabela RSP

V5 (27.05.2018)
modificat tabela RQ_GROUPP adaugat coloana CRT_TM pentru a putea refolosi grupurile de la o anumita data si pentru
alte requesturi.