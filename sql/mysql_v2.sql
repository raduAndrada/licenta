-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema grouping
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema grouping
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `grouping` DEFAULT CHARACTER SET utf8 ;
USE `grouping` ;

-- -----------------------------------------------------
-- Table `grouping`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`course` (
  `nm` VARCHAR(100) NOT NULL,
  `crt_tm` DATETIME NOT NULL,
  `students_count` INT(11) NOT NULL,
  `proffessor_usrnm` VARCHAR(300) NOT NULL,
  `st_dt` DATETIME NOT NULL,
  `end_dt` DATETIME NOT NULL,
  `enrollment_key` VARCHAR(45) NULL DEFAULT NULL,
  `dsc` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`nm`),
  UNIQUE INDEX `nm_UNIQUE` (`nm` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`usr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`usr` (
  `usrnm` VARCHAR(150) NOT NULL,
  `psswd` VARCHAR(150) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `usr_tp` VARCHAR(100) NOT NULL,
  `f_nm` VARCHAR(100) NULL DEFAULT NULL,
  `l_nm` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`usrnm`, `email`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`course_usr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`course_usr` (
  `usr_usrnm` VARCHAR(100) NOT NULL,
  `course_nm` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`usr_usrnm`, `course_nm`),
  INDEX `cousr_usr_course_fk_idx` (`course_nm` ASC),
  CONSTRAINT `cousr_usr_course_fk`
    FOREIGN KEY (`course_nm`)
    REFERENCES `grouping`.`course` (`nm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `usr_usr_course_fk`
    FOREIGN KEY (`usr_usrnm`)
    REFERENCES `grouping`.`usr` (`usrnm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`groupp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`groupp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `size` INT(11) NOT NULL DEFAULT '1',
  `leader_usrnm` VARCHAR(300) NULL DEFAULT NULL,
  `mandate` INT(11) NULL DEFAULT NULL,
  `proj_nm` VARCHAR(150) NULL DEFAULT NULL,
  `course_nm` VARCHAR(150) NOT NULL,
  `groupp_tp` VARCHAR(150) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `groupp_usr_fk_idx` (`leader_usrnm` ASC),
  INDEX `groupp_course_fk_idx` (`course_nm` ASC),
  CONSTRAINT `groupp_course_fk`
    FOREIGN KEY (`course_nm`)
    REFERENCES `grouping`.`course` (`nm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `groupp_usr_fk`
    FOREIGN KEY (`leader_usrnm`)
    REFERENCES `grouping`.`usr` (`usrnm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`proffessor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`proffessor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usr_usrnm` VARCHAR(150) NOT NULL,
  `usr_email` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `professor_usr_fk` (`usr_usrnm` ASC, `usr_email` ASC),
  CONSTRAINT `professor_usr_fk`
    FOREIGN KEY (`usr_usrnm` , `usr_email`)
    REFERENCES `grouping`.`usr` (`usrnm` , `email`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`student` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usr_usrnm` VARCHAR(150) NOT NULL,
  `usr_email` VARCHAR(150) NOT NULL,
  `groupp_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `student_usr_fk` (`usr_usrnm` ASC, `usr_email` ASC),
  CONSTRAINT `student_usr_fk`
    FOREIGN KEY (`usr_usrnm` , `usr_email`)
    REFERENCES `grouping`.`usr` (`usrnm` , `email`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
