-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema grouping
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema grouping
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `grouping` DEFAULT CHARACTER SET utf8 ;
USE `grouping` ;

-- -----------------------------------------------------
-- Table `grouping`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`course` (
  `nm` VARCHAR(100) NOT NULL,
  `crt_tm` DATETIME NOT NULL,
  `students_count` INT(11) NOT NULL,
  `proffessor_usrnm` VARCHAR(300) NOT NULL,
  `st_dt` DATETIME NULL DEFAULT NULL,
  `end_dt` DATETIME NULL DEFAULT NULL,
  `enrollment_key` VARCHAR(45) NULL DEFAULT NULL,
  `dsc` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`nm`),
  UNIQUE INDEX `nm_UNIQUE` (`nm` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`usr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`usr` (
  `usrnm` VARCHAR(150) NOT NULL,
  `psswd` VARCHAR(150) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `usr_tp` VARCHAR(100) NOT NULL,
  `f_nm` VARCHAR(100) NULL DEFAULT NULL,
  `l_nm` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`usrnm`, `email`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`course_usr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`course_usr` (
  `usr_usrnm` VARCHAR(100) NOT NULL,
  `course_nm` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`usr_usrnm`, `course_nm`),
  INDEX `cousr_usr_course_fk_idx` (`course_nm` ASC),
  CONSTRAINT `cousr_usr_course_fk`
    FOREIGN KEY (`course_nm`)
    REFERENCES `grouping`.`course` (`nm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `usr_usr_course_fk`
    FOREIGN KEY (`usr_usrnm`)
    REFERENCES `grouping`.`usr` (`usrnm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`groupp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`groupp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `size` INT(11) NOT NULL DEFAULT '1',
  `leader_usrnm` VARCHAR(300) NULL DEFAULT NULL,
  `mandate` INT(11) NULL DEFAULT NULL,
  `proj_nm` VARCHAR(150) NULL DEFAULT NULL,
  `course_nm` VARCHAR(150) NOT NULL,
  `groupp_tp` VARCHAR(150) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `groupp_usr_fk_idx` (`leader_usrnm` ASC),
  INDEX `groupp_course_fk_idx` (`course_nm` ASC),
  CONSTRAINT `groupp_course_fk`
    FOREIGN KEY (`course_nm`)
    REFERENCES `grouping`.`course` (`nm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `groupp_usr_fk`
    FOREIGN KEY (`leader_usrnm`)
    REFERENCES `grouping`.`usr` (`usrnm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`lesson`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`lesson` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `week_nb` INT(11) NOT NULL,
  `theme` VARCHAR(150) NULL DEFAULT NULL,
  `course_nm` VARCHAR(300) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `lesson_course_fk_idx` (`course_nm` ASC),
  CONSTRAINT `lesson_course_fk`
    FOREIGN KEY (`course_nm`)
    REFERENCES `grouping`.`course` (`nm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 99
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`proffessor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`proffessor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usr_usrnm` VARCHAR(150) NOT NULL,
  `usr_email` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `professor_usr_fk` (`usr_usrnm` ASC, `usr_email` ASC),
  CONSTRAINT `professor_usr_fk`
    FOREIGN KEY (`usr_usrnm` , `usr_email`)
    REFERENCES `grouping`.`usr` (`usrnm` , `email`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`rq`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`rq` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lesson_id` INT(11) NOT NULL,
  `rq_tp` VARCHAR(150) NOT NULL,
  `allowed_tm` INT(11) NULL DEFAULT NULL,
  `max_rsp` INT(11) NULL DEFAULT NULL,
  `time_measurement_tp` VARCHAR(150) NULL DEFAULT NULL,
  `rq_status` VARCHAR(150) NULL DEFAULT 'PENDING',
  `rq_groupp_tp` VARCHAR(150) NULL DEFAULT NULL,
  `rq_groupp_sz` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `rq_lesson_fk_idx` (`lesson_id` ASC),
  CONSTRAINT `rq_lesson_fk`
    FOREIGN KEY (`lesson_id`)
    REFERENCES `grouping`.`lesson` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`question` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `rq_id` INT(11) NOT NULL,
  `question_text` VARCHAR(300) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `quesion_rq_fk_idx` (`rq_id` ASC),
  CONSTRAINT `quesion_rq_fk`
    FOREIGN KEY (`rq_id`)
    REFERENCES `grouping`.`rq` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 76
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`question_answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`question_answer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_id` INT(11) NOT NULL,
  `answer_text` VARCHAR(300) NOT NULL,
  `correct` INT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `answer_question_fk_idx` (`question_id` ASC),
  CONSTRAINT `answer_question_fk`
    FOREIGN KEY (`question_id`)
    REFERENCES `grouping`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 61
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`rq_groupp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`rq_groupp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `rq_id` INT(11) NULL DEFAULT NULL,
  `rq_status` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `lesson_groupp_rq_fk_idx` (`rq_id` ASC),
  CONSTRAINT `rq_groupp_rq_fk`
    FOREIGN KEY (`rq_id`)
    REFERENCES `grouping`.`rq` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 29025009
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`student` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usr_usrnm` VARCHAR(150) NOT NULL,
  `usr_email` VARCHAR(150) NOT NULL,
  `groupp_id` INT(11) NULL DEFAULT NULL,
  `rq_groupp_id` INT(11) NULL DEFAULT NULL,
  `checked_in` INT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `student_usr_fk` (`usr_usrnm` ASC, `usr_email` ASC),
  INDEX `student_lesson_groupp_fk_idx` (`rq_groupp_id` ASC),
  CONSTRAINT `student_lesson_groupp_fk`
    FOREIGN KEY (`rq_groupp_id`)
    REFERENCES `grouping`.`rq_groupp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `student_usr_fk`
    FOREIGN KEY (`usr_usrnm` , `usr_email`)
    REFERENCES `grouping`.`usr` (`usrnm` , `email`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `grouping`.`rsp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grouping`.`rsp` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `rq_groupp_id` INT(11) NOT NULL,
  `rsp_text` VARCHAR(300) NULL DEFAULT NULL,
  `question_id` INT(11) NOT NULL,
  `student_usrnm` VARCHAR(150) NULL DEFAULT NULL,
  `vote_count` INT(3) NULL DEFAULT '0',
  PRIMARY KEY (`id`, `rq_groupp_id`),
  INDEX `rsp_lesson_groupp_fk_idx` (`rq_groupp_id` ASC),
  INDEX `rsp_question_fk_idx` (`question_id` ASC),
  INDEX `rsp_student_fk_idx` (`student_usrnm` ASC),
  CONSTRAINT `rsp_lesson_groupp_fk`
    FOREIGN KEY (`rq_groupp_id`)
    REFERENCES `grouping`.`rq_groupp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rsp_question_fk`
    FOREIGN KEY (`question_id`)
    REFERENCES `grouping`.`question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rsp_student_fk`
    FOREIGN KEY (`student_usrnm`)
    REFERENCES `grouping`.`student` (`usr_usrnm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 205
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
