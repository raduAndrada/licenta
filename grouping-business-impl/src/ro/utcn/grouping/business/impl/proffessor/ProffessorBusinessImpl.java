package ro.utcn.grouping.business.impl.proffessor;

import java.time.Instant;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import ro.utcn.grouping.business.api.proffessor.ProffessorBusiness;
import ro.utcn.grouping.business.base.CurrentUsrContext;
import ro.utcn.grouping.data.api.course.CourseDao;
import ro.utcn.grouping.data.api.course.CourseEntityDelete;
import ro.utcn.grouping.data.api.course.CourseEntityUpdate;
import ro.utcn.grouping.data.api.course.CourseUsrDao;
import ro.utcn.grouping.data.api.course.ImtCourseEntityDelete;
import ro.utcn.grouping.data.api.course.ImtCourseEntityGet;
import ro.utcn.grouping.data.api.course.ImtCourseEntityUpdate;
import ro.utcn.grouping.data.api.course.ImtCourseUsrEntityGet;
import ro.utcn.grouping.data.api.groupp.GrouppDao;
import ro.utcn.grouping.data.api.groupp.ImtGrouppEntityGet;
import ro.utcn.grouping.data.api.lesson.ImtLessonEntityGet;
import ro.utcn.grouping.data.api.lesson.LessonDao;
import ro.utcn.grouping.data.api.proffessor.ImtProffessorEntityGet;
import ro.utcn.grouping.data.api.proffessor.ProffessorDao;
import ro.utcn.grouping.data.api.question.ImtQuestionAnswerEntityDelete;
import ro.utcn.grouping.data.api.question.ImtQuestionAnswerEntityGet;
import ro.utcn.grouping.data.api.question.ImtQuestionAnswerEntityUpdate;
import ro.utcn.grouping.data.api.question.ImtQuestionEntityDelete;
import ro.utcn.grouping.data.api.question.ImtQuestionEntityGet;
import ro.utcn.grouping.data.api.question.ImtQuestionEntityUpdate;
import ro.utcn.grouping.data.api.question.QuestionAnswerDao;
import ro.utcn.grouping.data.api.question.QuestionDao;
import ro.utcn.grouping.data.api.rq.ImtRqEntityDelete;
import ro.utcn.grouping.data.api.rq.ImtRqEntityGet;
import ro.utcn.grouping.data.api.rq.ImtRqEntityUpdate;
import ro.utcn.grouping.data.api.rq.ImtRqGrouppEntityDelete;
import ro.utcn.grouping.data.api.rq.ImtRqGrouppEntityGet;
import ro.utcn.grouping.data.api.rq.ImtRqGrouppEntityUpdate;
import ro.utcn.grouping.data.api.rq.RqDao;
import ro.utcn.grouping.data.api.rq.RqGrouppDao;
import ro.utcn.grouping.data.api.rsp.ImtRspEntityGet;
import ro.utcn.grouping.data.api.rsp.RspDao;
import ro.utcn.grouping.data.api.student.ImtStudentEntityGet;
import ro.utcn.grouping.data.api.student.ImtStudentEntityUpdate;
import ro.utcn.grouping.data.api.student.StudentDao;
import ro.utcn.grouping.model.base.BusinessException;
import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseCreateResult;
import ro.utcn.grouping.model.course.CourseRecord;
import ro.utcn.grouping.model.course.CourseUsr;
import ro.utcn.grouping.model.course.ImtCourse;
import ro.utcn.grouping.model.course.ImtCourseCreateResult;
import ro.utcn.grouping.model.course.ImtCourseCriteria;
import ro.utcn.grouping.model.course.ImtCourseRecord;
import ro.utcn.grouping.model.course.ImtCourseUsrCriteria;
import ro.utcn.grouping.model.course.MdfCourseUsr;
import ro.utcn.grouping.model.groupp.Groupp;
import ro.utcn.grouping.model.groupp.GrouppConstraints;
import ro.utcn.grouping.model.groupp.GrouppGetResult;
import ro.utcn.grouping.model.groupp.ImtGroupp;
import ro.utcn.grouping.model.groupp.ImtGrouppCriteria;
import ro.utcn.grouping.model.groupp.ImtGrouppGetResult;
import ro.utcn.grouping.model.lesson.ImtLesson;
import ro.utcn.grouping.model.lesson.ImtLessonCriteria;
import ro.utcn.grouping.model.lesson.Lesson;
import ro.utcn.grouping.model.proffesor.ImtProffessorCriteria;
import ro.utcn.grouping.model.question.ImtQuestionAnswer;
import ro.utcn.grouping.model.question.ImtQuestionAnswerCriteria;
import ro.utcn.grouping.model.question.ImtQuestionCriteria;
import ro.utcn.grouping.model.question.ImtQuestionRecord;
import ro.utcn.grouping.model.question.Question;
import ro.utcn.grouping.model.question.QuestionAnswer;
import ro.utcn.grouping.model.question.QuestionAnswerCriteria;
import ro.utcn.grouping.model.question.QuestionCriteria;
import ro.utcn.grouping.model.question.QuestionRecord;
import ro.utcn.grouping.model.rq.ImtRq;
import ro.utcn.grouping.model.rq.ImtRqCreateResult;
import ro.utcn.grouping.model.rq.ImtRqCriteria;
import ro.utcn.grouping.model.rq.ImtRqGetResult;
import ro.utcn.grouping.model.rq.ImtRqGroupp;
import ro.utcn.grouping.model.rq.ImtRqGrouppCriteria;
import ro.utcn.grouping.model.rq.ImtRqGrouppGetResult;
import ro.utcn.grouping.model.rq.ImtRqRecord;
import ro.utcn.grouping.model.rq.MdfRq;
import ro.utcn.grouping.model.rq.MdfRqGroupp;
import ro.utcn.grouping.model.rq.Rq;
import ro.utcn.grouping.model.rq.RqCreateResult;
import ro.utcn.grouping.model.rq.RqCriteria;
import ro.utcn.grouping.model.rq.RqDetail;
import ro.utcn.grouping.model.rq.RqGet;
import ro.utcn.grouping.model.rq.RqGetResult;
import ro.utcn.grouping.model.rq.RqGroupp;
import ro.utcn.grouping.model.rq.RqGrouppGetResult;
import ro.utcn.grouping.model.rq.RqRecord;
import ro.utcn.grouping.model.rq.RqStatus;
import ro.utcn.grouping.model.rq.RqTp;
import ro.utcn.grouping.model.rq.TimeMeasurementTp;
import ro.utcn.grouping.model.rsp.ImtRspCriteria;
import ro.utcn.grouping.model.rsp.ImtRspDetail;
import ro.utcn.grouping.model.rsp.ImtRspDetailGetResult;
import ro.utcn.grouping.model.rsp.ImtRspRecord;
import ro.utcn.grouping.model.rsp.Rsp;
import ro.utcn.grouping.model.rsp.RspDetail;
import ro.utcn.grouping.model.rsp.RspDetailGetResult;
import ro.utcn.grouping.model.student.ImtStudentCriteria;
import ro.utcn.grouping.model.student.Student;

public class ProffessorBusinessImpl implements ProffessorBusiness {

	private CurrentUsrContext currentUsrContext;
	private CourseDao courseDao;
	private GrouppDao grouppDao;
	private StudentDao studentDao;
	private CourseUsrDao courseUsrDao;
	private LessonDao lessonDao;
	private ProffessorDao proffessorDao;
	private RqDao rqDao;
	private QuestionDao questionDao;
	private QuestionAnswerDao questionAnswerDao;
	private RqGrouppDao rqGrouppDao;
	private RspDao rspDao;

	@Inject
	public void setCourseDao(CourseDao courseDao) {
		this.courseDao = courseDao;
	}

	@Inject
	public void setGrouppDao(GrouppDao grouppDao) {
		this.grouppDao = grouppDao;
	}

	@Inject
	public void setStudentDao(StudentDao studentDao) {
		this.studentDao = studentDao;
	}

	@Inject
	public void setCourseUsrDao(CourseUsrDao courseUsrDao) {
		this.courseUsrDao = courseUsrDao;
	}

	@Inject
	public void setLessonDao(LessonDao lessonDao) {
		this.lessonDao = lessonDao;
	}

	@Inject
	public void setCurrentUsrContext(CurrentUsrContext currentUsrContext) {
		this.currentUsrContext = currentUsrContext;
	}

	@Inject
	public void setProffessorDao(ProffessorDao proffessorDao) {
		this.proffessorDao = proffessorDao;
	}

	@Inject
	public void setRqDao(RqDao rqDao) {
		this.rqDao = rqDao;
	}

	@Inject
	public void setQuestionDao(QuestionDao questionDao) {
		this.questionDao = questionDao;
	}

	@Inject
	public void setQuestionAnswerDao(QuestionAnswerDao questionAnswerDao) {
		this.questionAnswerDao = questionAnswerDao;
	}

	@Inject
	public void setRqGrouppDao(RqGrouppDao rqGrouppDao) {
		this.rqGrouppDao = rqGrouppDao;
	}

	@Inject
	public void setRspDao(RspDao rspDao) {
		this.rspDao = rspDao;
	}

	@Override
	public RqCreateResult addRq(RqDetail rqDetail) {
		rqDao.addRq(MdfRq.create().from(rqDetail.getRq())
				.setRqStatus(rqDetail.getRqGroupp() != null ? RqStatus.OPENED : RqStatus.PENDING));
		if (rqDetail.getRqGroupp() != null) {
			rqGrouppDao.addRqGroupp(MdfRqGroupp.create().from(rqDetail.getRqGroupp()).setCrtTm(Instant.now()));
		}
		return ImtRqCreateResult.builder().allowedTm(rqDetail.getRq().getAllowedTm())
				.timeMeasurementTp(rqDetail.getRq().getTimeMeasurementTp()).rqTp(rqDetail.getRq().getRqTp()).build();
	}

	@Override
	public RqGetResult getRqForLesson(RqGet rqGet) {

		final List<Rq> rqList = rqDao.getRq(ImtRqEntityGet.builder().criteria(ImtRqCriteria.builder()
				.lessonIdIncl(rqGet.getLessonId() != null ? ImmutableList.of(rqGet.getLessonId()) : ImmutableList.of())
				.idIncl(rqGet.getId() != null ? ImmutableList.of(rqGet.getId()) : ImmutableList.of()).build()).build());
		final Integer weekNb = lessonDao
				.getLesson(ImtLessonEntityGet.builder()
						.criteria(ImtLessonCriteria.builder().addIdIncl(rqGet.getLessonId()).build()).build())
				.stream().findFirst().get().getWeekNb();

		final List<RqRecord> rqRecords = Lists.newArrayList();
		rqList.forEach(e -> {
			rqRecords.add(ImtRqRecord.builder().questions(getQuestionForRq(e.getId())).rq(e).build());
		});
		return ImtRqGetResult.builder().list(rqRecords).weekNb(weekNb).build();
	}

	@Override
	public RqRecord addQuestionToRq(QuestionRecord questionRecord) {
		this.questionDao.addQuestion(questionRecord.getQuestion());
		if (questionRecord.getQuestionAnswerList() != null && !questionRecord.getQuestionAnswerList().isEmpty()) {
			questionRecord.getQuestionAnswerList().forEach(e -> {
				this.questionAnswerDao.addQuestionAnswer(ImtQuestionAnswer.builder().answerText(e.getAnswerText())
						.correct(e.getCorrect()).questionId(questionRecord.getQuestion().getId()).build());
			});
		}
		final Rq rq = this.rqDao.getRq(ImtRqEntityGet.builder()
				.criteria(ImtRqCriteria.builder().addIdIncl(questionRecord.getQuestion().getRqId()).build()).build())
				.stream().findFirst().orElse(null);

		return ImtRqRecord.builder().questions(getQuestionForRq(rq.getId())).rq(rq).build();
	}

	@Override
	public RqRecord updateQuestionListForRq(RqRecord rqRecord) {
		updateQuestionsForRq(
				rqRecord.getQuestions().stream().map(QuestionRecord::getQuestion).collect(Collectors.toList()),
				rqRecord.getRq().getId());
		rqRecord.getQuestions().stream()
				.forEach(e -> updateQuestionAnswer(e.getQuestionAnswerList(), e.getQuestion().getId()));

		final Rq rq = this.rqDao
				.getRq(ImtRqEntityGet.builder()
						.criteria(ImtRqCriteria.builder().addIdIncl(rqRecord.getRq().getId()).build()).build())
				.stream().findFirst().orElse(null);

		return ImtRqRecord.builder().questions(getQuestionForRq(rq.getId())).rq(rq).build();
	}

	@Override
	public Rq deleteRq(Long rqId) {
		final RqCriteria criteria = ImtRqCriteria.builder().addIdIncl(rqId).build();
		final Rq rqToDelete = rqDao.getRq(ImtRqEntityGet.builder().criteria(criteria).build()).stream().findFirst()
				.orElse(null);
		if (rqToDelete == null) {
			throw new BusinessException("The request to delete could not be found!");
		}
		// delete all the questions and answers from this request first

		// get the questions
		final QuestionCriteria qCriteria = ImtQuestionCriteria.builder().addRqIdIncl(rqId).build();
		final List<Long> qIdList = questionDao.getQuestion(ImtQuestionEntityGet.builder().criteria(qCriteria).build())
				.stream().map(Question::getId).collect(Collectors.toList());

		// build the criteria
		final QuestionAnswerCriteria qaCriteria = ImtQuestionAnswerCriteria.builder().questionIdIncl(qIdList).build();

		// delete their answers
		this.questionAnswerDao
				.deleteQuestionAnswer(ImtQuestionAnswerEntityDelete.builder().criteria(qaCriteria).build());

		// delete the questions
		this.questionDao.deleteQuestion(ImtQuestionEntityDelete.builder().criteria(qCriteria).build());

		// delete all the rq groups
		this.rqGrouppDao.deleteRqGroupp(ImtRqGrouppEntityDelete.builder()
				.criteria(ImtRqGrouppCriteria.builder().addRqIdIncl(rqId).build()).build());

		// TODO delete all the rsp when that table will exist !!!
		rqDao.deleteRq(ImtRqEntityDelete.builder().criteria(criteria).build());
		return ImtRq.builder().from(rqToDelete).build();
	}

	@Override
	public RqGrouppGetResult getRqGrouppForCourse(String courseNm) {
		final List<Long> lessonsIdList = lessonDao
				.getLesson(ImtLessonEntityGet.builder()
						.criteria(ImtLessonCriteria.builder().addCourseNmIncl(courseNm).build()).build())
				.stream().map(Lesson::getId).collect(Collectors.toList());
		final List<Long> rqIdList = rqDao.getRq(
				ImtRqEntityGet.builder().criteria(ImtRqCriteria.builder().lessonIdIncl(lessonsIdList).build()).build())
				.stream().map(Rq::getId).collect(Collectors.toList());
		return ImtRqGrouppGetResult.builder().list(rqGrouppDao.getRqGroupp(ImtRqGrouppEntityGet.builder()
				.criteria(ImtRqGrouppCriteria.builder().rqIdIncl(rqIdList).build()).build())).build();
	}

	@Override
	public RqRecord updateRq(Rq rq) {
		final RqCriteria criteria = ImtRqCriteria.builder().addIdIncl(rq.getId()).build();
		final Rq rqToActivate = rqDao.getRq(ImtRqEntityGet.builder().criteria(criteria).build()).stream().findFirst()
				.orElse(null);
		if (rqToActivate == null) {
			throw new BusinessException("The request to delete could not be found!");
		}
		rqDao.updateRq(ImtRqEntityUpdate.builder().rqStatus(rq.getRqStatus()).allowedTm(rq.getAllowedTm())
				.maxRsp(rq.getMaxRsp()).timeMeasurementTp(rq.getTimeMeasurementTp()).criteria(criteria).build());

		final List<QuestionRecord> questions = getQuestionForRq(rq.getId());
		if (rq.getRqStatus().equals(RqStatus.OPENED)) {
			formGroupForRq(rqToActivate);
		}
		if (rq.getRqStatus().equals(RqStatus.ONGOING)) {
			lauchRqGroupp(rqToActivate);
		}

		return ImtRqRecord.builder().questions(questions).rq(rq).build();
	}

	@Override
	public RspDetailGetResult getRspsForRq(Long rqId) {
		final Rq rq = rqDao
				.getRq(ImtRqEntityGet.builder().criteria(ImtRqCriteria.builder().addIdIncl(rqId).build()).build())
				.stream().findFirst().orElse(null);
		if (rq == null) {
			throw new BusinessException("rq not found");
		}
		final List<Question> questionList = questionDao.getQuestion(ImtQuestionEntityGet.builder()
				.criteria(ImtQuestionCriteria.builder().addRqIdIncl(rqId).build()).build());
		final List<RqGroupp> rqGrouppList = rqGrouppDao.getRqGroupp(ImtRqGrouppEntityGet.builder()
				.criteria(ImtRqGrouppCriteria.builder().addRqIdIncl(rqId).build()).build());

		final List<RspDetail> rspDetailList = Lists.newArrayList();
		rqGrouppList.forEach(e -> {
			for (int i = 0; i < questionList.size(); i++) {
				List<Rsp> rspList = rspDao.getRsp(ImtRspEntityGet.builder().criteria(ImtRspCriteria.builder()
						.addQuestionIdIncl(questionList.get(i).getId()).addRqGrouppIdIncl(e.getId()).build()).build());
				final Set<String> studentUsrnmsList = rspList.stream().map(Rsp::getStudentUsrnm)
						.collect(Collectors.toSet());
				if (!rspList.isEmpty()) {
					Collections.sort(rspList, new Comparator<Rsp>() {
						@Override
						public int compare(Rsp o1, Rsp o2) {
							return o2.getVoteCount().compareTo(o1.getVoteCount());
						}
					});
					if (rspList.size() != rq.getMaxRsp()) {
						rspList = rspList.subList(0, rq.getMaxRsp() < rspList.size() ? rq.getMaxRsp() : rspList.size());
					}

					final RspDetail rspDetail = ImtRspDetail.builder()
							.rspRecord(ImtRspRecord.builder().addAllList(rspList)
									.questionText(questionList.get(i).getQuestionText()).build())
							.addAllStudentUsrnmsList(studentUsrnmsList).build();
					rspDetailList.add(rspDetail);
				}
			}

		});
		return ImtRspDetailGetResult.builder().list(rspDetailList).build();
	}

	@Override
	public CourseCreateResult createCourse(Course course) {
		if (courseNmExists(course)) {
			throw new BusinessException("Course name is already taken");
		}
		if (!isCurrentUsrProffessor()) {
			throw new BusinessException("User doesn't own the rights of creating a course");
		}

		courseDao.addCourse(buildCourseRecordForAdd(course));

		courseUsrDao.addCourseUsr(
				MdfCourseUsr.create().setCourseNm(course.getNm()).setUsrUsrnm(currentUsrContext.getUsername()));

		// we add a lesson list for the created course
		final List<Lesson> lessonsForCourse = buildLessonsForCreatedCourse(course.getNm());
		lessonsForCourse.forEach(e -> lessonDao.addLesson(e));
		return ImtCourseCreateResult.builder().nm(course.getNm()).build();
	}

	@Override
	public Course updateCourse(Course course) {
		if (!courseNmExists(course)) {
			throw new BusinessException("Course name does not exist");
		}
		courseDao.updateCourse(buildCourseEntityForUpdate(course));

		// we add a lesson list for the created course
		final List<Lesson> lessonsForCourse = buildLessonsForCreatedCourse(course.getNm());
		lessonsForCourse.forEach(e -> lessonDao.addLesson(e));
		return ImtCourse.builder().dsc(course.getDsc()).nm(course.getNm()).studentsCount(course.getStudentsCount())
				.build();
	}

	@Override
	public Course deleteCourse(String courseNm) {

		final Optional<Course> courseToDelete = getCourseByNm(courseNm);
		if (!courseToDelete.isPresent()) {
			throw new BusinessException("Course name does not exist");
		}
		courseDao.deleteCourse(buildCourseEntityForDelete(courseToDelete.get()));
		return courseToDelete.get();
	}

	@Override
	public GrouppGetResult groupForSemProjs(GrouppConstraints constraints) {

		final List<String> studentsUsrnms = courseUsrDao.getCourseUsr(ImtCourseUsrEntityGet.builder()
				.criteria(ImtCourseUsrCriteria.builder().addCourseNmIncl(constraints.getCourseNm()).build()).build())
				.stream().map(CourseUsr::getUsrUsrnm).collect(Collectors.toList());
		final List<Student> students = studentDao.getStudent(ImtStudentEntityGet.builder()
				.criteria(ImtStudentCriteria.builder().usrUsrnmIncl(studentsUsrnms).build()).build());

		switch (constraints.getGrouppTp()) {
		case SEM_PROJ: {
			if (constraints.getDefaultt()) {
				groupStudentsForSemProjWithDefault(students, constraints);
			}
			break;
		}
		case SEM_PROJ_CHOOSE: {
			groupStudentsForSemProjChoose(constraints);
			break;
		}
		default: {
			break;
		}
		}

		return ImtGrouppGetResult.builder()
				.list(grouppDao.getGroupp(ImtGrouppEntityGet.builder()
						.criteria(ImtGrouppCriteria.builder().addCourseNmIncl(constraints.getCourseNm()).build())
						.build()))
				.build();
	}

	@Override
	public GrouppGetResult getGroupsForCourse(String courseNm) {
		if (!getCourseByNm(courseNm).isPresent()) {
			throw new BusinessException("Course name does not exist");
		}
		return ImtGrouppGetResult.builder().list(grouppDao.getGroupp(ImtGrouppEntityGet.builder()
				.criteria(ImtGrouppCriteria.builder().addCourseNmIncl(courseNm).build()).build())).build();
	}

	private boolean courseNmExists(Course course) {
		return courseDao
				.getCourse(ImtCourseEntityGet.builder()
						.criteria(ImtCourseCriteria.builder().addNmIncl(course.getNm()).build()).build())
				.stream().findAny().isPresent();

	}

	private boolean isCurrentUsrProffessor() {
		return this.proffessorDao
				.getProffessor(
						ImtProffessorEntityGet.builder()
								.criteria(ImtProffessorCriteria.builder()
										.addUsrUsrnmIncl(currentUsrContext.getUsername()).build())
								.build())
				.stream().findAny().isPresent();
	}

	private Optional<Course> getCourseByNm(String nm) {
		return courseDao.getCourse(
				ImtCourseEntityGet.builder().criteria(ImtCourseCriteria.builder().addNmIncl(nm).build()).build())
				.stream().findFirst();
	}

	private CourseRecord buildCourseRecordForAdd(Course courseToAdd) {
		// TODO add timeManager so that this can be tested
		return ImtCourseRecord.builder()
				.course(ImtCourse.copyOf(courseToAdd).withProffessorUsrnm(currentUsrContext.getUsername()))
				.crtTm(Instant.now()).build();
	}

	private List<Lesson> buildLessonsForCreatedCourse(String courseNm) {
		int i = 1;
		final List<Lesson> lessonList = Lists.newArrayList();
		for (; i <= 14; i++) {
			lessonList.add(ImtLesson.builder().courseNm(courseNm).theme("Week " + i).weekNb(i).build());
		}
		return lessonList;

	}

	private CourseEntityUpdate buildCourseEntityForUpdate(Course course) {
		final Course actualCourse = getCourseByNm(course.getNm()).get();
		return ImtCourseEntityUpdate.builder().criteria(ImtCourseCriteria.builder().addNmIncl(course.getNm()).build())
				.dsc(course.getDsc() != null ? course.getDsc() : actualCourse.getDsc())
				.studentsCount(
						course.getStudentsCount() != null ? course.getStudentsCount() : actualCourse.getStudentsCount())
				.build();
	}

	private CourseEntityDelete buildCourseEntityForDelete(Course course) {
		return ImtCourseEntityDelete.builder().criteria(ImtCourseCriteria.builder().addNmIncl(course.getNm()).build())
				.build();
	}

	private void groupStudentsForSemProjWithDefault(List<Student> students, GrouppConstraints contraints) {
		// rearrange list randomly
		final long seed = System.nanoTime();
		Collections.shuffle(students, new Random(seed));

		// iterate through list
		for (int i = 0; i < students.size(); i += contraints.getGrouppSize()) {

			try {
				// sublist to make a group
				final List<Student> group = students.subList(i, i + contraints.getGrouppSize());
				final Random randomizer = new Random();

				// choose leader randomly
				final Student leader = group.get(randomizer.nextInt(group.size()));
				final Groupp grouppToAdd = ImtGroupp.builder().projNm(contraints.getProjNm())
						.courseNm(contraints.getCourseNm()).leaderUsrnm(leader.getUsrUsrnm()).size(group.size())
						.mandate(contraints.getMandate()).grouppTp(contraints.getGrouppTp()).build();
				grouppDao.addGroupp(grouppToAdd);

				// add group id for each student
				group.forEach(e -> studentDao.updateStudent(ImtStudentEntityUpdate.builder()
						.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(e.getUsrUsrnm()).build())
						.grouppId(grouppToAdd.getId()).build()));
			} catch (final Exception e) {
				throw new BusinessException("Group could not be formed. Contraints are inconsistent with the database");
			}

		}

	}

	private void groupStudentsForSemProjChoose(GrouppConstraints constraints) {
		grouppDao.addGroupp(ImtGroupp.builder().projNm(constraints.getProjNm()).courseNm(constraints.getCourseNm())
				.size(constraints.getGrouppSize()).mandate(constraints.getMandate()).grouppTp(constraints.getGrouppTp())
				.build());

	}

	private List<QuestionRecord> getQuestionForRq(Long rqId) {
		final Rq selectedRq = rqDao
				.getRq(ImtRqEntityGet.builder().criteria(ImtRqCriteria.builder().addIdIncl(rqId).build()).build())
				.stream().findFirst().orElse(null);
		if (selectedRq == null) {
			throw new BusinessException("The request cannot be found");
		} else {
			final List<Question> questionsForRq = this.questionDao.getQuestion(ImtQuestionEntityGet.builder()
					.criteria(ImtQuestionCriteria.builder().rqIdIncl(ImmutableList.of(rqId)).build()).build());
			final List<QuestionRecord> questionRecordList = Lists.newArrayList();
			if (selectedRq.getRqTp() == RqTp.TEXT_FILLING) {
				questionsForRq.forEach(e -> questionRecordList.add(ImtQuestionRecord.builder().question(e).build()));
			} else {
				questionsForRq.forEach(e -> questionRecordList.add(ImtQuestionRecord.builder().question(e)
						.questionAnswerList(questionAnswerDao.getQuestionAnswer(ImtQuestionAnswerEntityGet.builder()
								.criteria(ImtQuestionAnswerCriteria.builder().addQuestionIdIncl(e.getId()).build())
								.build()))
						.build()));
			}

			return questionRecordList;
		}
	}

	private void updateQuestionsForRq(List<Question> qList, Long rqId) {
		final Set<Long> allRqQuestionIds = questionDao
				.getQuestion(ImtQuestionEntityGet.builder()
						.criteria(ImtQuestionCriteria.builder().addRqIdIncl(rqId).build()).build())
				.stream().map(Question::getId).collect(Collectors.toSet());
		final Set<Long> updatedQuestionIds = qList.stream().map(Question::getId).collect(Collectors.toSet());
		final Set<Long> rqQuestionsForDeleteIds = Sets.difference(allRqQuestionIds, updatedQuestionIds);
		qList.forEach(q -> {

			final QuestionCriteria questionCriteria = ImtQuestionCriteria.builder().addIdIncl(q.getId())
					.addRqIdIncl(rqId).build();
			final Question dbQuestion = questionDao
					.getQuestion(ImtQuestionEntityGet.builder().criteria(questionCriteria).build()).stream().findFirst()
					.orElse(null);

			questionDao.updateQuestion(ImtQuestionEntityUpdate.builder().criteria(questionCriteria)
					.questionText(q.getQuestionText() != null ? q.getQuestionText() : dbQuestion.getQuestionText())
					.build());

		});
		rqQuestionsForDeleteIds.forEach(e -> {
			questionAnswerDao.deleteQuestionAnswer(ImtQuestionAnswerEntityDelete.builder()
					.criteria(ImtQuestionAnswerCriteria.builder().addQuestionIdIncl(e).build()).build());

			questionDao.deleteQuestion(ImtQuestionEntityDelete.builder()
					.criteria(ImtQuestionCriteria.builder().addIdIncl(e).build()).build());
		});

	}

	private void updateQuestionAnswer(List<QuestionAnswer> qaList, Long qId) {

		final Set<Long> allQuestionsAnswerIdList = this.questionAnswerDao
				.getQuestionAnswer(ImtQuestionAnswerEntityGet.builder()
						.criteria(ImtQuestionAnswerCriteria.builder().addQuestionIdIncl(qId).build()).build())
				.stream().map(QuestionAnswer::getId).collect(Collectors.toSet());
		if (qaList != null && !qaList.isEmpty()) {
			final Set<Long> updatedQuestionAnswerIds = qaList.stream().map(QuestionAnswer::getId)
					.collect(Collectors.toSet());
			final Set<Long> questionAnswersForDeleteIds = Sets.difference(allQuestionsAnswerIdList,
					updatedQuestionAnswerIds);
			questionAnswersForDeleteIds.forEach(e -> {
				this.questionAnswerDao.deleteQuestionAnswer(ImtQuestionAnswerEntityDelete.builder()
						.criteria(ImtQuestionAnswerCriteria.builder().addQuestionIdIncl(e).build()).build());
			});
		}
		if (qaList != null) {
			for (final QuestionAnswer qa : qaList) {
				if (qa.getId() == null) {

					this.questionAnswerDao.addQuestionAnswer(ImtQuestionAnswer.builder().questionId(qId)
							.answerText(qa.getAnswerText()).correct(qa.getCorrect()).build());
				} else {
					final QuestionAnswerCriteria questionAnswerCriteria = ImtQuestionAnswerCriteria.builder()
							.addIdIncl(qa.getId()).build();
					final QuestionAnswer dbQa = this.questionAnswerDao
							.getQuestionAnswer(
									ImtQuestionAnswerEntityGet.builder().criteria(questionAnswerCriteria).build())
							.stream().findFirst().orElse(null);
					this.questionAnswerDao.updateQuestionAnswer(ImtQuestionAnswerEntityUpdate.builder()
							.answerText(qa.getAnswerText() != null ? qa.getAnswerText() : dbQa.getAnswerText())
							.correct(qa.getCorrect() != null ? qa.getCorrect() : dbQa.getCorrect()).build());
				}
			}
		}
	}

	private void formGroupForRq(Rq rq) {

		// get the lesson for the rq
		final Lesson lessonOfRq = lessonDao
				.getLesson(ImtLessonEntityGet.builder()
						.criteria(ImtLessonCriteria.builder().addIdIncl(rq.getLessonId()).build()).build())
				.stream().findFirst().orElse(null);
		if (lessonOfRq == null) {
			throw new BusinessException("Request does not belong to any lesson");
		}

		// get the course name for the lesson
		final String courseNm = lessonOfRq.getCourseNm();

		// get the logged in students for this course
		final List<Student> studentsList = getCheckedInStudentsUsrnms(courseNm);

		switch (rq.getRqGrouppTp()) {
		case CHOOSE_GROUP: {
			break;
		}
		case RANDOM_GROUP: {
			if (!studentsList.isEmpty()) {
				formRandomRqGroupp(studentsList, rq.getRqGrouppSz(), rq.getId());

			}
			break;
		}
		case COMBINED_GROUP: {

		}
		default: {
			break;
		}
		}
	}

	private List<Student> getCheckedInStudentsUsrnms(String courseNm) {
		final List<String> studentsUsrnms = courseUsrDao
				.getCourseUsr(ImtCourseUsrEntityGet.builder()
						.criteria(ImtCourseUsrCriteria.builder().addCourseNmIncl(courseNm).build()).build())
				.stream().map(CourseUsr::getUsrUsrnm).collect(Collectors.toList());
		return studentDao
				.getStudent(ImtStudentEntityGet.builder()
						.criteria(ImtStudentCriteria.builder().usrUsrnmIncl(studentsUsrnms).build()).build())
				.stream().filter(e -> e.getCheckedIn()).collect(Collectors.toList());
	}

	private void formRandomRqGroupp(List<Student> students, Integer size, Long rqId) {
		// rearrange list randomly
		final long seed = System.nanoTime();
		Collections.shuffle(students, new Random(seed));

		// iterate through list
		for (int i = 0; i < students.size(); i += size) {

			try {
				// sublist to make a group
				final List<Student> group = students.subList(i, i + size);

				final RqGroupp rqGrouppToAdd = ImtRqGroupp.builder().rqId(rqId).rqStatus(RqStatus.OPENED)
						.crtTm(Instant.now()).build();
				rqGrouppDao.addRqGroupp(rqGrouppToAdd);

				// add group id for each student
				group.forEach(e -> studentDao.updateStudent(ImtStudentEntityUpdate.builder()
						.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(e.getUsrUsrnm()).build())
						.rqGrouppId(rqGrouppToAdd.getId()).build()));
			} catch (final Exception e) {
				throw new BusinessException("Group could not be formed. Contraints are inconsistent with the database");
			}

		}

	}

	private void lauchRqGroupp(Rq rq) {

		final List<QuestionRecord> questions = getQuestionForRq(rq.getId());

		// compute allowed time
		// per question => total time = time per question * number of questions
		final Integer allowedTm = rq.getTimeMeasurementTp() == TimeMeasurementTp.ON_QUESTION
				? questions.size() * rq.getAllowedTm()
				: rq.getAllowedTm();

		rqGrouppDao.updateRqGroupp(ImtRqGrouppEntityUpdate.builder().rqStatus(RqStatus.ONGOING)
				.criteria(ImtRqGrouppCriteria.builder().addRqIdIncl(rq.getId()).build()).build());
		deactivateRqRandomGroupp(allowedTm, rq.getId());
	}

	private void deactivateRqRandomGroupp(Integer sec, Long rqId) {
		new java.util.Timer().schedule(new java.util.TimerTask() {
			@Override
			public void run() {
				rqDao.updateRq(ImtRqEntityUpdate.builder().rqStatus(RqStatus.TERMINATED)
						.criteria(ImtRqCriteria.builder().addIdIncl(rqId).build()).build());
				rqGrouppDao.updateRqGroupp(ImtRqGrouppEntityUpdate.builder().rqStatus(RqStatus.TERMINATED)
						.criteria(ImtRqGrouppCriteria.builder().addRqIdIncl(rqId).build()).build());
			}
		}, sec * 1000);
	}

}
