package ro.utcn.grouping.business.impl.student;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import ro.utcn.grouping.business.api.student.StudentBusiness;
import ro.utcn.grouping.data.api.course.CourseDao;
import ro.utcn.grouping.data.api.course.CourseUsrDao;
import ro.utcn.grouping.data.api.course.ImtCourseEntityGet;
import ro.utcn.grouping.data.api.course.ImtCourseUsrEntityGet;
import ro.utcn.grouping.data.api.groupp.GrouppDao;
import ro.utcn.grouping.data.api.groupp.ImtGrouppEntityGet;
import ro.utcn.grouping.data.api.question.ImtQuestionAnswerEntityGet;
import ro.utcn.grouping.data.api.question.ImtQuestionEntityGet;
import ro.utcn.grouping.data.api.question.QuestionAnswerDao;
import ro.utcn.grouping.data.api.question.QuestionDao;
import ro.utcn.grouping.data.api.rq.ImtRqEntityGet;
import ro.utcn.grouping.data.api.rq.ImtRqGrouppEntityGet;
import ro.utcn.grouping.data.api.rq.RqDao;
import ro.utcn.grouping.data.api.rq.RqGrouppDao;
import ro.utcn.grouping.data.api.rsp.ImtRspEntityGet;
import ro.utcn.grouping.data.api.rsp.ImtRspEntityUpdate;
import ro.utcn.grouping.data.api.rsp.RspDao;
import ro.utcn.grouping.data.api.student.ImtStudentEntityGet;
import ro.utcn.grouping.data.api.student.ImtStudentEntityUpdate;
import ro.utcn.grouping.data.api.student.StudentDao;
import ro.utcn.grouping.model.base.BusinessException;
import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseDetail;
import ro.utcn.grouping.model.course.CourseUsr;
import ro.utcn.grouping.model.course.ImtCourse;
import ro.utcn.grouping.model.course.ImtCourseCriteria;
import ro.utcn.grouping.model.course.ImtCourseDetail;
import ro.utcn.grouping.model.course.ImtCourseUsr;
import ro.utcn.grouping.model.course.ImtCourseUsrCriteria;
import ro.utcn.grouping.model.course.ImtStudentCourseGetResult;
import ro.utcn.grouping.model.course.StudentCourseGetResult;
import ro.utcn.grouping.model.groupp.Groupp;
import ro.utcn.grouping.model.groupp.GrouppGetResult;
import ro.utcn.grouping.model.groupp.GrouppStudent;
import ro.utcn.grouping.model.groupp.GrouppTp;
import ro.utcn.grouping.model.groupp.ImtGroupp;
import ro.utcn.grouping.model.groupp.ImtGrouppCriteria;
import ro.utcn.grouping.model.groupp.ImtGrouppGetResult;
import ro.utcn.grouping.model.question.ImtQuestionAnswerCriteria;
import ro.utcn.grouping.model.question.ImtQuestionCriteria;
import ro.utcn.grouping.model.question.ImtQuestionRecord;
import ro.utcn.grouping.model.question.Question;
import ro.utcn.grouping.model.question.QuestionRecord;
import ro.utcn.grouping.model.rq.ImtRq;
import ro.utcn.grouping.model.rq.ImtRqCriteria;
import ro.utcn.grouping.model.rq.ImtRqGroupp;
import ro.utcn.grouping.model.rq.ImtRqGrouppCriteria;
import ro.utcn.grouping.model.rq.ImtRqRecord;
import ro.utcn.grouping.model.rq.Rq;
import ro.utcn.grouping.model.rq.RqGroupp;
import ro.utcn.grouping.model.rq.RqRecord;
import ro.utcn.grouping.model.rq.RqTp;
import ro.utcn.grouping.model.rsp.ImtRspCreateList;
import ro.utcn.grouping.model.rsp.ImtRspCreateResult;
import ro.utcn.grouping.model.rsp.ImtRspCreateResultList;
import ro.utcn.grouping.model.rsp.ImtRspCriteria;
import ro.utcn.grouping.model.rsp.ImtRspGetResult;
import ro.utcn.grouping.model.rsp.ImtRspRecord;
import ro.utcn.grouping.model.rsp.Rsp;
import ro.utcn.grouping.model.rsp.RspCreateList;
import ro.utcn.grouping.model.rsp.RspCreateResult;
import ro.utcn.grouping.model.rsp.RspCreateResultList;
import ro.utcn.grouping.model.rsp.RspGetResult;
import ro.utcn.grouping.model.rsp.RspRecord;
import ro.utcn.grouping.model.student.ImtStudentCriteria;
import ro.utcn.grouping.model.student.Student;

public class StudentBusinessImpl implements StudentBusiness {

	CourseUsrDao courseUsrDao;
	CourseDao courseDao;
	GrouppDao grouppDao;
	StudentDao studentDao;
	RqDao rqDao;
	RqGrouppDao rqGrouppDao;
	QuestionDao questionDao;
	QuestionAnswerDao questionAnswerDao;
	RspDao rspDao;

	@Inject
	public void setCourseUsrDao(CourseUsrDao courseUsrDao) {
		this.courseUsrDao = courseUsrDao;
	}

	@Inject
	public void setCourseDao(CourseDao courseDao) {
		this.courseDao = courseDao;
	}

	@Inject
	public void setGrouppDao(GrouppDao grouppDao) {
		this.grouppDao = grouppDao;
	}

	@Inject
	public void setStudentDao(StudentDao studentDao) {
		this.studentDao = studentDao;
	}

	@Inject
	public void setRqDao(RqDao rqDao) {
		this.rqDao = rqDao;
	}

	@Inject
	public void setQuestionDao(QuestionDao questionDao) {
		this.questionDao = questionDao;
	}

	@Inject
	public void setQuestionAnswerDao(QuestionAnswerDao questionAnswerDao) {
		this.questionAnswerDao = questionAnswerDao;
	}

	@Inject
	public void setRqGrouppDao(RqGrouppDao rqGrouppDao) {
		this.rqGrouppDao = rqGrouppDao;
	}

	@Inject
	public void setRspDao(RspDao rspDao) {
		this.rspDao = rspDao;
	}

	@Override
	public RspCreateResultList addRsps(RspCreateList rspCreateList) {
		final List<RspCreateResult> rspCreatedList = Lists.newArrayList();
		rspCreateList.getList().stream().forEach(rsp -> {
			final Student stud = getStudentByUsrnm(rsp.getStudentUsrnm());
			if (stud == null) {
				throw new BusinessException("Student not found");
			}
			final Question q = questionDao
					.getQuestion(ImtQuestionEntityGet.builder()
							.criteria(ImtQuestionCriteria.builder().addIdIncl(rsp.getQuestionId()).build()).build())
					.stream().findFirst().orElse(null);

			if (q == null) {
				throw new BusinessException("Question not found");
			}
			// we don't add the response if it's not null
			if (rsp.getRspText() != null) {
				rspDao.addRsp(rsp);
				rspCreatedList.add(
						ImtRspCreateResult.builder().rqGrouppId(rsp.getRqGrouppId()).rspText(rsp.getRspText()).build());
			}
		});

		return ImtRspCreateResultList.builder().list(rspCreatedList).build();
	}

	@Override
	public RspGetResult getRspsForRqGroupp(Long rqGrouppId) {

		final List<Rsp> rspListTemp = rspDao.getRsp(ImtRspEntityGet.builder()
				.criteria(ImtRspCriteria.builder().addRqGrouppIdIncl(rqGrouppId).build()).build());

		final List<Long> questionsIdList = Lists.newArrayList();

		rspListTemp.forEach(e -> {
			if (!questionsIdList.contains(e.getQuestionId())) {
				questionsIdList.add(e.getQuestionId());
			}
		});
		final List<RspRecord> rspRecords = Lists.newArrayList();

		questionsIdList.forEach(e -> {
			final String questionText = questionDao
					.getQuestion(ImtQuestionEntityGet.builder()
							.criteria(ImtQuestionCriteria.builder().addIdIncl(e).build()).build())
					.stream().findFirst().map(Question::getQuestionText).orElse(null);

			final List<Rsp> rsps = rspDao.getRsp(ImtRspEntityGet.builder()
					.criteria(ImtRspCriteria.builder().addRqGrouppIdIncl(rqGrouppId).addQuestionIdIncl(e).build())
					.build());
			rspRecords.add(ImtRspRecord.builder().questionText(questionText).list(rsps).build());

		});

		return ImtRspGetResult.builder().list(rspRecords).build();
	}

	@Override
	public RspCreateList voteRsp(RspCreateList rspCreateList) {

		rspCreateList.getList().forEach(e -> {
			final Rsp rsp = rspDao.getRsp(
					ImtRspEntityGet.builder().criteria(ImtRspCriteria.builder().addIdIncl(e.getId()).build()).build())
					.stream().findFirst().orElse(null);

			if (rsp == null) {
				throw new BusinessException("Response not found");
			}
			Integer voteCount = rsp.getVoteCount();
			voteCount++;

			rspDao.updateRsp(ImtRspEntityUpdate.builder()
					.criteria(ImtRspCriteria.builder().addIdIncl(e.getId()).build()).voteCount(voteCount).build());
		});

		return ImtRspCreateList.builder()
				.list(rspDao
						.getRsp(ImtRspEntityGet.builder()
								.criteria(ImtRspCriteria.builder().addRqGrouppIdIncl(
										rspCreateList.getList().stream().findFirst().map(Rsp::getRqGrouppId).orElse(0L))
										.build())
								.build()))
				.build();
	}

	@Override
	public Course enrollStudent(String usrUsrnm, String courseNm) {

		final Course course = courseDao
				.getCourse(ImtCourseEntityGet.builder()
						.criteria(ImtCourseCriteria.builder().addNmIncl(courseNm).build()).build())
				.stream().findFirst().orElse(null);
		if (course == null) {
			throw new BusinessException("Course name is invalid");
		} else {
			final CourseUsr courseUsr = ImtCourseUsr.builder().courseNm(courseNm).usrUsrnm(usrUsrnm).build();
			courseUsrDao.addCourseUsr(courseUsr);
			return ImtCourse.copyOf(course);
		}

	}

	@Override
	public GrouppGetResult getSemesterGrouppsByChoice(String courseNm) {
		if (!getCourseByNm(courseNm).isPresent()) {
			throw new BusinessException("Course name does not exist");
		}

		final List<Groupp> groupps = grouppDao.getGroupp(ImtGrouppEntityGet.builder().criteria(
				ImtGrouppCriteria.builder().addCourseNmIncl(courseNm).addGrouppTpIncl(GrouppTp.SEM_PROJ_CHOOSE).build())
				.build());
		final List<Groupp> gouppsWithStuds = Lists.newArrayList();
		groupps.forEach(e -> {
			gouppsWithStuds.add(ImtGroupp.builder().studentsUsrUsrnmsInGroupp(getStudentsInGroupp(e.getId()))
					.courseNm(e.getCourseNm()).grouppTp(e.getGrouppTp()).id(e.getId()).projNm(e.getProjNm())
					.size(e.getSize()).leaderUsrnm(e.getLeaderUsrnm()).mandate(e.getMandate()).build());
		});
		return ImtGrouppGetResult.builder().list(gouppsWithStuds).build();
	}

	@Override
	public int joinGroupp(GrouppStudent grouppStudent) {
		return studentDao.updateStudent(ImtStudentEntityUpdate.builder()
				.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(grouppStudent.getUsrUsrnm()).build())
				.grouppId(grouppStudent.getGouppId()).build());
	}

	@Override
	public RqRecord getRqForStudent(String studentUsrnm) {

		final Student stud = getStudentByUsrnm(studentUsrnm);

		if (stud == null) {
			throw new BusinessException("Student not found");
		}

		final RqGroupp rqGroupp = rqGrouppDao
				.getRqGroupp(ImtRqGrouppEntityGet.builder()
						.criteria(ImtRqGrouppCriteria.builder().addIdIncl(stud.getRqGrouppId()).build()).build())
				.stream().findFirst().orElse(null);

		if (rqGroupp == null) {
			throw new BusinessException("Request group not found");
		}
		final Rq rq = rqDao.getRq(ImtRqEntityGet.builder()
				.criteria(ImtRqCriteria.builder().addIdIncl(rqGroupp.getRqId()).build()).build()).stream().findFirst()
				.orElse(null);
		if (rq == null) {
			throw new BusinessException("Request not found");
		}

		return ImtRqRecord.builder().rq(ImtRq.copyOf(rq)).questions(getQuestionForRq(rqGroupp.getRqId())).build();

	}

	@Override
	public StudentCourseGetResult getAvailableCourses(String usrnm) {
		final List<Course> courses = courseDao
				.getCourse(ImtCourseEntityGet.builder().criteria(ImtCourseCriteria.builder().build()).build());
		final List<String> courseNmsForUsr = courseUsrDao
				.getCourseUsr(ImtCourseUsrEntityGet.builder()
						.criteria(ImtCourseUsrCriteria.builder().addUsrUsrnmIncl(usrnm).build()).build())
				.stream().map(CourseUsr::getCourseNm).collect(Collectors.toList());
		final List<CourseDetail> courseDetailList = Lists.newArrayList();
		courses.forEach(e -> {
			courseDetailList
					.add(ImtCourseDetail.builder().course(e).enrolled(courseNmsForUsr.contains(e.getNm())).build());
		});
		return ImtStudentCourseGetResult.builder().list(courseDetailList).build();
	}

	@Override
	public RqGroupp getRqGrouppForStudent(String studentUsrnm) {
		final Student stud = getStudentByUsrnm(studentUsrnm);

		if (stud == null) {
			throw new BusinessException("Student not found");
		}

		if (stud.getRqGrouppId() != null) {
			final RqGroupp rqGrouppForStud = rqGrouppDao
					.getRqGroupp(ImtRqGrouppEntityGet.builder()
							.criteria(ImtRqGrouppCriteria.builder().addIdIncl(stud.getRqGrouppId()).build()).build())
					.stream().findFirst().orElse(null);
			return rqGrouppForStud != null ? ImtRqGroupp.copyOf(rqGrouppForStud) : null;
		}
		return null;
	}

	private Student getStudentByUsrnm(String studentUsrnm) {
		return studentDao
				.getStudent(ImtStudentEntityGet.builder()
						.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(studentUsrnm).build()).build())
				.stream().findFirst().orElse(null);
	}

	private List<String> getStudentsInGroupp(Long grouppId) {
		final List<Student> students = studentDao.getStudent(ImtStudentEntityGet.builder()
				.criteria(ImtStudentCriteria.builder().addGrouppIdIncl(grouppId).build()).build());
		return students.size() != 0 && students != null
				? students.stream().map(Student::getUsrUsrnm).collect(Collectors.toList())
				: ImmutableList.of();
	}

	private Optional<Course> getCourseByNm(String nm) {
		return courseDao.getCourse(
				ImtCourseEntityGet.builder().criteria(ImtCourseCriteria.builder().addNmIncl(nm).build()).build())
				.stream().findFirst();
	}

	private List<QuestionRecord> getQuestionForRq(Long rqId) {
		final Rq selectedRq = rqDao
				.getRq(ImtRqEntityGet.builder().criteria(ImtRqCriteria.builder().addIdIncl(rqId).build()).build())
				.stream().findFirst().orElse(null);
		if (selectedRq == null) {
			throw new BusinessException("The request cannot be found");
		} else {
			final List<Question> questionsForRq = this.questionDao.getQuestion(ImtQuestionEntityGet.builder()
					.criteria(ImtQuestionCriteria.builder().rqIdIncl(ImmutableList.of(rqId)).build()).build());
			final List<QuestionRecord> questionRecordList = Lists.newArrayList();
			if (selectedRq.getRqTp() == RqTp.TEXT_FILLING) {
				questionsForRq.forEach(e -> questionRecordList.add(ImtQuestionRecord.builder().question(e).build()));
			} else {
				questionsForRq.forEach(e -> questionRecordList.add(ImtQuestionRecord.builder().question(e)
						.questionAnswerList(questionAnswerDao.getQuestionAnswer(ImtQuestionAnswerEntityGet.builder()
								.criteria(ImtQuestionAnswerCriteria.builder().addQuestionIdIncl(e.getId()).build())
								.build()))
						.build()));
			}

			return questionRecordList;
		}
	}

}
