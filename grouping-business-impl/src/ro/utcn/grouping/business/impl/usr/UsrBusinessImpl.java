package ro.utcn.grouping.business.impl.usr;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;

import ro.utcn.grouping.business.api.usr.UsrBusiness;
import ro.utcn.grouping.data.api.course.CourseDao;
import ro.utcn.grouping.data.api.course.CourseUsrDao;
import ro.utcn.grouping.data.api.course.ImtCourseEntityGet;
import ro.utcn.grouping.data.api.course.ImtCourseUsrEntityGet;
import ro.utcn.grouping.data.api.lesson.ImtLessonEntityGet;
import ro.utcn.grouping.data.api.lesson.LessonDao;
import ro.utcn.grouping.data.api.proffessor.ProffessorDao;
import ro.utcn.grouping.data.api.student.ImtStudentEntityGet;
import ro.utcn.grouping.data.api.student.ImtStudentEntityUpdate;
import ro.utcn.grouping.data.api.student.StudentDao;
import ro.utcn.grouping.data.api.usr.ImtUsrEntityGet;
import ro.utcn.grouping.data.api.usr.UsrDao;
import ro.utcn.grouping.model.base.BusinessException;
import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseGetResult;
import ro.utcn.grouping.model.course.CourseRecord;
import ro.utcn.grouping.model.course.CourseUsr;
import ro.utcn.grouping.model.course.ImtCourseCriteria;
import ro.utcn.grouping.model.course.ImtCourseGetResult;
import ro.utcn.grouping.model.course.ImtCourseRecord;
import ro.utcn.grouping.model.course.ImtCourseUsrCriteria;
import ro.utcn.grouping.model.lesson.ImtLessonCriteria;
import ro.utcn.grouping.model.lesson.Lesson;
import ro.utcn.grouping.model.proffesor.ImtProffessor;
import ro.utcn.grouping.model.student.ImtStudent;
import ro.utcn.grouping.model.student.ImtStudentCriteria;
import ro.utcn.grouping.model.usr.ImtUsr;
import ro.utcn.grouping.model.usr.ImtUsrCreateResult;
import ro.utcn.grouping.model.usr.ImtUsrCriteria;
import ro.utcn.grouping.model.usr.ImtUsrGetResult;
import ro.utcn.grouping.model.usr.LoginUsr;
import ro.utcn.grouping.model.usr.MdfUsr;
import ro.utcn.grouping.model.usr.Usr;
import ro.utcn.grouping.model.usr.UsrCreateResult;
import ro.utcn.grouping.model.usr.UsrGet;
import ro.utcn.grouping.model.usr.UsrGetResult;
import ro.utcn.grouping.model.usr.UsrTp;

public class UsrBusinessImpl implements UsrBusiness {

	UsrDao usrDao;
	StudentDao studentDao;
	ProffessorDao proffessorDao;
	CourseDao courseDao;
	CourseUsrDao courseUsrDao;
	LessonDao lessonDao;

	@Inject
	public void setUsrDao(UsrDao usrDao) {
		this.usrDao = usrDao;
	}

	@Inject
	public void setStudentDao(StudentDao studentDao) {
		this.studentDao = studentDao;
	}

	@Inject
	public void setProffessorDao(ProffessorDao proffessorDao) {
		this.proffessorDao = proffessorDao;
	}

	@Inject
	public void setCourseDao(CourseDao courseDao) {
		this.courseDao = courseDao;
	}

	@Inject
	public void setCourseUsrDao(CourseUsrDao courseUsrDao) {
		this.courseUsrDao = courseUsrDao;
	}

	@Inject
	public void setLessonDao(LessonDao lessonDao) {
		this.lessonDao = lessonDao;
	}

	@Override
	public UsrCreateResult createUsr(Usr usr) {
		if (!checkIfEmailIsAvailable(usr.getEmail()) && !checkIfUsrnmIsAvailable(usr.getUsrnm())) {
			throw new BusinessException("Email or userame already taken");
		} else {

			usrDao.addUsr(MdfUsr.create().from(usr)
					.setPsswd(Hashing.md5().hashString(usr.getPsswd(), Charsets.UTF_8).toString()));
			if (usr.getUsrTp().equals(UsrTp.STUDENT)) {
				studentDao.addStudent(ImtStudent.builder().usrUsrnm(usr.getUsrnm()).usrEmail(usr.getEmail()).build());
			} else if (usr.getUsrTp().equals(UsrTp.PROFFESSOR)) {
				proffessorDao.addProffessor(
						ImtProffessor.builder().usrUsrnm(usr.getUsrnm()).usrEmail(usr.getEmail()).build());
			}
			return ImtUsrCreateResult.builder().email(usr.getEmail()).usrnm(usr.getUsrnm()).build();
		}

	}

	@Override
	public Usr deleteUsr(String usrnm) {
		return null;
	}

	@Override
	public Usr loginUsr(LoginUsr loginUsr) {

		final Usr usr = getUsr(loginUsr.getUsrnm());
		if (usr == null
				|| !usr.getPsswd().equals(Hashing.md5().hashString(loginUsr.getPsswd(), Charsets.UTF_8).toString())) {
			throw new BusinessException("Invalid username or password");
		} else {
			if (checkIfUsrIsStudent(loginUsr.getUsrnm())) {
				checkIn(loginUsr.getUsrnm());
			}
			return ImtUsr.copyOf(usr);
		}

	}

	@Override
	public Usr logoutUsr(String usrnm) {
		final Usr usr = getUsr(usrnm);
		if (usr == null) {
			throw new BusinessException("The user does not exist ");
		}
		if (checkIfUsrIsStudent(usrnm)) {
			checkOut(usrnm);
		}
		return ImtUsr.copyOf(usr);

	}

	@Override
	public Usr getUsr(String usrnm) {
		return usrDao.getUsr(
				ImtUsrEntityGet.builder().criteria(ImtUsrCriteria.builder().addUsrnmIncl(usrnm).build()).build())
				.stream().findFirst().orElse(null);
	}

	@Override
	public UsrGetResult getUsrs(UsrGet usrGet) {
		final List<Usr> usrList = usrDao.getUsr(ImtUsrEntityGet.builder()
				.criteria(ImtUsrCriteria.builder()
						.usrnmIncl(usrGet.getUsrnm() != null ? ImmutableList.of(usrGet.getUsrnm()) : ImmutableList.of())
						.emailIncl(usrGet.getEmail() != null ? ImmutableList.of(usrGet.getEmail()) : ImmutableList.of())
						.build())
				.build());
		return ImtUsrGetResult.builder().list(usrList).build();
	}

	@Override
	public CourseGetResult getCoursesForUsr(String usrnm) {
		final List<String> courseNms = courseUsrDao
				.getCourseUsr(ImtCourseUsrEntityGet.builder()
						.criteria(ImtCourseUsrCriteria.builder().addUsrUsrnmIncl(usrnm).build()).build())
				.stream().map(CourseUsr::getCourseNm).collect(Collectors.toList());

		final List<CourseRecord> courseRecords = Lists.newArrayList();
		if (!courseNms.isEmpty()) {
			final List<Course> courses = courseDao.getCourse(ImtCourseEntityGet.builder()
					.criteria(ImtCourseCriteria.builder().addAllNmIncl(courseNms).build()).build());
			courses.forEach(e -> {
				final List<Lesson> lessonsForCourse = lessonDao.getLesson(ImtLessonEntityGet.builder()
						.criteria(ImtLessonCriteria.builder().addCourseNmIncl(e.getNm()).build()).build());
				courseRecords.add(ImtCourseRecord.builder().course(e).lessonsList(lessonsForCourse).build());
			});

		}
		return ImtCourseGetResult.builder().addAllList(courseRecords).build();
	}

	@Override
	public CourseRecord getCourseForUsrByCourseNm(String usrnm, String courseNm) {
		final List<String> courseNms = courseUsrDao.getCourseUsr(ImtCourseUsrEntityGet.builder()
				.criteria(ImtCourseUsrCriteria.builder().addUsrUsrnmIncl(usrnm).addCourseNmIncl(courseNm).build())
				.build()).stream().map(CourseUsr::getCourseNm).collect(Collectors.toList());
		final Course course = courseDao
				.getCourse(ImtCourseEntityGet.builder()
						.criteria(ImtCourseCriteria.builder().addAllNmIncl(courseNms).build()).build())
				.stream().findFirst().orElse(null);
		return ImtCourseRecord.builder().course(course)
				.lessonsList(lessonDao.getLesson(ImtLessonEntityGet.builder()
						.criteria(ImtLessonCriteria.builder().addCourseNmIncl(course.getNm()).build()).build()))
				.build();
	}

	private boolean checkIn(String studentUsrnm) {
		final int count = this.studentDao.updateStudent(ImtStudentEntityUpdate.builder()
				.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(studentUsrnm).build()).checkedIn(true).build());
		return count == 1;

	}

	private boolean checkOut(String studentUsrnm) {
		final int count = this.studentDao.updateStudent(ImtStudentEntityUpdate.builder()
				.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(studentUsrnm).build()).checkedIn(false).build());
		return count == 1;
	}

	private boolean checkIfEmailIsAvailable(String email) {
		return usrDao.getUsr(
				ImtUsrEntityGet.builder().criteria(ImtUsrCriteria.builder().addEmailIncl(email).build()).build())
				.isEmpty();
	}

	private boolean checkIfUsrnmIsAvailable(String usrnm) {
		return usrDao.getUsr(
				ImtUsrEntityGet.builder().criteria(ImtUsrCriteria.builder().addUsrnmIncl(usrnm).build()).build())
				.isEmpty();
	}

	private boolean checkIfUsrIsStudent(String usrnm) {
		return studentDao
				.getStudent(ImtStudentEntityGet.builder()
						.criteria(ImtStudentCriteria.builder().addUsrUsrnmIncl(usrnm).build()).build())
				.stream().findFirst().isPresent();
	}

}
