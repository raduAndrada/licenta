package ro.utcn.grouping.business.base;

@FunctionalInterface
public interface CurrentUsrSession {

	String getUsrSession();

}
