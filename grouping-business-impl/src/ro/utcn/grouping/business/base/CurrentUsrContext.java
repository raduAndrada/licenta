package ro.utcn.grouping.business.base;

@FunctionalInterface
public interface CurrentUsrContext {

	String getUsername();

}
