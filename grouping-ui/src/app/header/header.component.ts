import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Usr } from '../shared/usr.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {

  }

  getUsrLoggedInUsrnm() {
      const usrLoggedIn: Usr = this.authService.getUsrLoggedIn();
      if (usrLoggedIn !== null ) {
      return usrLoggedIn.usrnm;
    } else {
      return '';
    }
  }

}
