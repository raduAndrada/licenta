import { Injectable } from '@angular/core';
import { RspDetail } from '../shared/rsp/rsp-detail.model';
import { Subject } from 'rxjs/Subject';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { RspDetailGetResult } from '../shared/rsp/rsp-detail-get-result.model';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class RspService {

    rspDetails: RspDetail [];
    rspDetailChanged = new Subject<RspDetail[]> ();
    shouldReload: boolean;
    shouldReloadChange = new Subject<boolean>();

    private proffessorsUrl = 'http://localhost:8000/v1/proffessors';

    private handleError<T> (operation = 'operation', result?: T) {
     return (error: any): Observable<T> => {
       console.error(error); // log to console instead
       return of(result as T);
     };
   }


   getShouldReload() {
     return this.shouldReload;
   }

   setRspDetails(rspDetails: RspDetail []) {
       this.rspDetails = rspDetails;
       this.rspDetailChanged.next(this.rspDetails.slice());
   }

   setShouldReload(reload: boolean) {
     this.shouldReload = reload;
     this.shouldReloadChange.next(this.shouldReload);
   }


    getRspDetails(rqId: number): Observable<RspDetailGetResult> {
        return this.http.get<RspDetailGetResult>( this.proffessorsUrl + '/lessons/requests/responses/' + rqId )
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<RspDetailGetResult>('getGroupps for course'))
              );
    }


   constructor( private http: HttpClient) {
   }
}
