import { Component, OnInit } from '@angular/core';
import { RspService } from './rsp-service.service';
import { ActivatedRoute, Params } from '@angular/router';
import { RspDetailGetResult } from '../shared/rsp/rsp-detail-get-result.model';
import { RspDetail } from '../shared/rsp/rsp-detail.model';

@Component({
  selector: 'app-rsps',
  templateUrl: './rsps.component.html',
  styleUrls: ['./rsps.component.css']
})
export class RspsComponent implements OnInit {

  rqId: number;
  rspDetailList = [];

  constructor(private rspService: RspService,
              private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.rqId = + params['rqId'];
      this.rspService.setShouldReload(true);
    });
     this.rspService.getRspDetails(this.rqId).subscribe((rspDetails: RspDetailGetResult) => {
      if (rspDetails != null) {
          this.rspService.setRspDetails(rspDetails.list);
          } else {
          // alert(this.message);
      }
      });
    this.rspService.rspDetailChanged.subscribe((rspDetailList: RspDetail[]) => {
      this.rspDetailList = rspDetailList;
    });
  }

}
