import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Course } from '../../shared/course/course.model';


@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {

  course = null ;
  usrnm = '';
  courseNm = '';
  message: string;
  constructor(private courseService: CourseService,
              private route: ActivatedRoute
  ) { }


  ngOnInit() {
    this.route.queryParams
    .subscribe(params => {
      this.usrnm = params['usrnm'];
    });
   this.courseNm = this.route.snapshot.params['courseNm'];
   this.courseService.getCourseForUsrByCourseNm(this.usrnm, this.courseNm)
                .subscribe(
                    (course: Course ) => {this.course = course ; });
  }

  updateCourse() {
    this.courseService.updateCourse(this.course)
      .subscribe(updatedCourse => {
          this.course = updatedCourse;
          if (updatedCourse != null) {
              this.message = 'Course with ' + this.course.nm + ' has been successfully updated';
              alert(this.message);
              } else {
              this.message = 'Course update failed';
              alert(this.message);
          }
      });
  }

}
