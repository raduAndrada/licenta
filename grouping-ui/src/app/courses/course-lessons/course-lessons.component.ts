import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { Lesson } from '../../shared/course/lesson.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-course-lessons',
  templateUrl: './course-lessons.component.html',
  styleUrls: ['./course-lessons.component.css']
})
export class CourseLessonsComponent implements OnInit {

  lessons = [];
  courseNm = '';
  constructor(
              private courseService: CourseService,
              private route: ActivatedRoute,
              private router: Router
  ) { }

  ngOnInit() {
    this.courseNm = this.route.snapshot.params['courseNm'];
    this.route.params
    .subscribe(params => {
      this.courseNm = params['courseNm'];
    });
    this.lessons = this.courseService.getLessonsForCurrentCourse();
    this.courseService.lessonsForCurrentCourseChange.subscribe(
      (lessons: Lesson[]) => {
        this.lessons = lessons;
      }
    );
  }

  onViewRqList(lessonId: number) {
    this.router.navigate(['requests', this.courseNm, lessonId]);
  }

}
