import { Component, OnInit } from '@angular/core';
import { CourseService } from './course.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseRecord } from '../shared/course/course-record.model';
import { CourseGet } from '../shared/course/course-get.model';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {


  courses: CourseRecord[];
  usrnm: string;

  constructor(private courseService: CourseService,
              private route: ActivatedRoute,
              private router: Router
            ) { }

  ngOnInit() {
    this.usrnm = this.route.snapshot.params['usrnm'];
    this.courses = this.courseService.getCoursesForUsrByUsrnm(this.usrnm).list;
    this.courseService.courseGetChange.subscribe(
      (courseGet: CourseGet) => {
        this.courses = courseGet.list;
      }
    );
  }

  addCourse() {
    this.router.navigate(['create'], {relativeTo : this.route});
  }

  editCourse(coursenm: string) {
    this.router.navigate([coursenm], {relativeTo : this.route, queryParams: { usrnm: this.usrnm }});
  }

  viewGroups(coursenm: string) {
    this.router.navigate([coursenm, 'group-list'], {relativeTo : this.route });
  }

  addGroups(coursenm: string) {
    this.router.navigate([coursenm, 'group'], {relativeTo : this.route});
  }

  editLessons(course: CourseRecord) {
    this.courseService.setLessonForCurrentCourse(course.lessonsList);
    this.router.navigate([course.course.nm, 'lessons'], {relativeTo : this.route});
  }

}
