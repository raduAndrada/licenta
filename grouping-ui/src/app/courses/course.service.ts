import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { UsrGet } from '../shared/usr-get.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Usr } from '../shared/usr.model';
import { Inject, Injectable } from '@angular/core';
import { CourseGet } from '../shared/course/course-get.model';
import { Course } from '../shared/course/course.model';
import { Lesson } from '../shared/course/lesson.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CourseService {

    private usrsUrl = 'http://localhost:8000/v1/usrs/courses';
    private studentsUrl = 'http://localhost:8000/v1/students/courses';
    private proffessorsUrl = 'http://localhost:8000/v1/proffessors/courses';

    lessonsForCurrentCourse = [];
    lessonsForCurrentCourseChange = new Subject<Lesson[]>();

    courseGet = new CourseGet();
    courseGetChange = new Subject<CourseGet>();

    usrnm = '';

    constructor( private http: HttpClient) {
    }

      getAvailableCourses(): Observable<CourseGet> {
          return this.http.get<CourseGet>( this.usrsUrl)
          .pipe(
                  tap(_ => console.log(_)),
                  catchError(this.handleError<CourseGet>('getAvailableCourses'))
                );
      }

      getCoursesForUsrByUsrnm(usrnm: string): CourseGet {
          const url = this.usrsUrl + '/' + usrnm;
          this.usrnm = usrnm;
          this.http.get<CourseGet>(url)
          .pipe(
            tap(_ => console.log(_)),
            catchError(this.handleError<Course>('getCorseForUsrCourseNm'))
          )
          .subscribe(
            (courseGet: CourseGet) => {
              this.courseGet = courseGet;
              this.courseGetChange.next(this.courseGet);
            }
          );
          return this.courseGet;
      }

      getCourseForUsrByCourseNm(usrnm: string, courseNm: string): Observable<Course> {

        const url = this.usrsUrl + '/' + usrnm + '/' + courseNm;
        return this.http.get<Course>(url)
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<Course>('getCorseForUsrCourseNm'))
              );
       }


      addCourse(course: Course) {
        const url = this.proffessorsUrl;
        const usrUrl = this.usrsUrl + '/' + this.usrnm;
        let courseAdded = null;
        this.http.post<Course>(url, course)
        .pipe(
          tap(_ => console.log(_)),
          catchError(this.handleError<Course>('addCourse'))
        )
        .subscribe(
           (c: Course) => {
            courseAdded = c;
            this.http.get<CourseGet>(usrUrl)
                      .pipe(
                        tap(_ => console.log(_)),
                        catchError(this.handleError<Course>('getCorseForUsrCourseNm'))
                      )
                      .subscribe(
                        (courseGet: CourseGet) => {
                          this.courseGet = courseGet;
                          this.courseGetChange.next(this.courseGet);
                        }
                      );
          }
        );
        setTimeout(() => {
          return courseAdded;
           }, 500);
        }


      updateCourse(course: Course): Observable<Course> {
      const url = this.proffessorsUrl;
      return this.http.put<Course>(url, course)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<Course>('updateCourse'))
            );
        }

      enrollCourse(course: Course): Observable<Course> {
      const url = this.studentsUrl;
      return this.http.post<Course>(url, course)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<Course>('enrollCourse'))
            );
          }

      getLessonsForCurrentCourse(): Lesson[] {
        return this.lessonsForCurrentCourse;
      }

      setLessonForCurrentCourse(lessons: Lesson[]): void {
        this.lessonsForCurrentCourse = lessons;
        this.lessonsForCurrentCourseChange.next(this.lessonsForCurrentCourse.slice());
      }

      private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error); // log to console instead
          return of(result as T);
        };
      }

}
