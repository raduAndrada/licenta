import { Component, OnInit } from '@angular/core';

import { CourseService } from '../course.service';
import { Course } from '../../shared/course/course.model';

@Component({
  selector: 'app-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.css']
})
export class CourseCreateComponent implements OnInit {

  courseToAdd = new Course('', 0, '');
  message = '';
  constructor(private courseService: CourseService) { }

  ngOnInit() {
  }

  addCourse() {

    const addedCourse = this.courseService.addCourse(this.courseToAdd);
    }
}
