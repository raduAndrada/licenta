import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app.routing.module';
import { UsrService } from './usrs/usr.service';
import { UsrListComponent } from './usrs/usr-list/usr-list.component';
import { UsrDetailComponent } from './usrs/usr-detail/usr-detail.component';
import { UsrEditComponent } from './usrs/usr-edit/usr-edit.component';
import { HttpClientModule } from '@angular/common/http';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailComponent } from './courses/course-detail/course-detail.component';
import { CourseService } from './courses/course.service';
import { AuthService } from './shared/auth.service';
import { CourseCreateComponent } from './courses/course-create/course-create.component';
import { HTTP_INTERCEPTORS} from '@angular/common/http';
import { AuthInterceptor } from './shared/auth.interceptor';
import { DropdownDirective } from './shared/dropdown.directive';
import { GrouppService } from './groupps/groupp-service.service';
import { GrouppCreateComponent } from './groupps/groupp-create/groupp-create.component';
import { GrouppsComponent } from './groupps/groupps.component';
import { EnumToArrayPipe } from './shared/enum-to-array.pipe';
import { CourseLessonsComponent } from './courses/course-lessons/course-lessons.component';
import { RqCreateComponent } from './rqs/rq-create/rq-create.component';
import { RqListComponent } from './rqs/rq-list/rq-list.component';
import { RqService } from './rqs/rq.service';
import { QuestionsComponent } from './rqs/questions/questions.component';
import { RspsComponent } from './rsps/rsps.component';
import { RspService } from './rsps/rsp-service.service';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    UsrListComponent,
    UsrDetailComponent,
    UsrEditComponent,
    CoursesComponent,
    CourseDetailComponent,
    CourseCreateComponent,
    GrouppCreateComponent,
    GrouppsComponent,
    DropdownDirective,
    EnumToArrayPipe,
    CourseLessonsComponent,
    RqCreateComponent,
    RqListComponent,
    QuestionsComponent,
    RspsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    UsrService,
    CourseService,
    GrouppService,
    RqService,
    RspService,
    AuthService,
     {
         provide: HTTP_INTERCEPTORS,
         useClass: AuthInterceptor,
         multi: true
     }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
