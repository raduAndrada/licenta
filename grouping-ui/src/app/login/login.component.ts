import { Component, OnInit } from '@angular/core';
import { Usr } from '../shared/usr.model';
import { Router } from '@angular/router';
import { UsrService } from '../usrs/usr.service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUsr: Usr;
  login( usrnm: string, psswd: string ): void {
      this.usrService.loginUsr( usrnm, psswd )
          .subscribe( loginUsr => {this.loginUsr = loginUsr;
           if (loginUsr != null) {
               this.authService.setUsrLoggedIn(loginUsr);
               localStorage.setItem('isLoggedIn', 'true');
               localStorage.setItem('username', usrnm);
               localStorage.setItem('role', loginUsr.usrTp);
               this.router.navigate(['/users']);
              }
          });

  }


  constructor( private usrService: UsrService, private router: Router , private authService: AuthService) {
  }

  ngOnInit() {
  }

}
