import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { GrouppService } from '../groupp-service.service';
import { Rq} from '../../shared/rq/rq.model';
import { GrouppConstraints } from '../../shared/groupp/groupp-costraints.model';
import { GrouppTp } from '../../shared/groupp/groupp.model';
import { GrouppGet } from '../../shared/groupp/groupp-get.model';


@Component({
  selector: 'app-groupp-create',
  templateUrl: './groupp-create.component.html',
  styleUrls: ['./groupp-create.component.css']
})
export class GrouppCreateComponent implements OnInit {


  @ViewChild('f') grouppForm: NgForm;
  courseNm: string;
  message: string;
  grouppTp = GrouppTp;
  selectedTp: GrouppTp;

  constructor( private route: ActivatedRoute,
               private grouppService: GrouppService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.courseNm = params['courseNm'];
    });
  }

  onSubmit(form: NgForm) {
    const constaints = new GrouppConstraints(this.courseNm, this.selectedTp, form.value.size, form.value.mandate, 'abc', true);

    this.grouppService.getGrouppingForSemesterProjects(constaints)
      .subscribe((grouppGet: GrouppGet) => {
          if (grouppGet != null) {
              this.message = 'Group for semestrial worked';
              this.grouppService.setGroupps(grouppGet.list);
              alert(this.message);
              } else {
              this.message = 'Group for semestrial failed';
              alert(this.message);
          }
      });
    this.grouppForm.reset();
  }

  onSelectTp(event: any) {
    this.selectedTp = event ;
  }

  onClear() {
    this.grouppForm.reset();
  }

}
