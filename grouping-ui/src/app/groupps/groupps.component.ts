import { Component, OnInit } from '@angular/core';
import { GrouppService } from './groupp-service.service';

import { ActivatedRoute, Params } from '@angular/router';
import { Groupp, GrouppTp } from '../shared/groupp/groupp.model';
import { GrouppGet } from '../shared/groupp/groupp-get.model';



@Component({
  selector: 'app-groupps',
  templateUrl: './groupps.component.html',
  styleUrls: ['./groupps.component.css']
})
export class GrouppsComponent implements OnInit {


  groups: Groupp[];
  courseNm: string;
  message: string;

  grouppTp = GrouppTp;

  constructor(private grouppService: GrouppService,
              private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.courseNm = params['courseNm'];
      this.grouppService.setShouldReload(true);
    });
     this.grouppService.getGroupps(this.courseNm).subscribe((grouppGet: GrouppGet) => {
      if (grouppGet != null) {
          this.message = 'Group for semestrial worked';
          this.grouppService.setGroupps(grouppGet.list);
          } else {
          this.message = 'Group for semestrial failed';
          // alert(this.message);
      }
      });
    this.grouppService.grouppsChanged.subscribe((groupList: Groupp[]) => {
      this.groups = groupList;
    });

    this.grouppService.shouldReloadChange.subscribe(
      (reload: boolean) => {
        if (reload) {
          this.grouppService.getGroupps(this.courseNm).subscribe((grouppGet: GrouppGet) => {
            if (grouppGet != null) {
                this.message = 'Group for semestrial worked';
                this.grouppService.setGroupps(grouppGet.list);
                } else {
                this.message = 'Group for semestrial failed';
                // alert(this.message);
            }
            });
        }
      }

    );

  }

}
