import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Rq } from '../shared/rq/rq.model';
import { Groupp } from '../shared/groupp/groupp.model';
import { GrouppConstraints } from '../shared/groupp/groupp-costraints.model';
import { GrouppGet } from '../shared/groupp/groupp-get.model';


@Injectable()
export class GrouppService {

    groupps: Groupp [];
    grouppsChanged = new Subject<Groupp[]> ();
    shouldReload: boolean;
    shouldReloadChange = new Subject<boolean>();

    private proffessorsUrl = 'http://localhost:8000/v1/proffessors';

    private handleError<T> (operation = 'operation', result?: T) {
     return (error: any): Observable<T> => {
       console.error(error); // log to console instead
       return of(result as T);
     };
   }

   getGrouppingForSemesterProjects(constraints: GrouppConstraints): Observable<GrouppGet> {
       return this.http.post<GrouppGet>( this.proffessorsUrl + '/semester-projects', constraints)
       .pipe(
               tap(_ => console.log(_)),
               catchError(this.handleError<GrouppGet>('getSemestrailProjectsGroupping'))
             );
   }

   getShouldReload() {
     return this.shouldReload;
   }

   setGroupps(groupps: Groupp[]) {
       this.groupps = groupps;
       this.grouppsChanged.next(this.groupps.slice());
   }

   setShouldReload(reload: boolean) {
     this.shouldReload = reload;
     this.shouldReloadChange.next(this.shouldReload);
   }


    getGroupps(courseNm: string): Observable<GrouppGet> {
        return this.http.get<GrouppGet>( this.proffessorsUrl + '/' + courseNm + '/semester-projects')
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<GrouppGet>('getGroupps for course'))
              );
    }


   constructor( private http: HttpClient) {
   }
}
