import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RqGet } from '../shared/rq/rq-get.model';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { RqCreateResult } from '../shared/rq/rq-create-result.model';
import { Rq, RqRecord } from '../shared/rq/rq.model';
import { Subject } from 'rxjs/Subject';
import { QuestionRecord } from '../shared/rq/question-record.model';
import { Question } from '../shared/rq/question.model';
import { RqGrouppGetResult } from '../shared/rq/rq-groupp-get-result.model';
import { RqDetail } from '../shared/rq/rq-detail.model';

@Injectable()
export class RqService {

    private proffessorsUrl = 'http://localhost:8000/v1/proffessors';

    rqListChange = new Subject<boolean>();
    shouldChange = false;

    rqRecordSelected = new RqRecord();
    rqRecordSelectedChange = new Subject<RqRecord>();

    constructor( private http: HttpClient) {
    }

    addRq(rqDetail: RqDetail): Observable<RqCreateResult> {
      const url = this.proffessorsUrl + '/lessons';
      return this.http.post<RqCreateResult>(url, rqDetail)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<RqCreateResult>('createRq'))
            );
    }

    addQuestion(questioRecord: QuestionRecord) {
      const url = this.proffessorsUrl + '/lessons/questions';
      return this.http.post<RqRecord>(url, questioRecord)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<RqRecord>('addQuestion'))
            );
    }

    deleteRq(courseNm: string, rqId: number) {
      return this.http.delete<Rq>( this.proffessorsUrl + '/lessons/requests/' + rqId)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<Rq>('deleteRq'))
            );
    }


    getRqList(courseNm: string, lessonId: number): Observable<RqGet> {
        return this.http.get<RqGet>( this.proffessorsUrl +  '/lessons' + '/' + lessonId)
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<RqGet>('getRqList'))
              );
    }

    getRqGrouppList(courseNm: string): Observable<RqGrouppGetResult> {
      return this.http.get<RqGrouppGetResult>( this.proffessorsUrl +  '/lessons/requests/request-groups/history/' + courseNm)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<RqGrouppGetResult>('getRqGrouppList'))
            );
  }

    getRqRecordSelected(): RqRecord {
      return this.rqRecordSelected;
    }

    setRqRecordSelected(rqRecord: RqRecord): void {
      this.rqRecordSelected = rqRecord;
      this.rqRecordSelectedChange.next(this.rqRecordSelected);
    }

    setShouldChange(shouldChange: boolean) {
      this.shouldChange = shouldChange;
      this.rqListChange.next(true);
    }


    updateRqQuestionList(rqRecord: RqRecord) {
      const url = this.proffessorsUrl + '/lessons/questions';
      return this.http.put<RqRecord>(url, rqRecord)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<RqRecord>('updateRequestQuestionList'))
            );
    }

    updateRq(rq: Rq) {
      const url = this.proffessorsUrl + '/lessons/requests';
      return this.http.put<Rq>(url, rq)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<RqRecord>('updateRequest'))
            );
    }



    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error); // log to console instead
          return of(result as T);
        };
      }
}
