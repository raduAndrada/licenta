import { Component, OnInit } from '@angular/core';
import { RqService } from '../rq.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RqGet } from '../../shared/rq/rq-get.model';
import { TimeMeasurementTp } from '../../shared/rq/time-measurement-tp.model';
import { RqTp } from '../../shared/rq/rq-tp.model';
import { RqRecord, Rq, RqStatus } from '../../shared/rq/rq.model';

@Component({
  selector: 'app-rq-list',
  templateUrl: './rq-list.component.html',
  styleUrls: ['./rq-list.component.css']
})
export class RqListComponent implements OnInit {

  rqRecordList = [];
  lessonId:  number;
  courseNm: string;
  weekNb: number;
  timeMeasurementTp: TimeMeasurementTp;
  rqTp: RqTp;
  rqStatus = RqStatus;
  timeCountDown = 0;

  constructor(  private router: Router,
                private route: ActivatedRoute,
                private rqService: RqService
  ) { }

  ngOnInit() {
    this.courseNm = this.route.snapshot.params['courseNm'];
    this.lessonId = this.route.snapshot.params['lessonId'];
    this.route.params
    .subscribe(params => {
      this.courseNm = params['courseNm'];
      this.lessonId = params['lessonId'];
    });

    this.rqService.getRqList(this.courseNm, this.lessonId)
    .subscribe(
      (rqGet: RqGet ) => {
        this.rqRecordList = rqGet.list;
        this.weekNb = rqGet.weekNb;
      });
    this.rqService.rqListChange.subscribe(
      (rqChanged: boolean) => {
        if (rqChanged) {
          this.rqService.getRqList(this.courseNm, this.lessonId)
          .subscribe(
            (rqGet: RqGet ) => {
              this.rqRecordList = rqGet.list;
            });
        }
      }
    );
  }


  onAddRq() {
    this.router.navigate(['create'], {relativeTo: this.route, queryParams: {lessonId: this.lessonId}});
  }

  onClickQuestions(rqRecord: RqRecord, rqCount: number) {
    this.rqService.setRqRecordSelected(rqRecord);
    this.router.navigate(['questions'], {relativeTo: this.route, queryParams:
                          {lessonId: this.lessonId, rqId: rqRecord.rq.id, rqTp: rqRecord.rq.rqTp, rqCount: rqCount}});
  }

  onLaunchRq (rq: Rq, nbOfQuestions: number) {
    const rqForLaunch = new Rq();

    rqForLaunch.rqStatus = rq.rqStatus.toString() === 'PENDING' ? RqStatus.OPENED : RqStatus.ONGOING;

    if (rq.rqStatus.toString() === 'PENDING') {
      this.computeRqTime(rq, nbOfQuestions);
      this.refreshList();
    }
    if (rq.rqStatus.toString() === 'TERMINATED') {
      this.router.navigate(['responses', rq.id]);
    }
    rqForLaunch.id = rq.id;
    if (rq.rqStatus.toString() !== 'TERMINATED') {
      this.rqService.updateRq(rqForLaunch).subscribe(
        (rqRecord: RqRecord ) => {
          if (rqRecord) {
            this.rqService.setShouldChange(true);
          }
        });
    }
  }

  onDeleteRq(rqId: number) {
    this.rqService.deleteRq(this.courseNm, rqId).subscribe(
      (rq: Rq ) => {
        if (rq) {
          this.rqService.setShouldChange(true);
        }
      });
  }

  private refreshList() {
    setTimeout(() => {
      this.rqService.setShouldChange(true);
    }, this.timeCountDown + 10000);
  }

  private computeRqTime(rq: Rq, nbOfQuestions: number) {
     this.timeCountDown = rq.timeMeasurementTp.toString() === 'ON_QUESTION'
    ? nbOfQuestions * rq.allowedTm
    : rq.allowedTm;
    // we wait twice the period for this in order to leave the students to vote
    this.timeCountDown *= 2000;
  }
}

