import { Component, OnInit } from '@angular/core';
import { Rq, RqStatus } from '../../shared/rq/rq.model';
import { TimeMeasurementTp } from '../../shared/rq/time-measurement-tp.model';
import { RqTp, RqGrouppTp } from '../../shared/rq/rq-tp.model';
import { RqService } from '../rq.service';
import { ActivatedRoute } from '@angular/router';
import { RqCreateResult } from '../../shared/rq/rq-create-result.model';
import { RqGrouppGetResult } from '../../shared/rq/rq-groupp-get-result.model';
import { RqGroupp } from '../../shared/rq/rq-groupp.model';
import { RqDetail } from '../../shared/rq/rq-detail.model';

@Component({
  selector: 'app-rq-create',
  templateUrl: './rq-create.component.html',
  styleUrls: ['./rq-create.component.css']
})
export class RqCreateComponent implements OnInit {

  rq = new Rq();
  timeMeasurementTp = TimeMeasurementTp;
  rqTp = RqTp;
  lessonId: number;
  rqGrouppTp = RqGrouppTp;

  rqStatus = RqStatus;

  courseNm = '';
  historyOrNew: string;
  historyList: RqGrouppGetResult;
  selectedRqGroupp: RqGroupp;


  constructor( private rqService: RqService,
               private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.lessonId = this.route.snapshot.queryParams['lessonId'];
    this.courseNm = this.route.snapshot.queryParams['courseNm'];
    this.route.queryParams
    .subscribe(params => {
      this.lessonId = params['lessonId'];
      this.courseNm = params['courseNm'];
    });
    this.rqService.getRqGrouppList(this.courseNm)
    .subscribe(
      (rqGet: RqGrouppGetResult ) => {
        this.historyList = rqGet;
      });
  }


  addRq() {
    this.rq.lessonId = this.lessonId;
    const rqDetail = new RqDetail(this.rq, this.historyOrNew === 'history' ? this.selectedRqGroupp : null);
    this.rqService.addRq(rqDetail)
    .subscribe(
      (rqCreateResult: RqCreateResult ) => {
        if (rqCreateResult) {
          this.rqService.setShouldChange(true);
        }
      });
  }
}
