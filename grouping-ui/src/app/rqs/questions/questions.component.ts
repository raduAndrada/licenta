import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RqService } from '../rq.service';
import { Question } from '../../shared/rq/question.model';
import { QuestionRecord } from '../../shared/rq/question-record.model';
import { QuestionAnswer } from '../../shared/rq/question-answer.model';
import { RqRecord, Rq } from '../../shared/rq/rq.model';
import { RqTp } from '../../shared/rq/rq-tp.model';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  lessonId: number;
  rqTp: string;
  rqId: number;
  rqCount: number;
  questionRecord =  null;
  selectedRqRecord = new RqRecord();

  constructor( private route: ActivatedRoute,
              private router: Router,
              private rqService: RqService
  ) { }



  ngOnInit() {
    this.lessonId = this.route.snapshot.queryParams['lessonId'];
    this.rqId = this.route.snapshot.queryParams['rqId'];
    this.rqTp = this.route.snapshot.queryParams['rqTp'];
    this.rqCount = this.route.snapshot.queryParams['rqCount'];


    this.route.queryParams
    .subscribe(params => {
      this.lessonId = params['lessonId'];
      this.rqId = params['rqId'];
      this.rqTp = params['rqTp'];
      this.rqCount = +params['rqCount'];
    });

    this.selectedRqRecord = this.rqService.getRqRecordSelected();
    this.rqService.rqRecordSelectedChange.subscribe(
      (rqRecord: RqRecord) => {
      this.selectedRqRecord = rqRecord;
      });

      this.questionRecord = this.selectedRqRecord.rq.rqTp === RqTp.CHOICE ? new QuestionRecord(new Question(), [new QuestionAnswer(false)])
                                                                            : new QuestionRecord(new Question());
  }

  onAddQuestion() {
    this.questionRecord.question.rqId = this.rqId;
    this.rqService.addQuestion(this.questionRecord)
    .subscribe(
      (rqRecord: RqRecord ) => {
        if (rqRecord) {
          this.rqService.setRqRecordSelected(rqRecord);
          this.questionRecord = rqRecord.rq.rqTp === RqTp.CHOICE ?
                                                  new QuestionRecord(new Question(), [new QuestionAnswer(false)])
                                                                             : new QuestionRecord(new Question());
        }
      });
  }

  onAddAnswer() {
    if (this.questionRecord.questionAnswerList === undefined) {
      this.questionRecord.questionAnswerList = [];
    }

    if (this.questionRecord.questionAnswerList.length <= 5) {
    this.questionRecord.questionAnswerList.push(new QuestionAnswer(false));
    } else {
      alert('Maximum number of answers for this question has been reached!');
    }
  }

  onAddAnswerForExistingQuestion(questionIndex: number) {
    if (this.selectedRqRecord.questions[questionIndex].questionAnswerList.length <= 5) {
    this.selectedRqRecord.questions[questionIndex].questionAnswerList.push(new QuestionAnswer(false));
    } else {
      alert('Maximum number of answers for this question has been reached!');
    }
  }

  onDeleteAnswer(questionIndex: number, questionsAnswerIndex: number) {
    // TODO if a choice question has no answers -> delete the question !!!! ????
    this.selectedRqRecord.questions[questionIndex].questionAnswerList.splice(questionsAnswerIndex, 1);

  }
  onDeleteQuestion(questionIndex: number) {
    const temp = this.selectedRqRecord.questions;
    temp.splice(questionIndex, 1);
    this.selectedRqRecord.questions = temp;
  }

  onSaveChanges() {
    this.rqService.updateRqQuestionList(this.selectedRqRecord)
    .subscribe(
      (rqRecord: RqRecord ) => {
        if (rqRecord) {
          // this.rqService.setRqRecordSelected(rqRecord);
          this.questionRecord = new QuestionRecord(new Question(), [new QuestionAnswer(false)]);
        }
      });
  }


}
