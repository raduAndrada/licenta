import { TimeMeasurementTp } from './time-measurement-tp.model';
import { RqTp } from './rq-tp.model';

export class RqCreateResult {
    constructor (
        public allowedTm?: number,
        public maxRsp?: number,
        public rqTp?: RqTp,
        public timeMeasurementTp?: TimeMeasurementTp
    ) {}
}
