export class Question {
    constructor (
        public id?: number,
        public rqId?: number,
        public questionText?: string
    ) {}
}
