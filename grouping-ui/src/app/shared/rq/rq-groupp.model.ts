import { RqStatus } from './rq.model';

export class RqGroupp {
    constructor (
        public rqId?: number,
        public rqStatus?: RqStatus,
        public crtTm?: Date
    ) {}
}
