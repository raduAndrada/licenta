export class QuestionAnswer {
    constructor (
        public correct?: boolean,
        public id?: number,
        public questionId?: number,
        public answerText?: string
    ) {}
}
