export enum RqTp {
    CHOICE = 'choice',
    TEXT_FILLING= 'text filling'
}

export enum RqGrouppTp {
    RANDOM_GROUP = 'random group',
    CHOOSE_GROUP = 'choose your group',
    COMBINED_GROUP = 'combined'
}
