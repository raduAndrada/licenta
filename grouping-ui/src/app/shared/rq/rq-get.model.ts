import { Rq, RqRecord } from './rq.model';

export class RqGet {
    constructor (
        public list: RqRecord[],
        public weekNb: number,
    ) {}
}
