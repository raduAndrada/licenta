import { RqTp, RqGrouppTp } from './rq-tp.model';
import { TimeMeasurementTp } from './time-measurement-tp.model';
import { QuestionRecord } from './question-record.model';

export class Rq {
    constructor (
        public id?: number,
        public lessonId?: number,
        public allowedTm?: number,
        public maxRsp?: number,
        public rqTp?: RqTp,
        public timeMeasurementTp?: TimeMeasurementTp,
        public rqStatus?: RqStatus,
        public rqGrouppTp?: RqGrouppTp,
        public rqGrouppSz?: number
    ) {}
}

export class RqRecord {
    constructor (
        public rq?: Rq,
        public questions?: QuestionRecord[]
    ) {}
}

export enum RqStatus {
    OPENED,
    ONGOING,
    PENDING,
    TERMINATED
}
