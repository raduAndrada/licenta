import { Rq } from './rq.model';
import { RqGroupp } from './rq-groupp.model';

export class RqDetail {
    constructor(
                public rq?: Rq,
                public rqGroupp?: RqGroupp
    ) {}
}
