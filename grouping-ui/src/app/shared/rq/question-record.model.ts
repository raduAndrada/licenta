import { Question } from './question.model';
import { QuestionAnswer } from './question-answer.model';

export class QuestionRecord {
    constructor (
        public question?: Question,
        public questionAnswerList?: QuestionAnswer[]
    ) {}
}
