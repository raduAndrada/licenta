import { Directive, Input, OnInit, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive ({
    selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {


    @HostBinding ('class.open') isOpen = false;

    @HostListener('click') toggleclick (eventData: Event) {
        this.isOpen = ! this.isOpen;

    }

    constructor() { }

    ngOnInit ( ) {

    }



}
