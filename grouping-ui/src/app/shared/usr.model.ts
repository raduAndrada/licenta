export class Usr {

    constructor(public usrnm: string, 
                public fnm: string, 
                public lnm: string,
                public email: string,
                public usrTp: string,
                public psswd: string
            ){}
    
}
