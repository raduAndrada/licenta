import { GrouppTp } from './groupp.model';

export class GrouppConstraints {
    constructor (
        public courseNm?: string,
        public grouppTp?: GrouppTp,
        public grouppSize?: number,
        public mandate?: number,
        public projNm?: string,
        public defaultt?: Boolean
    ) {}
}
