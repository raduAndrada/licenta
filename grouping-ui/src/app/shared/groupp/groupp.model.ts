export class Groupp {

    constructor (
        public size?: number,
        public projNm?: string,
        public leaderUsrnm?: string,
        public mandate?: number,
        public grouppTp?: GrouppTp
    ) {}
}


export enum GrouppTp {
    SEM_PROJ= 'Semester project',
    SEM_PROJ_CHOOSE= 'Semester project by choice'
}
