// tslint:disable-next-line:max-line-length
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {


    constructor(private authService: AuthService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler)
        : Observable<any> {
              const loggedIn = this.authService.getUsrLoggedIn();
              const copiedReq = req.clone(
                   {

                       headers: req.headers
                                            .append('X-Authorization',  loggedIn !== undefined && loggedIn !== null ?
                                                                                loggedIn.usrnm : 'Standard-Bearer' )
                                            .append('Access-Control-Allow-Origin', '*')
                                            .append('Access-Control-Allow-Headers',
                                                        'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With')
                                            .append('Access-Control-Allow-Methods', 'GET, PUT, POST')
                                            .append('Content-Type', 'application/json')}
                );
              return(next.handle(copiedReq));
    }
}
