import { Usr } from "./usr.model";

export class AuthService {

    private usrLoggedIn: Usr = null;
    authenticated = false;

    setUsrLoggedIn(usrLoggedIn){
        this.usrLoggedIn = usrLoggedIn;
        this.authenticated = true;
    }

    getUsrLoggedIn(){
        return this.usrLoggedIn;
    }

    isAuthenticated(){
        return this.authenticated;
    }
    

}