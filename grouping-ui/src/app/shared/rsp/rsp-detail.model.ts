import { RspRecord } from './rsp-record.model';

export class RspDetail {
    constructor (
        public studentUsrnmsList?: string[],
        public rspRecord?: RspRecord
    ) {}
}
