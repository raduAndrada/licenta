export class Rsp {
    constructor (
        public id?: number,
        public rqGrouppId?: number,
        public questionId?: number,
        public studentUsrnm?: string,
        public rspText?: string,
        public voteCount?: number
    ) {}
}
