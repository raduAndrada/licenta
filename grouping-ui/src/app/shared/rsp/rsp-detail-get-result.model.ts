import { RspDetail } from './rsp-detail.model';

export class RspDetailGetResult {
    constructor (
        public list?: RspDetail[]
    ) {}
}
