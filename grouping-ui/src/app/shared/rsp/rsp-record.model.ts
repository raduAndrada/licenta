import { Rsp } from './rsp.model';

export class RspRecord {
    constructor (
        public questionText?: string,
        public list?: Rsp[]
    ) {}
}
