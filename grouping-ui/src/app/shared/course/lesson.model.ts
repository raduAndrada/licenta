export class Lesson {

    constructor(public id?: number,
                public weekNb?: number,
                public theme?: string,
                public courseNm?: string
    ) {}

}
