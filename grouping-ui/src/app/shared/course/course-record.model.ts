import { Course } from './course.model';
import { Lesson } from './lesson.model';

export class CourseRecord {

    constructor(public lessonsList?: Lesson[],
                public course?: Course,
                public crtTm?: string
    ) {}

}
