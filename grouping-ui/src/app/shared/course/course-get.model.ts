import { CourseRecord } from './course-record.model';

export class CourseGet {
    list: Array<CourseRecord>;
}
