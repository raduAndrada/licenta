export class Course {

    constructor(public nm?: string,
                public studentsCount?: number,
                public dsc?: string,
                public stDt?: string,
                public endDt?: string
    ) {}

}
