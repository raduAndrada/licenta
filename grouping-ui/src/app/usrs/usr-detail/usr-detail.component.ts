import { Component, OnInit } from '@angular/core';
import { UsrService } from '../usr.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Usr } from '../../shared/usr.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-usr-detail',
  templateUrl: './usr-detail.component.html',
  styleUrls: ['./usr-detail.component.css']
})
export class UsrDetailComponent implements OnInit {

  message: string ;
  addedUsr: Usr;
  usrToAdd: Usr = new Usr('', '', '', '', '', '');
  types: string[] = ["STUDENT",'PROFFESSOR'];
  addUsr(){
      this.usrService.addUsr(this.usrToAdd)
      .subscribe(addedUsr => {
          this.addedUsr = addedUsr;
          if(addedUsr != null) {
              this.message = 'User with '+ this.usrToAdd.usrnm + ' has been successfully created';
              alert(this.message);
              this.router.navigate([ '/users', this.usrToAdd.usrnm, '/edit']);
              }
          else{
              this.message ='Registration failed, email address or username already taken';
              alert(this.message);
          }
      });
  }

  
  goBack(): void {
      this.location.back();
    }
  
  constructor(private usrService: UsrService, private location :Location , private router: Router, private route: ActivatedRoute ) { }

  ngOnInit() {
  }
}
