import { Component, OnInit } from '@angular/core';
import { Usr } from '../../shared/usr.model';
import { UsrService } from '../usr.service';

@Component({
  selector: 'app-usr-list',
  templateUrl: './usr-list.component.html',
  styleUrls: ['./usr-list.component.css']
})
export class UsrListComponent implements OnInit {

  usrs:  Usr[];

  constructor( private usrService: UsrService ) { }

  ngOnInit() {
      this.getUsrs();
  }

  getUsrs(): void {
    this.usrService.getUsrs()
   .subscribe(usrGet => this.usrs = usrGet.list);
 }

}
