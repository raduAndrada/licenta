import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders, HttpResponse , HttpRequest} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Usr } from '../shared/usr.model';
import { UsrGet } from '../shared/usr-get.model';
import { LoginUsr } from '../shared/login-usr.model';


@Injectable()
export class UsrService {

    private usrsUrl = 'http://localhost:8000/v1/usrs';
    isAuthenticated = false;

    constructor( private http: HttpClient) {
    }

    getUsrs(): Observable<UsrGet> {
        return this.http.get<UsrGet>( this.usrsUrl)
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<UsrGet>('getUsr'))
              );
    }
    getUserByUsrnm(usrnm: string): Observable<Usr> {
        const url = this.usrsUrl + '/' + usrnm;
        return this.http.get<Usr>(url)
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<Usr>('getUsr'))
              );
    }

    loginUsr(usrnm: string, psswd: string): Observable<Usr> {
        const url = this.usrsUrl + '/login';

        const usr = new LoginUsr(usrnm, psswd);
        return this.http.post<Usr>(url, usr);
    }

    addUsr(usr: Usr): Observable<Usr> {
        const url = this.usrsUrl;
        return this.http.post<Usr>(url + '/create', usr)
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<Usr>('addUsr'))
              );
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error); // log to console instead
          return of(result as T);
        };
      }

}

