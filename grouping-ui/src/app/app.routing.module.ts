import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UsrListComponent } from './usrs/usr-list/usr-list.component';
import { UsrDetailComponent } from './usrs/usr-detail/usr-detail.component';
import { UsrEditComponent } from './usrs/usr-edit/usr-edit.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailComponent } from './courses/course-detail/course-detail.component';
import { CourseCreateComponent } from './courses/course-create/course-create.component';
import { AuthGuard } from './login/auth.guard';
import { GrouppCreateComponent } from './groupps/groupp-create/groupp-create.component';
import { GrouppsComponent } from './groupps/groupps.component';
import { CourseLessonsComponent } from './courses/course-lessons/course-lessons.component';
import { RqListComponent } from './rqs/rq-list/rq-list.component';
import { RqCreateComponent } from './rqs/rq-create/rq-create.component';
import { QuestionsComponent } from './rqs/questions/questions.component';
import { RspsComponent } from './rsps/rsps.component';



const appRoutes: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'register', component: UsrDetailComponent },
    { path: 'users', component: UsrListComponent, canActivate: [AuthGuard],
      children : [
        { path: ':usrnm/edit', component: UsrEditComponent },
    ]},
    { path: 'responses/:rqId', component: RspsComponent, canActivate: [AuthGuard]},
    { path: 'requests/:courseNm/:lessonId', component: RqListComponent, canActivate: [AuthGuard],
       children : [
        { path: 'create', component: RqCreateComponent },
        { path: 'questions', component: QuestionsComponent },
    ]},
    { path: 'courses/:usrnm', component: CoursesComponent,
       children: [
        { path: 'create', component: CourseCreateComponent },
        { path: ':courseNm/lessons', component: CourseLessonsComponent },
        { path: ':courseNm/group-list', component: GrouppsComponent },
        { path: ':courseNm/group', component: GrouppCreateComponent },
        { path: ':courseNm', component: CourseDetailComponent }
        ],
        canActivate: [AuthGuard]
    },
    { path: '', redirectTo: 'login', pathMatch: 'full'}
  ];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes/*, {useHash: true}*/)
    ],
    providers: [
        AuthGuard
    ],
    exports: [ RouterModule]
})
export class AppRoutingModule {

}
