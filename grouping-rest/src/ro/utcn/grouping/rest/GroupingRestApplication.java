package ro.utcn.grouping.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupingRestApplication {

	public static void main(final String[] args) {
		SpringApplication.run(GroupingRestApplication.class, args);
	}
}
