package ro.utcn.grouping.rest.usr;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.utcn.grouping.business.api.usr.UsrBusiness;
import ro.utcn.grouping.model.base.BusinessException;
import ro.utcn.grouping.model.course.CourseGetResult;
import ro.utcn.grouping.model.course.CourseRecord;
import ro.utcn.grouping.model.usr.ImtUsrGet;
import ro.utcn.grouping.model.usr.LoginUsr;
import ro.utcn.grouping.model.usr.Usr;
import ro.utcn.grouping.model.usr.UsrCreateResult;
import ro.utcn.grouping.model.usr.UsrGetResult;

@RestController
@RequestMapping(value = "/v1/usrs")
@CrossOrigin(maxAge = 3600)
public class UsrRest {

	private UsrBusiness usrBusiness;

	@Inject
	public void setUsrBusiness(UsrBusiness usrBusiness) {
		this.usrBusiness = usrBusiness;
	}

	@RequestMapping(path = "/{usrnm}", method = RequestMethod.GET)
	public ResponseEntity<Usr> getUsr(@PathVariable final String usrnm) {
		final Usr userGetResult = usrBusiness.getUsr(usrnm);
		if (userGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(userGetResult);
		}
	}

	@RequestMapping(path = "/login", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Usr> postLogin(@RequestBody final LoginUsr usr) {
		final Usr loginUsr;
		try {
			loginUsr = usrBusiness.loginUsr(usr);
		} catch (final BusinessException e) {
			return null;
		}
		return ResponseEntity.ok(loginUsr);
	}

	@RequestMapping(path = "/logout", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Usr> postLogout(@RequestBody final LoginUsr usr) {
		Usr logoutUsr;
		try {
			logoutUsr = usrBusiness.logoutUsr(usr.getUsrnm());
		} catch (final BusinessException e) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(logoutUsr);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<UsrGetResult> getUsrs() {
		final UsrGetResult usrGetResult = usrBusiness.getUsrs(ImtUsrGet.builder().build());
		if (usrGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(usrGetResult);
		}
	}

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<UsrCreateResult> postCreate(@RequestBody final Usr usr) {
		final UsrCreateResult createdUsr;
		try {
			createdUsr = usrBusiness.createUsr(usr);
		} catch (final BusinessException e) {
			return null;
		}
		return ResponseEntity.ok(createdUsr);
	}

	@RequestMapping(path = "/courses/{usrnm}", method = RequestMethod.GET)
	public ResponseEntity<CourseGetResult> getCoursesForUsr(@PathVariable final String usrnm) {
		final CourseGetResult courseGetResult = usrBusiness.getCoursesForUsr(usrnm);
		if (courseGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(courseGetResult);
		}
	}

	@RequestMapping(path = "/courses/{usrnm}/{courseNm}", method = RequestMethod.GET)
	public ResponseEntity<CourseRecord> getCourseForUsr(@PathVariable final String usrnm,
			@PathVariable final String courseNm) {
		final CourseRecord course = usrBusiness.getCourseForUsrByCourseNm(usrnm, courseNm);
		if (course == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(course);
		}
	}

}
