package ro.utcn.grouping.rest.proffessor;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.utcn.grouping.business.api.proffessor.ProffessorBusiness;
import ro.utcn.grouping.model.base.BusinessException;
import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseCreateResult;
import ro.utcn.grouping.model.groupp.GrouppConstraints;
import ro.utcn.grouping.model.groupp.GrouppGetResult;
import ro.utcn.grouping.model.question.QuestionRecord;
import ro.utcn.grouping.model.rq.ImtRqGet;
import ro.utcn.grouping.model.rq.Rq;
import ro.utcn.grouping.model.rq.RqCreateResult;
import ro.utcn.grouping.model.rq.RqDetail;
import ro.utcn.grouping.model.rq.RqGetResult;
import ro.utcn.grouping.model.rq.RqGrouppGetResult;
import ro.utcn.grouping.model.rq.RqRecord;
import ro.utcn.grouping.model.rsp.RspDetailGetResult;

@RestController
@RequestMapping(value = "/v1/proffessors")
@CrossOrigin(maxAge = 3600)
public class ProffessorRest {

	private ProffessorBusiness proffessorBusiness;

	@Inject
	public void setProffessorBusiness(ProffessorBusiness proffessorBusiness) {
		this.proffessorBusiness = proffessorBusiness;
	}

	@RequestMapping(path = "/courses", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<CourseCreateResult> postCreateCourse(@RequestBody final Course course) {
		try {
			final CourseCreateResult createdCourse = proffessorBusiness.createCourse(course);
			return ResponseEntity.ok(createdCourse);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "{usrnm}/courses", method = RequestMethod.PUT)
	@Transactional
	public ResponseEntity<Course> postUpdateCourse(@RequestBody final Course course) {
		try {
			final Course updatedCourse = proffessorBusiness.updateCourse(course);
			return ResponseEntity.ok(updatedCourse);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "/courses/{nm}", method = RequestMethod.DELETE)
	@Transactional
	public ResponseEntity<Course> postDeleteCourse(@PathVariable("nm") String nm) {
		try {
			final Course deletedCourse = proffessorBusiness.deleteCourse(nm);
			return ResponseEntity.ok(deletedCourse);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "/semester-projects", method = RequestMethod.POST)
	public ResponseEntity<GrouppGetResult> getGrouppingForSemProjs(@RequestBody GrouppConstraints constraints) {
		final GrouppGetResult grouppGetResult = proffessorBusiness.groupForSemProjs(constraints);
		if (grouppGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(grouppGetResult);
		}
	}

	@RequestMapping(path = "{courseNm}/semester-projects", method = RequestMethod.GET)
	public ResponseEntity<GrouppGetResult> getAvailableSemesterProjects(@PathVariable("courseNm") String courseNm) {
		final GrouppGetResult grouppGetResult = proffessorBusiness.getGroupsForCourse(courseNm);
		if (grouppGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(grouppGetResult);
		}
	}

	@RequestMapping(path = "/lessons", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<RqCreateResult> postAddRq(@RequestBody final RqDetail rq) {
		try {
			final RqCreateResult createdRq = proffessorBusiness.addRq(rq);
			return ResponseEntity.ok(createdRq);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "/lessons/{lessonId}", method = RequestMethod.GET)
	public ResponseEntity<RqGetResult> getRqForLesson(@PathVariable("lessonId") Long lessonId) {
		final RqGetResult rqGetResult = proffessorBusiness
				.getRqForLesson(ImtRqGet.builder().lessonId(lessonId).build());
		if (rqGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(rqGetResult);
		}
	}

	@RequestMapping(path = "/lessons/questions", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<RqRecord> postAddQuestion(@RequestBody final QuestionRecord questionRecord) {
		try {
			final RqRecord rqForQuestions = proffessorBusiness.addQuestionToRq(questionRecord);
			return ResponseEntity.ok(rqForQuestions);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "/lessons/questions", method = RequestMethod.PUT)
	@Transactional
	public ResponseEntity<RqRecord> putRqQuestion(@RequestBody final RqRecord rqRecord) {
		try {
			final RqRecord updatedRq = proffessorBusiness.updateQuestionListForRq(rqRecord);
			return ResponseEntity.ok(updatedRq);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "/lessons/requests/{rqId}", method = RequestMethod.DELETE)
	public ResponseEntity<Rq> deleteRqForLesson(@PathVariable("rqId") Long rqId) {
		final Rq rq = proffessorBusiness.deleteRq(rqId);
		if (rq == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(rq);
		}
	}

	@RequestMapping(path = "/lessons/requests", method = RequestMethod.PUT)
	@Transactional
	public ResponseEntity<RqRecord> putRqUpdate(@RequestBody final Rq rq) {
		try {
			final RqRecord updatedRq = proffessorBusiness.updateRq(rq);
			return ResponseEntity.ok(updatedRq);
		} catch (final BusinessException e) {
			return ResponseEntity.badRequest().build();
		}

	}

	@RequestMapping(path = "/lessons/requests/responses/{rqId}", method = RequestMethod.GET)
	public ResponseEntity<RspDetailGetResult> getRspForRq(@PathVariable("rqId") Long rqId) {
		final RspDetailGetResult RspDetailGetResult = proffessorBusiness.getRspsForRq(rqId);
		if (RspDetailGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(RspDetailGetResult);
		}
	}

	@RequestMapping(path = "/lessons/requests/request-groups/history/{corseNm}", method = RequestMethod.GET)
	public ResponseEntity<RqGrouppGetResult> getRqForCourse(@PathVariable("corseNm") String corseNm) {
		final RqGrouppGetResult rqGrouppGetResult = proffessorBusiness.getRqGrouppForCourse(corseNm);
		if (rqGrouppGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(rqGrouppGetResult);
		}
	}

}
