package ro.utcn.grouping.rest.groupp;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.utcn.grouping.business.api.group.GroupBusiness;
import ro.utcn.grouping.model.groupp.Groupp;

@RestController
@RequestMapping(value = "/v1/groups")
public class GroupRest {

	private GroupBusiness groupBusiness;

	@Inject
	public void setGroupBusiness(GroupBusiness groupBusiness) {
		this.groupBusiness = groupBusiness;
	}

	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Groupp> postGroup(@RequestBody final Groupp group) {
		groupBusiness.group(group);
		return ResponseEntity.ok(group);
	}
}
