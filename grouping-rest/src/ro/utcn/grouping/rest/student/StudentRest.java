package ro.utcn.grouping.rest.student;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.utcn.grouping.business.api.student.StudentBusiness;
import ro.utcn.grouping.model.base.BusinessException;
import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseUsr;
import ro.utcn.grouping.model.course.StudentCourseGetResult;
import ro.utcn.grouping.model.groupp.GrouppGetResult;
import ro.utcn.grouping.model.groupp.GrouppStudent;
import ro.utcn.grouping.model.rq.RqGroupp;
import ro.utcn.grouping.model.rq.RqRecord;
import ro.utcn.grouping.model.rsp.RspCreateList;
import ro.utcn.grouping.model.rsp.RspCreateResultList;
import ro.utcn.grouping.model.rsp.RspGetResult;

@RestController
@RequestMapping(value = "/v1/students")
@CrossOrigin(maxAge = 3600)
public class StudentRest {

	private StudentBusiness studentBusiness;

	@Inject
	public void setStudentBusiness(StudentBusiness studentBusiness) {
		this.studentBusiness = studentBusiness;
	}

	@RequestMapping(path = "/response/{rqGrouppId}", method = RequestMethod.GET)
	public ResponseEntity<RspGetResult> getRspListForRqGrupp(@PathVariable("rqGrouppId") Long rqGrouppId) {
		final RspGetResult rspGetResult = studentBusiness.getRspsForRqGroupp(rqGrouppId);
		return ResponseEntity.ok(rspGetResult);
	}

	@RequestMapping(path = "/response", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<RspCreateResultList> postRspCreate(@RequestBody final RspCreateList rspCreateList) {
		final RspCreateResultList rspCreateResultList = studentBusiness.addRsps(rspCreateList);
		return ResponseEntity.ok(rspCreateResultList);
	}

	@RequestMapping(path = "/response/vote", method = RequestMethod.PUT)
	@Transactional
	public ResponseEntity<RspCreateList> putRspVote(@RequestBody final RspCreateList rspCreateList) {
		;
		final RspCreateList rsps = studentBusiness.voteRsp(rspCreateList);
		return ResponseEntity.ok(rsps);
	}

	@RequestMapping(path = "/request-group/{usrnm}", method = RequestMethod.GET)
	public ResponseEntity<RqGroupp> getRqGrouppForStudent(@PathVariable("usrnm") String usrnm) {
		final RqGroupp rqGroupp = studentBusiness.getRqGrouppForStudent(usrnm);
		return ResponseEntity.ok(rqGroupp);
	}

	@RequestMapping(path = "/request/{usrnm}", method = RequestMethod.GET)
	public ResponseEntity<RqRecord> getRqForStudent(@PathVariable("usrnm") String usrnm) {
		final RqRecord rqRecord = studentBusiness.getRqForStudent(usrnm);
		return ResponseEntity.ok(rqRecord);
	}

	@RequestMapping(path = "/courses/{usrnm}", method = RequestMethod.GET)
	public ResponseEntity<StudentCourseGetResult> getAvailableCourses(@PathVariable("usrnm") String usrnm) {
		final StudentCourseGetResult courseGetResult = studentBusiness.getAvailableCourses(usrnm);
		if (courseGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(courseGetResult);
		}
	}

	@RequestMapping(path = "/courses", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Course> postEnroll(@RequestBody final CourseUsr courseUsr) {
		try {
			final Course createdCourseUsr = studentBusiness.enrollStudent(courseUsr.getUsrUsrnm(),
					courseUsr.getCourseNm());
			return ResponseEntity.ok(createdCourseUsr);
		} catch (final BusinessException e) {
			return (ResponseEntity<Course>) ResponseEntity.badRequest();
		}

	}

	@RequestMapping(path = "groups/{courseNm}/semester-projects", method = RequestMethod.GET)
	public ResponseEntity<GrouppGetResult> getAvailableSemesterProjects(@PathVariable("courseNm") String courseNm) {
		final GrouppGetResult grouppGetResult = studentBusiness.getSemesterGrouppsByChoice(courseNm);
		if (grouppGetResult == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(grouppGetResult);
		}
	}

	@RequestMapping(path = "/join-group", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Integer> postJoinGrouppForSemesterProj(@RequestBody final GrouppStudent grouppStudent) {
		try {
			final Integer joined = studentBusiness.joinGroupp(grouppStudent);
			return ResponseEntity.ok(joined);
		} catch (final BusinessException e) {
			return (ResponseEntity<Integer>) ResponseEntity.badRequest();
		}

	}
}
