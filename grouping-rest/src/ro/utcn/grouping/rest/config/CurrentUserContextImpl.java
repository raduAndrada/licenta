package ro.utcn.grouping.rest.config;

import java.util.Optional;

import ro.utcn.grouping.business.base.CurrentUsrContext;

public class CurrentUserContextImpl implements CurrentUsrContext {

	static final ThreadLocal<String> USERNAME = new ThreadLocal<>();

	@Override
	public String getUsername() {
		return Optional.ofNullable(USERNAME.get()).orElseThrow(() -> new IllegalStateException("User not available"));
	}

}
