package ro.utcn.grouping.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ro.utcn.grouping.business.api.group.GroupBusiness;
import ro.utcn.grouping.business.api.proffessor.ProffessorBusiness;
import ro.utcn.grouping.business.api.student.StudentBusiness;
import ro.utcn.grouping.business.api.usr.UsrBusiness;
import ro.utcn.grouping.business.impl.groupp.GroupBusinessImpl;
import ro.utcn.grouping.business.impl.proffessor.ProffessorBusinessImpl;
import ro.utcn.grouping.business.impl.student.StudentBusinessImpl;
import ro.utcn.grouping.business.impl.usr.UsrBusinessImpl;

/**
 * @author Martin
 *
 */
@Configuration
public class BusinessConfiguration {

	@Bean
	public UsrBusiness usrBusiness() {
		return new UsrBusinessImpl();
	}

	@Bean
	public StudentBusiness studentBusiness() {
		return new StudentBusinessImpl();
	}

	@Bean
	public ProffessorBusiness proffessorBusiness() {
		return new ProffessorBusinessImpl();
	}

	@Bean
	public GroupBusiness GroupBusiness() {
		return new GroupBusinessImpl();
	}

}
