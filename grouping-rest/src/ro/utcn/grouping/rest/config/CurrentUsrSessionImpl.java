package ro.utcn.grouping.rest.config;

import java.util.Optional;

import ro.utcn.grouping.business.base.CurrentUsrSession;

public class CurrentUsrSessionImpl implements CurrentUsrSession {

	static final ThreadLocal<String> SESSION_KEY = new ThreadLocal<>();

	@Override
	public String getUsrSession() {
		return Optional.ofNullable(SESSION_KEY.get())
				.orElseThrow(() -> new IllegalStateException("User not available"));
	}

}
