package ro.utcn.grouping.model.usr;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface UsrCriteria {

	@Nullable
	Set<String> getEmailIncl();

	@Nullable
	Set<String> getUsrnmIncl();
}
