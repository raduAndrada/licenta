package ro.utcn.grouping.model.usr;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtUsrGet.Builder.class)
public interface UsrGet {

	@Nullable
	String getUsrnm();

	@Nullable
	String getEmail();
}
