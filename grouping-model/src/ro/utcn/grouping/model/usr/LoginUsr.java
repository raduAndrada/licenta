package ro.utcn.grouping.model.usr;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtLoginUsr.class)
@JsonDeserialize(builder = ImtLoginUsr.Builder.class)
public interface LoginUsr {

	String getUsrnm();

	String getPsswd();

}
