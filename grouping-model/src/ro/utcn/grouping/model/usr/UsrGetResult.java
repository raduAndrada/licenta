package ro.utcn.grouping.model.usr;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtUsrGetResult.class)
@JsonDeserialize(builder = ImtUsrGetResult.Builder.class)
public interface UsrGetResult {

	List<Usr> getList();
}
