package ro.utcn.grouping.model.usr;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtUsrRecord.class)
@JsonDeserialize(builder = ImtUsrRecord.Builder.class)
public interface UsrRecord {
	Usr getUsr();

	UsrTp getUsrTp();

}
