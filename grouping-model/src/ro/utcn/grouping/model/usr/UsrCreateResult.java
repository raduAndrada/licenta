package ro.utcn.grouping.model.usr;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtUsrCreateResult.class)
@JsonDeserialize(builder = ImtUsrCreateResult.Builder.class)
public interface UsrCreateResult {

	String getUsrnm();

	String getEmail();

}
