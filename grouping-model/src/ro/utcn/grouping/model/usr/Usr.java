package ro.utcn.grouping.model.usr;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtUsr.class)
@JsonDeserialize(builder = ImtUsr.Builder.class)
public interface Usr {

	String getUsrnm();

	String getEmail();

	String getPsswd();

	UsrTp getUsrTp();

	String getFnm();

	String getLnm();

}
