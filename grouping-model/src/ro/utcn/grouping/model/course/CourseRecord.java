package ro.utcn.grouping.model.course;

import java.time.Instant;
import java.util.List;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import ro.utcn.grouping.model.lesson.Lesson;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
public interface CourseRecord {

	Course getCourse();

	List<Lesson> getLessonsList();

	Instant getCrtTm();

}
