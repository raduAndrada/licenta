package ro.utcn.grouping.model.course;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtCourseUsr.class)
@JsonDeserialize(builder = ImtCourseUsr.Builder.class)
public interface CourseUsr {

	String getCourseNm();

	String getUsrUsrnm();

}
