package ro.utcn.grouping.model.course;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtCourseUsrGetResult.class)
@JsonDeserialize(builder = ImtCourseUsrGetResult.Builder.class)
public interface CourseUsrGetResult {

	List<CourseUsr> getList();
}
