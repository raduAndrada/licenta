package ro.utcn.grouping.model.course;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface CourseCriteria {

	@Nullable
	Set<String> getNmIncl();

}
