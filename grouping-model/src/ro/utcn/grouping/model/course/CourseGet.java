package ro.utcn.grouping.model.course;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtCourseGet.Builder.class)
public interface CourseGet {

	@Nullable
	String getNm();

}
