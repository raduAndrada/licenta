package ro.utcn.grouping.model.course;

import java.time.Instant;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtCourse.class)
@JsonDeserialize(builder = ImtCourse.Builder.class)
public interface Course {

	String getNm();

	String getDsc();

	Integer getStudentsCount();

	Instant getStDt();

	Instant getEndDt();

	String getEnrollmentKey();

	String getProffessorUsrnm();

}
