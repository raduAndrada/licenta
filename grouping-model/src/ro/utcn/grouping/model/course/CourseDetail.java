package ro.utcn.grouping.model.course;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Interface used in order to get the courses for a student
 *
 * @author Andrada
 *
 */

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtCourseDetail.class)
@JsonDeserialize(builder = ImtCourseDetail.Builder.class)
public interface CourseDetail {

	/**
	 * Check if student enrolled in course
	 *
	 * @return true, if student enrolled, false otherwise
	 */
	boolean getEnrolled();

	/**
	 * @return the course
	 */
	Course getCourse();

}
