package ro.utcn.grouping.model.base;

import org.immutables.value.Value;

@Value.Immutable
@Value.Modifiable
public interface CurrentSession {

	Long getSesssionAvalability();

	String getSessionKey();
}
