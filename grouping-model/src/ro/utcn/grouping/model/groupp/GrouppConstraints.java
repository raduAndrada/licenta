package ro.utcn.grouping.model.groupp;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtGrouppConstraints.class)
@JsonDeserialize(builder = ImtGrouppConstraints.Builder.class)
public interface GrouppConstraints {

	String getCourseNm();

	GrouppTp getGrouppTp();

	Integer getGrouppSize();

	Integer getMandate();

	String getProjNm();

	/**
	 * If default: leader is randomly chosen, otherwise team members are allowed to
	 * vote for their leader
	 *
	 * @return true, leader random false, leader chosen by teammates
	 */
	Boolean getDefaultt();
}
