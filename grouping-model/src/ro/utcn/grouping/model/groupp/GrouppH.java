package ro.utcn.grouping.model.groupp;

import java.time.Instant;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ro.utcn.grouping.model.rsp.Rsp;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonDeserialize(as = ImtGrouppH.class)
@JsonSerialize(as = ImtGrouppH.class)
public interface GrouppH {

	Long getGroupId();

	Integer getSession();

	String getTheme();

	Rsp getFinalRsp();

	Instant getCrtTm();
}
