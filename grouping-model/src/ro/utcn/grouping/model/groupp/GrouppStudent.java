package ro.utcn.grouping.model.groupp;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtGrouppStudent.class)
@JsonDeserialize(builder = ImtGrouppStudent.Builder.class)
public interface GrouppStudent {

	Long getGouppId();

	String getUsrUsrnm();

}
