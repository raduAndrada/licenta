package ro.utcn.grouping.model.groupp;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface GrouppCriteria {

	@Nullable
	Set<Long> getIdIncl();

	@Nullable
	Set<String> getCourseNmIncl();

	@Nullable
	Set<GrouppTp> getGrouppTpIncl();
}
