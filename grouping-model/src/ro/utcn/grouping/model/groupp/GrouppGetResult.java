package ro.utcn.grouping.model.groupp;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtGrouppGetResult.class)
@JsonDeserialize(builder = ImtGrouppGetResult.Builder.class)
public interface GrouppGetResult {

	List<Groupp> getList();

}
