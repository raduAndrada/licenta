package ro.utcn.grouping.model.groupp;

import java.util.List;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonDeserialize(as = ImtGroupp.class)
@JsonSerialize(as = ImtGroupp.class)
public interface Groupp {

	Long getId();

	Integer getSize();

	String getProjNm();

	String getLeaderUsrnm();

	Integer getMandate();

	String getCourseNm();

	GrouppTp getGrouppTp();

	@Nullable
	List<String> getStudentsUsrUsrnmsInGroupp();

}
