package ro.utcn.grouping.model.student;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonDeserialize(as = ImtStudent.Builder.class)
@JsonSerialize(as = ImtStudent.class)
public interface Student {

	Long getId();

	String getUsrUsrnm();

	String getUsrEmail();

	Long getGrouppId();

	Long getRqGrouppId();

	Boolean getCheckedIn();
}
