package ro.utcn.grouping.model.student;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtStudentHGetResult.class)
@JsonDeserialize(builder = ImtStudentHGetResult.Builder.class)
public interface StudentHGetResult {

	List<StudentH> getList();
}
