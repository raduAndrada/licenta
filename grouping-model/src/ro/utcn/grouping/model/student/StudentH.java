package ro.utcn.grouping.model.student;

import java.time.Instant;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonDeserialize(as = ImtStudentH.Builder.class)
@JsonSerialize(as = ImtStudentH.class)
public interface StudentH {

	Long getStudentId();

	Long getGroupId();

	String getUsrnm();

	Instant getCrtTm();

}