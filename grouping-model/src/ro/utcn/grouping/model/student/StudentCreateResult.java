package ro.utcn.grouping.model.student;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtStudentCreateResult.class)
@JsonDeserialize(builder = ImtStudentCreateResult.Builder.class)
public interface StudentCreateResult {

	String getUsrUsrnm();

	String getUsrEmail();

	Long getGroupId();

}
