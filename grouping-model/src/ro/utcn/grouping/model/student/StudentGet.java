package ro.utcn.grouping.model.student;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtStudentGet.Builder.class)
public interface StudentGet {

	@Nullable
	String getUsrUsrnm();

	@Nullable
	String getUsrEmail();
}
