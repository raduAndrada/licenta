package ro.utcn.grouping.model.lesson;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtLessonGetResult.class)
@JsonDeserialize(builder = ImtLessonGetResult.Builder.class)
public interface LessonGetResult {

	List<Lesson> getList();
}
