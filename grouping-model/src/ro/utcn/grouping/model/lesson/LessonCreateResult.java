package ro.utcn.grouping.model.lesson;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtLessonCreateResult.class)
@JsonDeserialize(builder = ImtLessonCreateResult.Builder.class)
public interface LessonCreateResult {

	String getCourseNm();

}
