package ro.utcn.grouping.model.lesson;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtLessonGroupp.class)
@JsonDeserialize(builder = ImtLessonGroupp.Builder.class)
public interface LessonGroupp {

	Long getId();

	Long getLessonId();

	Integer getSize();

}
