package ro.utcn.grouping.model.lesson;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtLessonGrouppGetResult.class)
@JsonDeserialize(builder = ImtLessonGrouppGetResult.Builder.class)
public interface LessonGrouppGetResult {

	List<LessonGroupp> getList();
}
