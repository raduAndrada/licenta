package ro.utcn.grouping.model.lesson;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtLesson.class)
@JsonDeserialize(builder = ImtLesson.Builder.class)
public interface Lesson {

	Long getId();

	Integer getWeekNb();

	String getTheme();

	String getCourseNm();

}
