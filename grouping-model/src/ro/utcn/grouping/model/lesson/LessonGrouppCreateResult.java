package ro.utcn.grouping.model.lesson;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtLessonGrouppCreateResult.class)
@JsonDeserialize(builder = ImtLessonGrouppCreateResult.Builder.class)
public interface LessonGrouppCreateResult {

	Long getId();

}
