package ro.utcn.grouping.model.lesson;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtLessonGrouppGet.Builder.class)
public interface LessonGrouppGet {

	@Nullable
	String getLessonId();

}
