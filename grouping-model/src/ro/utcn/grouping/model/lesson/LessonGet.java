package ro.utcn.grouping.model.lesson;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtLessonGet.Builder.class)
public interface LessonGet {

	@Nullable
	String getCourseNm();

}
