package ro.utcn.grouping.model.rq;

public enum RqStatus {
	OPENED, ONGOING, PENDING, TERMINATED
}
