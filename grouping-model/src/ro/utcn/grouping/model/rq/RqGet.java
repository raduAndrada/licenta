package ro.utcn.grouping.model.rq;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtRqGet.Builder.class)
public interface RqGet {

	@Nullable
	Long getId();

	@Nullable
	Long getLessonId();
}
