package ro.utcn.grouping.model.rq;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ro.utcn.grouping.model.question.QuestionRecord;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtRqRecord.class)
@JsonDeserialize(builder = ImtRqRecord.Builder.class)
public interface RqRecord {

	Rq getRq();

	@Nullable
	Set<QuestionRecord> getQuestions();

}
