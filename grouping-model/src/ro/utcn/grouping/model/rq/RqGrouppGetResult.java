package ro.utcn.grouping.model.rq;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRqGrouppGetResult.class)
@JsonDeserialize(builder = ImtRqGrouppGetResult.Builder.class)
public interface RqGrouppGetResult {

	List<RqGroupp> getList();

}
