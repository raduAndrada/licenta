package ro.utcn.grouping.model.rq;

import java.time.Instant;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtRqGroupp.class)
@JsonDeserialize(builder = ImtRqGroupp.Builder.class)
public interface RqGroupp {

	Long getId();

	Long getRqId();

	RqStatus getRqStatus();

	Instant getCrtTm();
}
