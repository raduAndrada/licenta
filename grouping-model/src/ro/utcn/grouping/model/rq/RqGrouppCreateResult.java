package ro.utcn.grouping.model.rq;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRqGrouppCreateResult.class)
@JsonDeserialize(builder = ImtRqGrouppCreateResult.Builder.class)
public interface RqGrouppCreateResult {

	Long getRqId();

	Integer getSize();

	RqTp getRqGrouppTp();

}
