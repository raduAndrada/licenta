package ro.utcn.grouping.model.rq;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRqGetResult.class)
@JsonDeserialize(builder = ImtRqGetResult.Builder.class)
public interface RqGetResult {

	Integer getWeekNb();

	List<RqRecord> getList();

}
