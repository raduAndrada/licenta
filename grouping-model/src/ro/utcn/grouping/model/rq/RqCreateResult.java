package ro.utcn.grouping.model.rq;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRqCreateResult.class)
@JsonDeserialize(builder = ImtRqCreateResult.Builder.class)
public interface RqCreateResult {

	Integer getAllowedTm();

	Integer getMaxRsp();

	RqTp getRqTp();

	TimeMeasurementTp getTimeMeasurementTp();

}
