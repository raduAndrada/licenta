package ro.utcn.grouping.model.rq;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface RqCriteria {

	@Nullable
	Set<Long> getIdIncl();

	@Nullable
	Set<Long> getLessonIdIncl();

}
