package ro.utcn.grouping.model.rq;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtRqDetail.class)
@JsonDeserialize(builder = ImtRqDetail.Builder.class)
public interface RqDetail {

	Rq getRq();

	@Nullable
	RqGroupp getRqGroupp();
}
