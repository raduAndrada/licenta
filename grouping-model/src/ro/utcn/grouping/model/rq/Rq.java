package ro.utcn.grouping.model.rq;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtRq.class)
@JsonDeserialize(builder = ImtRq.Builder.class)
public interface Rq {

	@Nullable
	Long getId();

	@Nullable
	Long getLessonId();

	// time until the request becomes invalid
	// it will be expressed in seconds
	@Nullable
	Integer getAllowedTm();

	// max number of allowed responses
	@Nullable
	Integer getMaxRsp();

	// type of the requestTime
	@Nullable
	RqTp getRqTp();

	// TIME MEASUREMENT
	@Nullable
	TimeMeasurementTp getTimeMeasurementTp();

	// when a rq is active it starts the grouping
	@Nullable
	RqStatus getRqStatus();

	// type of groups
	@Nullable
	RqGrouppTp getRqGrouppTp();

	@Nullable
	Integer getRqGrouppSz();

}
