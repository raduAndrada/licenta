package ro.utcn.grouping.model.question;

import java.util.List;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtQuestionRecord.class)
@JsonDeserialize(builder = ImtQuestionRecord.Builder.class)
public interface QuestionRecord {

	Question getQuestion();

	@Nullable
	List<QuestionAnswer> getQuestionAnswerList();

}
