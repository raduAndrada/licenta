package ro.utcn.grouping.model.question;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonSerialize(as = ImtQuestionAnswer.class)
@JsonDeserialize(builder = ImtQuestionAnswer.Builder.class)
public interface QuestionAnswer {

	Long getId();

	Long getQuestionId();

	String getAnswerText();

	Boolean getCorrect();

}
