package ro.utcn.grouping.model.question;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtQuestionGetResult.class)
@JsonDeserialize(builder = ImtQuestionGetResult.Builder.class)
public interface QuestionGetResult {

	List<QuestionRecord> getList();

}
