package ro.utcn.grouping.model.question;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface QuestionAnswerCriteria {

	@Nullable
	Set<Long> getIdIncl();

	@Nullable
	Set<Long> getQuestionIdIncl();

}
