package ro.utcn.grouping.model.question;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtQuestionGet.Builder.class)
public interface QuestionGet {

	@Nullable
	Long getId();

	@Nullable
	Long getRqId();
}
