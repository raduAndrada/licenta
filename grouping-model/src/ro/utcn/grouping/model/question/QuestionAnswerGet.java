package ro.utcn.grouping.model.question;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtQuestionAnswerGet.Builder.class)
public interface QuestionAnswerGet {

	@Nullable
	Long getId();

	@Nullable
	Long getQuestionId();
}
