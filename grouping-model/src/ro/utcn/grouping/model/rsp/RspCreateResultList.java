package ro.utcn.grouping.model.rsp;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRspCreateResultList.class)
@JsonDeserialize(builder = ImtRspCreateResultList.Builder.class)
public interface RspCreateResultList {

	List<RspCreateResult> getList();

}
