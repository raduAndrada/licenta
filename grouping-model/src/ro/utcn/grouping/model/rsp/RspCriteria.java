package ro.utcn.grouping.model.rsp;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface RspCriteria {

	@Nullable
	Set<Long> getRqGrouppIdIncl();

	@Nullable
	Set<Long> getIdIncl();

	@Nullable
	Set<Long> getQuestionIdIncl();

	@Nullable
	Set<String> getStudentUsrnmIncl();

}
