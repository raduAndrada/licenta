package ro.utcn.grouping.model.rsp;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonDeserialize(as = ImtRsp.class)
@JsonSerialize(as = ImtRsp.class)
public interface Rsp {

	/**
	 * Get unique key for the Rsp
	 *
	 * @return identifier for the rsp
	 */
	Long getId();

	/**
	 *
	 * @return key to the rqGroupp
	 */
	Long getRqGrouppId();

	/**
	 *
	 * @return key for question
	 */
	Long getQuestionId();

	/**
	 *
	 * @return student's usrnm
	 */
	String getStudentUsrnm();

	/**
	 *
	 * @return student's answer text
	 */
	String getRspText();

	/**
	 *
	 * @return the number of votes this rsp got
	 */
	Integer getVoteCount();

}
