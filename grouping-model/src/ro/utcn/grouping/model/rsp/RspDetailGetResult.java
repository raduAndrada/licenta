package ro.utcn.grouping.model.rsp;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRspDetailGetResult.class)
@JsonDeserialize(builder = ImtRspDetailGetResult.Builder.class)
public interface RspDetailGetResult {

	List<RspDetail> getList();
}
