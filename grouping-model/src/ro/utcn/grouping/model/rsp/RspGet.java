package ro.utcn.grouping.model.rsp;

import java.util.List;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtRspGet.Builder.class)
public interface RspGet {

	@Nullable
	Long getRqGrouppId();

	@Nullable
	List<Long> getQuestionsIds();

}
