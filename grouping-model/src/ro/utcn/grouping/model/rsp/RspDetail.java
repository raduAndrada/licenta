package ro.utcn.grouping.model.rsp;

import java.util.List;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRspDetail.class)
@JsonDeserialize(builder = ImtRspDetail.Builder.class)
public interface RspDetail {

	@Nullable
	List<String> getStudentUsrnmsList();

	@Nullable
	RspRecord getRspRecord();

}
