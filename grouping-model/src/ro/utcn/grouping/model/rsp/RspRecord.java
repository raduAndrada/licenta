package ro.utcn.grouping.model.rsp;

import java.util.List;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
@JsonDeserialize(as = ImtRspRecord.class)
@JsonSerialize(as = ImtRspRecord.class)
public interface RspRecord {

	String getQuestionText();

	List<Rsp> getList();

}
