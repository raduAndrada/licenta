package ro.utcn.grouping.model.rsp;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRspCreateList.class)
@JsonDeserialize(builder = ImtRspCreateList.Builder.class)
public interface RspCreateList {

	List<Rsp> getList();
}
