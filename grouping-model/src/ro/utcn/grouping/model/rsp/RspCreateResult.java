package ro.utcn.grouping.model.rsp;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtRspCreateResult.class)
@JsonDeserialize(builder = ImtRspCreateResult.Builder.class)
public interface RspCreateResult {

	String getRspText();

	Long getRqGrouppId();
}
