package ro.utcn.grouping.model.proffesor;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = ImtProffessorGet.Builder.class)
public interface ProffessorGet {

	@Nullable
	String getUsrUsrnm();

	@Nullable
	String getUsrEmail();
}
