package ro.utcn.grouping.model.proffesor;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtProffessorCreateResult.class)
@JsonDeserialize(builder = ImtProffessorCreateResult.Builder.class)
public interface ProffessorCreateResult {

	String getUsrUsrnm();

	String getUsrEmail();

}
