package ro.utcn.grouping.model.proffesor;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Value.Modifiable
@Serial.Version(1)
public interface Proffessor {

	Long id();

	String getUsrUsrnm();

	String getUsrEmail();

}
