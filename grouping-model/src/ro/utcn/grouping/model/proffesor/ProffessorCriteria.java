package ro.utcn.grouping.model.proffesor;

import java.util.Set;

import javax.annotation.Nullable;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

@Value.Immutable
@Serial.Version(1)
public interface ProffessorCriteria {

	@Nullable
	Set<String> getUsrEmailIncl();

	@Nullable
	Set<String> getUsrUsrnmIncl();

	@Nullable
	Set<Long> getIdIncl();
}
