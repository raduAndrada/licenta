package ro.utcn.grouping.model.proffesor;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImtProffessorGetResult.class)
@JsonDeserialize(builder = ImtProffessorGetResult.Builder.class)
public interface ProffessorGetResult {

	List<Proffessor> getList();
}
