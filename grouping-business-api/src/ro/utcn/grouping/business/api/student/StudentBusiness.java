package ro.utcn.grouping.business.api.student;

import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.StudentCourseGetResult;
import ro.utcn.grouping.model.groupp.GrouppGetResult;
import ro.utcn.grouping.model.groupp.GrouppStudent;
import ro.utcn.grouping.model.rq.RqGroupp;
import ro.utcn.grouping.model.rq.RqRecord;
import ro.utcn.grouping.model.rsp.RspCreateList;
import ro.utcn.grouping.model.rsp.RspCreateResultList;
import ro.utcn.grouping.model.rsp.RspGetResult;

public interface StudentBusiness {

	public RspCreateResultList addRsps(RspCreateList rspCreateList);

	public RspGetResult getRspsForRqGroupp(Long rqGrouppId);

	public RspCreateList voteRsp(RspCreateList rspCreateList);

	public Course enrollStudent(String usrUsrnm, String courseNm);

	public GrouppGetResult getSemesterGrouppsByChoice(String courseNm);

	public int joinGroupp(GrouppStudent grouppStudent);

	public RqGroupp getRqGrouppForStudent(String studentUsrnm);

	public RqRecord getRqForStudent(String studentUsrnm);

	public StudentCourseGetResult getAvailableCourses(String studentUsrnm);

}
