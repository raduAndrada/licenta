package ro.utcn.grouping.business.api.usr;

import ro.utcn.grouping.model.course.CourseGetResult;
import ro.utcn.grouping.model.course.CourseRecord;
import ro.utcn.grouping.model.usr.LoginUsr;
import ro.utcn.grouping.model.usr.Usr;
import ro.utcn.grouping.model.usr.UsrCreateResult;
import ro.utcn.grouping.model.usr.UsrGet;
import ro.utcn.grouping.model.usr.UsrGetResult;

public interface UsrBusiness {

	public Usr getUsr(String usrnm);

	public UsrCreateResult createUsr(Usr usr);

	public Usr deleteUsr(String usrnm);

	public Usr loginUsr(LoginUsr usr);

	public Usr logoutUsr(String usrnm);

	public UsrGetResult getUsrs(UsrGet usrGet);

	public CourseGetResult getCoursesForUsr(String usrnm);

	public CourseRecord getCourseForUsrByCourseNm(String usrnm, String courseNm);

}
