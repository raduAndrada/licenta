package ro.utcn.grouping.business.api.proffessor;

import ro.utcn.grouping.model.course.Course;
import ro.utcn.grouping.model.course.CourseCreateResult;
import ro.utcn.grouping.model.groupp.GrouppConstraints;
import ro.utcn.grouping.model.groupp.GrouppGetResult;
import ro.utcn.grouping.model.question.QuestionRecord;
import ro.utcn.grouping.model.rq.Rq;
import ro.utcn.grouping.model.rq.RqCreateResult;
import ro.utcn.grouping.model.rq.RqDetail;
import ro.utcn.grouping.model.rq.RqGet;
import ro.utcn.grouping.model.rq.RqGetResult;
import ro.utcn.grouping.model.rq.RqGrouppGetResult;
import ro.utcn.grouping.model.rq.RqRecord;
import ro.utcn.grouping.model.rsp.RspDetailGetResult;

public interface ProffessorBusiness {

	/**
	 * Method to add a rq to a lesson
	 *
	 * @param rq
	 *            the request to be added
	 * @return the create result
	 */
	public RqCreateResult addRq(RqDetail rqDetail);

	/**
	 * Gets a list of requests for a lesson
	 *
	 * @param rqGet
	 *            the entity to query the database
	 * @return the rqs for the lesson
	 */
	public RqGetResult getRqForLesson(RqGet rqGet);

	/**
	 * Method to update a request (edit its questions) or activate it
	 *
	 * @param rqRecord
	 *            the record with the questions and its answers
	 * @return the updated request
	 */
	public RqRecord updateQuestionListForRq(RqRecord rqRecord);

	/**
	 * Delete a rq for a lesson
	 *
	 * @param rqId
	 *            the id of the deleted request
	 * @return the result of the deletion
	 */
	public Rq deleteRq(Long rqId);

	/**
	 * When a teacher wants to launch (activate the grouping) he needs to call this
	 * method
	 *
	 * @param rqId
	 *            the identifier of the request we are launching
	 * @return the data about the rq activated
	 */
	public RqRecord updateRq(Rq rq);

	/**
	 * We can add a question which has an answer if the rqTp is TEXT_FILLING
	 * otherwise if rqTp is CHOICE the question does not have an answer
	 *
	 * @param questionRecord
	 *            the record with the question and its answer (optionally)
	 * @return the created question
	 */
	public RqRecord addQuestionToRq(QuestionRecord questionRecord);

	/**
	 * Get the rsps for a specific rq
	 *
	 * @param rqId
	 *            the id of the rq
	 * @return the list with the details about the answers
	 */
	public RspDetailGetResult getRspsForRq(Long rqId);

	/**
	 * Get the groups associated to a specific course
	 *
	 * @param courseNm
	 *            name for the course
	 * @return the list of the groups formed
	 */
	public RqGrouppGetResult getRqGrouppForCourse(String courseNm);

	public CourseCreateResult createCourse(Course course);

	public Course updateCourse(Course course);

	public Course deleteCourse(String courseNm);

	/**
	 * For this method grouping logic, we do the following: randomly rearrange the
	 * list of students registered for the course then we split the list into the
	 * constraints.size
	 *
	 * @param constraints
	 *            the constraints from the the teacher with all the necessary params
	 * @return the groups from the app
	 */
	public GrouppGetResult groupForSemProjs(GrouppConstraints constraints);

	public GrouppGetResult getGroupsForCourse(String courseNm);

}
