package ro.utcn.grouping.business.api.group;

import ro.utcn.grouping.model.groupp.Groupp;

public interface GroupBusiness {

	public void group(Groupp group);

}
